package com.epam.training.car;

import com.epam.training.car.engine.ElectricEngine;
import com.epam.training.car.engine.InternalCombustionEngine;
import com.epam.training.car.feature.*;

/* the class of some abstract passenger car */
public abstract class PassengerCar {
	private int carID;
	private int carPrice; // measured in US Dollars
	private int topSpeed; // measured in km/hour
	private int curbWeight; // measured in kg
	private double consumption; // measured in l/100km (or eq. for el. cars)
	private BodyStyle bodyStyle;
	private DriveArrangement driveArrangement;
	private Gearbox gearbox;

	/* getters and setters */
	/* no need in validation cause we take data from XML/XSD */
	public int getCarID() {
		return carID;
	}

	public void setCarID(int carID) {
		this.carID = carID;
	}

	public int getCarPrice() {
		return carPrice;
	}

	public void setCarPrice(int carPrice) {
		this.carPrice = carPrice;
	}

	public int getTopSpeed() {
		return topSpeed;
	}

	public void setTopSpeed(int topSpeed) {
		this.topSpeed = topSpeed;
	}

	public int getCurbWeight() {
		return curbWeight;
	}

	public void setCurbWeight(int curbWeight) {
		this.curbWeight = curbWeight;
	}

	public double getConsumption() {
		return consumption;
	}

	public void setConsumption(double consumption) {
		this.consumption = consumption;
	}

	public BodyStyle getBodyStyle() {
		return bodyStyle;
	}

	public void setBodyStyle(BodyStyle bodyStyle) {
		this.bodyStyle = bodyStyle;
	}

	public DriveArrangement getDriveArrangement() {
		return driveArrangement;
	}

	public void setDriveArrangement(DriveArrangement driveArrangement) {
		this.driveArrangement = driveArrangement;
	}

	public Gearbox getGearbox() {
		return gearbox;
	}

	public void setGearbox(Gearbox gearbox) {
		this.gearbox = gearbox;
	}

	/* abstract methods to be implemented in the sub-classes */
	public abstract InternalCombustionEngine getCombustionEngine();
	
	public abstract void setCombustionEngine(InternalCombustionEngine engine);
	
	public abstract ElectricEngine getElectricEngine();
	
	public abstract void setElectricEngine(ElectricEngine engine);
}
