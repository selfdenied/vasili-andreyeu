package com.epam.training.car.engine;

import com.epam.training.car.feature.FuelType;

/* the class of a petrol engine */
public class PetrolEngine extends InternalCombustionEngine {
	private FuelType fuelType = FuelType.PETROL;

	@Override
	public String toString() {
		return "Engine fuel type: " + fuelType.name();
	}
}
