package com.epam.training.command;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.epam.training.car.PassengerCar;
import com.epam.training.constant.Constants;
import com.epam.training.exception.GeneralCommandException;
import com.epam.training.factory.CarBuilderFactory;
import com.epam.training.parser.AbstractCarBuilder;
import com.epam.training.taxifleet.TaxiFleet;
import com.epam.training.taxifleet.TaxiFleetCreator;

public class XMLParsingCommand implements ICommand {
	private static final Logger LOG = Logger.getLogger(XMLParsingCommand.class);
	private static final String url = "jsp/Result.jsp";
	private String parserType;
	private String path;

	@Override
	public String execute(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException,
			GeneralCommandException {
		parserType = request.getParameter("parser");
		path = (String) request.getAttribute("base");
		request.setAttribute("taxiFleet", obtainTaxiFleet(parserType, path));
		request.setAttribute("parser", parserType);
		return url;
	}

	private List<PassengerCar> obtainTaxiFleet(String parserType, String path) {
		AbstractCarBuilder builder = CarBuilderFactory
				.createCarBuilder(parserType);
		TaxiFleet taxiFleet = TaxiFleetCreator.constructTaxiFleet(builder,
				path + Constants.INPUT_FILE);
		LOG.info("The fleet was successfully created!");
		return taxiFleet.getTaxiFleetList();
	}
}
