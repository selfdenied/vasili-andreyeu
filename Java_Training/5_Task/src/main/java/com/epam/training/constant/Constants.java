package com.epam.training.constant;

/* the list of all constants used in the application */
public class Constants {
	public static final String INPUT_FILE = "data/cars.xml";
	public static final String COMBUSTION_CAR = "combustion-car";
	public static final String ELECTRIC_CAR = "electric-car";
	public static final String HYBRID_CAR = "hybrid-car";
	public static final String ENGINE = "engine";
	public static final String PETROL = "Petrol";
	public static final String DIESEL = "Diesel";
	public static final String ELECTRICITY = "Electricity";
	public static final String FUEL = "fuel";
}
