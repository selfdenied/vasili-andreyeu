package com.epam.training.car;

import com.epam.training.car.engine.ElectricEngine;
import com.epam.training.car.engine.InternalCombustionEngine;

/* the class of an electric passenger car */
public class ElectricPassengerCar extends PassengerCar {
	private ElectricEngine electricEngine;

	@Override
	public ElectricEngine getElectricEngine() {
		return electricEngine;
	}

	@Override
	public void setElectricEngine(ElectricEngine electricEngine) {
		this.electricEngine = electricEngine;
	}

	/* this method is not applicable to this class */
	@Override
	public InternalCombustionEngine getCombustionEngine() {
		return null;
	}

	/* this method is not applicable to this class */
	@Override
	public void setCombustionEngine(InternalCombustionEngine engine) {
		return;
	}
}
