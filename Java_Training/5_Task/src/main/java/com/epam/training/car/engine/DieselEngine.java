package com.epam.training.car.engine;

import com.epam.training.car.feature.FuelType;

/* the class of a diesel engine */
public class DieselEngine extends InternalCombustionEngine {
	private FuelType fuelType = FuelType.DIESEL;

	@Override
	public String toString() {
		return "Engine fuel type: " + fuelType.name();
	}
}
