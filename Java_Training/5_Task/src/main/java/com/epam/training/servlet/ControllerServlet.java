package com.epam.training.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.epam.training.command.ICommand;
import com.epam.training.command.factory.CommandFactory;
import com.epam.training.exception.GeneralCommandException;

public class ControllerServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger LOG = Logger.getLogger(ControllerServlet.class);

	public ControllerServlet() {
		super();
	}

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	protected void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String url = "Index.jsp";
		request.setAttribute("base", getServletContext().getRealPath("/"));
		CommandFactory commandFactory = CommandFactory.getInstance();
		ICommand command = commandFactory.getCommand();
		try {
			url = command.execute(request, response);
		} catch (GeneralCommandException exception) {
			LOG.error(exception.getMessage());
		}
		request.getRequestDispatcher(url).forward(request, response);
	}
}
