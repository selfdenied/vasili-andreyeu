package com.epam.training.command.factory;

import com.epam.training.command.*;

/**
 * Class {@code CommandFactory} returns a proper ICommand class depending on the
 * value of "action" request parameter. The factory is constructed using
 * Singleton design pattern (allows to create only 1 instance of factory).
 * 
 * @author Vasili Andreev
 * @version 1.0
 * @see com.epam.training.command.ICommand
 */
public class CommandFactory {

	/* Constructor is made private to implement Singleton design pattern */
	/* initializes a map with Command classes */
	private CommandFactory() {
	}

	public ICommand getCommand() {
		return new XMLParsingCommand();
	}

	public static CommandFactory getInstance() {
		return CommandFactoryHolder.commandFactory;
	}

	/*
	 * Private class that contains static field where CommandFactory object is
	 * created. Such schema prevents creation of 2 or more CommandFactory
	 * objects in a multithread environment
	 * 
	 * (see Singleton design pattern)
	 */
	private static class CommandFactoryHolder {
		private static final CommandFactory commandFactory = new CommandFactory();
	}
}
