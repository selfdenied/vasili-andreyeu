package com.epam.training.exception;

public class GeneralCommandException extends Exception {
	private static final long serialVersionUID = -2115115213686492866L;

	public GeneralCommandException(String message) {
		super(message);
	}

	public GeneralCommandException(Throwable cause) {
		super(cause);
	}

	public GeneralCommandException(String message, Throwable cause) {
		super(message, cause);
	}
}
