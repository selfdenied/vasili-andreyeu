package com.epam.training.car;

import com.epam.training.car.engine.ElectricEngine;
import com.epam.training.car.engine.InternalCombustionEngine;

/* the class of a hybrid drive passenger car */
public class HybridPassengerCar extends PassengerCar {
	private InternalCombustionEngine combustionEngine;
	private ElectricEngine electricEngine;

	@Override
	public InternalCombustionEngine getCombustionEngine() {
		return combustionEngine;
	}
	
	@Override
	public void setCombustionEngine(InternalCombustionEngine combustionEngine) {
		this.combustionEngine = combustionEngine;
	}

	@Override
	public ElectricEngine getElectricEngine() {
		return electricEngine;
	}
	
	@Override
	public void setElectricEngine(ElectricEngine electricEngine) {
		this.electricEngine = electricEngine;
	}
}
