<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel='stylesheet' href='css/style.css' type='text/css' />
<title>XML Parsing Application</title>
</head>
<body>
	<h1 class="maintext">Welcome to the XML Parsing application!</h1>
	<br>
	<br>
	<h2 class="addtext">Choose the type of Parser to create a <em>taxi fleet</em>:</h2>

	<br>
	<div class="addtext">
	<form id="buttons" action="controller" method="post">
	<br>
	<input type="radio" name="parser" value="SAX" checked>SAX parser
	<br>
	<br>
	<input type="radio" name="parser" value="DOM">DOM parser
	<br>
	<br>
	<input type="radio" name="parser" value="STAX">STAX parser
	<br>
	<br>
	<input type="submit" value="Choose">
	</form>
	</div>
	<br>
	<br>
	<p>
		Your host is ${header["host"]}. 
		<br> 
		<em>Administrator's e-mail: ${initParam.adminEmail}</em>
	</p>
</body>
</html>