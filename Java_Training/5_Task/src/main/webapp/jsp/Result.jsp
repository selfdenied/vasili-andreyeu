<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel='stylesheet' href='css/style.css' type='text/css' />
<title>The result of parsing</title>
</head>
<body>
<h1 class="maintext">Welcome to the XML Parsing application!</h1>
<br>
<br>
<h2 class="addtext">The <em>taxi fleet</em> has been successfully created...</h2>
<h2 class="addtext">
<span style="color:blue"><c:out value="${requestScope.parser}"></c:out></span> parser has been used
</h2>

<div>
<h3 class="taxitext">The list of cars in the taxi company:</h3>
<table>
		<tr align="center">
			<td><b>ID</b></td>
			<td><b>Price</b></td>
			<td><b>Мax speed</b></td>
			<td><b>Curb weight</b></td>
			<td><b>Consumption</b></td>
			<td><b>Body style</b></td>
			<td><b>Drive</b></td>
			<td><b>Gearbox</b></td>
		</tr>
		<c:forEach var='taxi' items='${requestScope.taxiFleet}'>
		<tr align="center">
			<td>c<c:out value="${taxi.carID}"></c:out></td>
			<td><c:out value="${taxi.carPrice}"></c:out> $</td>
			<td><c:out value="${taxi.topSpeed}"></c:out> km/h</td>
			<td><c:out value="${taxi.curbWeight}"></c:out> kg</td>
			<td><c:out value="${taxi.consumption}"></c:out> liters/100km</td>
			<td><c:out value="${taxi.bodyStyle}"></c:out></td>
			<td><c:out value="${taxi.driveArrangement}"></c:out></td>
			<td><c:out value="${taxi.gearbox}"></c:out></td>
		</tr>
		</c:forEach>
</table>
</div>
<br>
<br>
<h2 class="maintext"><a href="Index.jsp">Back</a></h2>
</body>
</html>