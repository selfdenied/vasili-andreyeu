package com.epam.minsk.classloader;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.HashMap;

public class CustomClassLoader extends ClassLoader {
	private static final String ALLOWED_PACKAGE = "com.epam.minsk.classes";
	private HashMap<String, Class<?>> cache;

	public CustomClassLoader() {
		this.cache = new HashMap<String, Class<?>>();
	}

	private void cacheClass(String fullyQualifiedName) {

		try {
			if (fullyQualifiedName.startsWith(ALLOWED_PACKAGE)) {
				byte[] classData = loadClassData(fullyQualifiedName);
				if (classData != null) {
					Class<?> clazz = defineClass(fullyQualifiedName, classData,
							0, classData.length);
					cache.put(clazz.getName(), clazz);
					System.out.println("Class " + clazz.getName()
							+ " has been cached");
				}
			}
		} catch (IOException IOE) {
			System.out.println("Class " + fullyQualifiedName
					+ " cannot be found!");
		}
	}

	private String normalize(String fullyQualifiedName) {
		return fullyQualifiedName.replace('.', '/');
	}

	private byte[] loadClassData(String fullyQualifiedName) throws IOException {
		byte[] data = null;
		String pathName = normalize(fullyQualifiedName) + ".class";
		URL url = getClass().getClassLoader().getResource(pathName);
		File file = new File(url.getFile());
		data = new byte[(int) file.length()];
		InputStream is = getClass().getClassLoader().getResourceAsStream(pathName);
		is.read(data);
		return data;
	}

	public synchronized Class<?> loadClass(String fullyQualifiedName)
			throws ClassNotFoundException {
		Class<?> result = cache.get(fullyQualifiedName);

		if (result == null) {
			cacheClass(fullyQualifiedName);
			result = cache.get(fullyQualifiedName);
		}

		if (result == null) {
			result = super.findSystemClass(fullyQualifiedName);
		}
		System.out.println("Class " + fullyQualifiedName + " has been loaded");
		return result;
	}
}
