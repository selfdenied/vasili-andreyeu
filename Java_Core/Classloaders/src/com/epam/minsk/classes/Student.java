package com.epam.minsk.classes;

public class Student {
	private static final String PHRASE = "I am a student!";
	
	public Student() {
		System.out.println(PHRASE);
	}
}
