package com.epam.minsk.classes;

public class Teacher {
	private static final String PHRASE = "I am a teacher!";
	
	public Teacher() {
		System.out.println(PHRASE);
	}
}
