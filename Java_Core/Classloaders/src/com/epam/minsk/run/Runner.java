package com.epam.minsk.run;

import com.epam.minsk.classloader.CustomClassLoader;

public class Runner {

	public static void main(String[] args) {
		CustomClassLoader loader = new CustomClassLoader();
		String student = "com.epam.minsk.classes.Student";
		String teacher = "com.epam.minsk.classes.Teacher";
		
		try {
			Class<?> studentClass = loader.loadClass(student);
			Class<?> teacherClass = loader.loadClass(teacher);
			studentClass.newInstance();
			teacherClass.newInstance();
		} catch (InstantiationException ex) {
			System.out.println("Error. Instantiation Exception!!!");
		} catch (IllegalAccessException ez) {
			System.out.println("Error. Illegal access!!!");
		} catch (ClassNotFoundException ex) {
			System.out.println("Error. Class not found!!!");
		}
	}
}
