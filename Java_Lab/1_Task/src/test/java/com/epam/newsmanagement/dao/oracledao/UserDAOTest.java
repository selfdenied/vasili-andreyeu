package com.epam.newsmanagement.dao.oracledao;

import java.util.Set;

import org.junit.Assert;

import com.epam.newsmanagement.bean.User;
import com.epam.newsmanagement.dao.AbstractDAOTest;

/**
 * Class {@code UserDAOTest} contains a number of methods that test the proper
 * functioning of C.R.U.D operations realized by UserDAO methods.
 * 
 * @author Vasili Andreev
 * @version 1.0
 * @see com.epam.newsmanagement.dao.AbstractDAOTest
 * @see com.epam.newsmanagement.dao.oracledao.OracleUserDAO
 */
public class UserDAOTest extends AbstractDAOTest {
	private static final String Q_TABLE_NAME = "SYS.USERS";
	private OracleUserDAO userDAO;

	/**
	 * Constructs AuthorDAOTest object
	 */
	public UserDAOTest() {
		this.userDAO = getCtx().getBean(OracleUserDAO.class);
		userDAO.setTestMode(true);
	}

	/**
	 * Tests the correctness of loaded dataset.
	 * 
	 * @throws Exception
	 *             If Exception of some kind has occurred.
	 */
	public void testDataLoaded() throws Exception {
		int rows = 20;
		Assert.assertNotNull(getDataSet());
		int rowCount = getDataSet().getTable(Q_TABLE_NAME).getRowCount();
		Assert.assertEquals(rows, rowCount);
	}

	/**
	 * Tests findEntityByID, findUserRole and findAll methods of UserDAO.
	 * 
	 * @throws Exception
	 *             If exception of some kind has occurred.
	 */
	public void testExctractMethods() throws Exception {
		Long userId = 1L;
		Set<User> usersList = userDAO.findAll();
		User user = userDAO.findEntityById(userId);
		String role = userDAO.findUserRole(userId);

		// tests the correctness of gained information
		Assert.assertNotNull(usersList);
		Assert.assertFalse(usersList.isEmpty());
		Assert.assertNotNull(user);
		Assert.assertEquals("Василий Андреев", user.getUserName());
		Assert.assertEquals("selfdenied", user.getLogin());
		Assert.assertEquals("Admin", role);
	}

	/**
	 * Tests addNewEntity and addUserRole methods of UserDAO.
	 * 
	 * @throws Exception
	 *             If exception of some kind has occurred.
	 */
	public void testAddMethods() throws Exception {
		Long userId = null;
		String userName = "Anthony Tailer";
		String login = "marat22";
		String password = "pass111";
		String roleName = "Admin";
		boolean isAdded = false;
		boolean isPresent = false;

		/* tests that no users with such login exist */
		Set<User> usersList = userDAO.findAll();
		for (User user : usersList) {
			Assert.assertNotEquals(login, user.getLogin());
		}
		User newUser = new User();
		newUser.setLogin(login);
		newUser.setPassword(password);
		newUser.setUserName(userName);

		// add new user
		isAdded = userDAO.addNewEntity(newUser);

		// test the correctness of add method
		Assert.assertTrue(isAdded);
		usersList = userDAO.findAll();
		for (User user : usersList) {
			if (login.equals(user.getLogin())) {
				isPresent = true;
				userId = user.getUserId();
			}
		}
		Assert.assertTrue(isPresent);
		Assert.assertNotNull(userId);

		/* tests that no role has been assigned to the user yet */
		Assert.assertNull(userDAO.findUserRole(userId));

		// add user role
		isAdded = userDAO.addUserRole(userId, roleName);

		// test the correctness of add method
		Assert.assertTrue(isAdded);
		Assert.assertEquals(roleName, userDAO.findUserRole(userId));
	}

	/**
	 * Tests updateEntity and updateUserRole methods of UserDAO.
	 * 
	 * @throws Exception
	 *             If exception of some kind has occurred.
	 */
	public void testUpdateMethods() throws Exception {
		Long userId = 14L;
		String newRole = "Admin";
		boolean isUpdated = false;

		User user = userDAO.findEntityById(userId);
		String role = userDAO.findUserRole(userId);
		Assert.assertNotNull(user);
		Assert.assertEquals("David Gill", user.getUserName());
		Assert.assertEquals("daveRT54", user.getLogin());
		Assert.assertEquals("Client", role);

		// update user's name, login and role
		user.setUserName("David Gill New");
		user.setLogin("daveNew");
		isUpdated = userDAO.updateEntity(user, userId);
		isUpdated = userDAO.updateUserRole(newRole, userId);

		// test the correctness of update method
		User newUser = userDAO.findEntityById(userId);
		role = userDAO.findUserRole(userId);
		Assert.assertEquals("David Gill New", newUser.getUserName());
		Assert.assertEquals("daveNew", newUser.getLogin());
		Assert.assertEquals("Admin", role);
		Assert.assertTrue(isUpdated);
	}

	/**
	 * Tests deleteEntity method of UserDAO.
	 * 
	 * @throws Exception
	 *             If exception of some kind has occurred.
	 */
	public void testDeleteMethod() throws Exception {
		Long userId = 5L;
		boolean isDeleted = true;

		try {
			isDeleted = userDAO.deleteEntity(userId);
		} catch (UnsupportedOperationException ex) {
			isDeleted = false;
			Assert.assertEquals("Error. This operation is not supported!",
					ex.getMessage());
		}
		Assert.assertFalse(isDeleted);
	}
}
