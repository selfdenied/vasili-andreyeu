package com.epam.newsmanagement.dao.oracledao;

import java.util.Set;

import org.junit.Assert;

import com.epam.newsmanagement.bean.Tag;
import com.epam.newsmanagement.dao.AbstractDAOTest;

/**
 * Class {@code TagDAOTest} contains a number of methods that test the proper
 * functioning of C.R.U.D operations realized by TagDAO methods.
 * 
 * @author Vasili Andreev
 * @version 1.0
 * @see com.epam.newsmanagement.dao.AbstractDAOTest
 * @see com.epam.newsmanagement.dao.oracledao.OracleTagDAO
 */
public class TagDAOTest extends AbstractDAOTest {
	private static final String Q_TABLE_NAME = "SYS.TAGS";
	private OracleTagDAO tagDAO;

	/**
	 * Constructs TagDAOTest object
	 */
	public TagDAOTest() {
		this.tagDAO = getCtx().getBean(OracleTagDAO.class);
		tagDAO.setTestMode(true);
	}

	/**
	 * Tests the correctness of loaded dataset.
	 * 
	 * @throws Exception
	 *             If Exception of some kind has occurred.
	 */
	public void testDataLoaded() throws Exception {
		int rows = 21;
		Assert.assertNotNull(getDataSet());
		int rowCount = getDataSet().getTable(Q_TABLE_NAME).getRowCount();
		Assert.assertEquals(rows, rowCount);
	}

	/**
	 * Tests findEntityByID, findAllTagsByNewsID and findAll methods of TagDAO.
	 * 
	 * @throws Exception
	 *             If exception of some kind has occurred.
	 */
	public void testExctractMethods() throws Exception {
		Long tagId = 20L;
		Long newsId = 7L;
		Set<Tag> tagsList = tagDAO.findAll();
		Set<Tag> newsTags = tagDAO.findAllTagsByNewsId(newsId);
		Tag tag = tagDAO.findEntityById(tagId);

		// tests the correctness of gained information
		Assert.assertNotNull(tagsList);
		Assert.assertFalse(tagsList.isEmpty());
		Assert.assertNotNull(newsTags);
		Assert.assertFalse(newsTags.isEmpty());
		Assert.assertNotNull(tag);
		Assert.assertEquals("Business", tag.getTagName());
	}

	/**
	 * Tests updateEntity method of TagDAO.
	 * 
	 * @throws Exception
	 *             If Exception of some kind has occurred.
	 */
	public void testUpdateMethod() throws Exception {
		boolean isUpdated = false;
		Long tagId = 18L;

		Tag tag = tagDAO.findEntityById(tagId);
		Assert.assertNotNull(tag);
		Assert.assertEquals("Sport", tag.getTagName());

		// update tag's name
		tag.setTagName("SPORT");
		isUpdated = tagDAO.updateEntity(tag, tagId);

		// check the correctness of the method
		Tag newTag = tagDAO.findEntityById(tagId);
		Assert.assertNotNull(newTag);
		Assert.assertEquals("SPORT", newTag.getTagName());

		// returning to the original state
		newTag.setTagName("Sport");
		tagDAO.updateEntity(newTag, tagId);
		Assert.assertTrue(isUpdated);
	}

	/**
	 * Tests addNewEntity and deleteEntity method of TagDAO.
	 * 
	 * @throws Exception
	 *             If Exception of some kind has occurred.
	 */
	public void testAddAndDeleteMethod() throws Exception {
		Long tagId = null;
		boolean isAdded = false;
		boolean isDeleted = false;
		String name = "Cool tag";
		Tag tag = new Tag();
		tag.setTagName(name);

		isAdded = tagDAO.addNewEntity(tag);
		for (Tag entity : tagDAO.findAll()) {
			if (name.equals(entity.getTagName())) {
				tagId = entity.getTagId();
			}
		}

		// tests the correctness of action
		tag = tagDAO.findEntityById(tagId);
		Assert.assertNotNull(tag);
		Assert.assertEquals(name, tag.getTagName());
		isDeleted = tagDAO.deleteEntity(tagId);

		// tests the correctness of action
		tag = tagDAO.findEntityById(tagId);
		Assert.assertNull(tag);
		Assert.assertTrue(isAdded);
		Assert.assertTrue(isDeleted);
	}

	/**
	 * Tests assignTagToNews and deassignTagsFromNews methods of TagDAO.
	 * 
	 * @throws Exception
	 *             If Exception of some kind has occurred.
	 */
	public void testAssignDeassignMethods() throws Exception {
		boolean isAssigned = false;
		boolean isPresent = false;
		boolean isDeassigned = false;
		Long tagId = 2L;
		Long newsId = 10L;

		/* check that the given tag is not assigned to the news message */
		Set<Tag> tagsList = tagDAO.findAllTagsByNewsId(newsId);
		Assert.assertNotNull(tagsList);
		for (Tag tag : tagsList) {
			Assert.assertNotEquals(tagId, tag.getTagId());
		}

		/* perform the assignment procedure */
		isAssigned = tagDAO.assignTagToNews(newsId, tagId);

		/* check the correctness of action */
		Set<Tag> newTagsList = tagDAO.findAllTagsByNewsId(newsId);
		Assert.assertNotNull(newTagsList);
		for (Tag tag : newTagsList) {
			if (tagId == tag.getTagId()) {
				isPresent = true;
			}
		}

		/* deassign all tags from news message */
		isDeassigned = tagDAO.deassignTagsFromNews(newsId);

		/* check the correctness of action */
		tagsList = tagDAO.findAllTagsByNewsId(newsId);
		Assert.assertTrue(tagsList.isEmpty());
		Assert.assertTrue(isPresent);
		Assert.assertTrue(isAssigned);
		Assert.assertTrue(isDeassigned);
	}
}
