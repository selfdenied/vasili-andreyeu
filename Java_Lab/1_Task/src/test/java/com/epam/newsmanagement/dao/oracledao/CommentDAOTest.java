package com.epam.newsmanagement.dao.oracledao;

import java.util.Set;

import org.junit.Assert;

import com.epam.newsmanagement.bean.Comment;
import com.epam.newsmanagement.dao.AbstractDAOTest;

/**
 * Class {@code CommentDAOTest} contains a number of methods that test the
 * proper functioning of C.R.U.D operations realized by CommentDAO methods.
 * 
 * @author Vasili Andreev
 * @version 1.0
 * @see com.epam.newsmanagement.dao.AbstractDAOTest
 * @see com.epam.newsmanagement.dao.oracledao.OracleCommentDAO
 */
public class CommentDAOTest extends AbstractDAOTest {
	private static final String Q_TABLE_NAME = "SYS.COMMENTS";
	private OracleCommentDAO commentDAO;

	/**
	 * Constructs CommentDAOTest object
	 */
	public CommentDAOTest() {
		this.commentDAO = getCtx().getBean(OracleCommentDAO.class);
		commentDAO.setTestMode(true);
	}

	/**
	 * Tests the correctness of loaded dataset.
	 * 
	 * @throws Exception
	 *             If Exception of some kind has occurred.
	 */
	public void testDataLoaded() throws Exception {
		int rows = 20;
		Assert.assertNotNull(getDataSet());
		int rowCount = getDataSet().getTable(Q_TABLE_NAME).getRowCount();
		Assert.assertEquals(rows, rowCount);
	}

	/**
	 * Tests findEntityByID, findCommentsByNewsId, and findAll methods of
	 * CommentDAO.
	 * 
	 * @throws Exception
	 *             If exception of some kind has occurred.
	 */
	public void testExctractMethods() throws Exception {
		Long commentId = 19L;
		Long newsId = 13L;
		String text = "Idiot...";

		try {
			Set<Comment> commentsList = commentDAO.findCommentsByNewsId(newsId);
			Comment comment = commentDAO.findEntityById(commentId);

			// tests the correctness of gained information
			Assert.assertNotNull(commentsList);
			Assert.assertFalse(commentsList.isEmpty());
			Assert.assertNotNull(comment);
			Assert.assertEquals(text, comment.getCommentText());
			Assert.assertEquals(newsId, comment.getNewsId());

			// findAll method is not supported
			commentsList = commentDAO.findAll();
		} catch (UnsupportedOperationException ex) {
			Assert.assertEquals("Error. This operation is not supported!",
					ex.getMessage());
		}
	}

	/**
	 * Tests updateEntity method of CommentDAO.
	 * 
	 * @throws Exception
	 *             If Exception of some kind has occurred.
	 */
	public void testUpdateMethod() throws Exception {
		boolean isUpdated = true;
		Long commentId = 11L;

		try {
			isUpdated = commentDAO.updateEntity(null, commentId);
		} catch (UnsupportedOperationException ex) {
			isUpdated = false;
			Assert.assertEquals("Error. This operation is not supported!",
					ex.getMessage());
		}
		Assert.assertFalse(isUpdated);
	}

	/**
	 * Tests addEntity and deleteEntity methods of CommentDAO.
	 * 
	 * @throws Exception
	 *             If Exception of some kind has occurred.
	 */
	public void testAddAndDeleteMethods() throws Exception {
		boolean isAdded = false;
		boolean isDeleted = false;
		Long newsId = 4L;
		Long commentId = 0L;
		String text = "Just an ordinary comment.";
		Comment comment = new Comment();
		comment.setNewsId(newsId);
		comment.setCommentText(text);

		isAdded = commentDAO.addNewEntity(comment);
		for (Comment entity : commentDAO.findCommentsByNewsId(newsId)) {
			if (text.equals(entity.getCommentText())) {
				commentId = entity.getCommentId();
			}
		}

		// tests the correctness of action
		comment = commentDAO.findEntityById(commentId);
		Assert.assertNotNull(comment);
		Assert.assertEquals(text, comment.getCommentText());
		Assert.assertEquals(newsId, comment.getNewsId());
		isDeleted = commentDAO.deleteEntity(commentId);

		// tests the correctness of action
		comment = commentDAO.findEntityById(commentId);
		Assert.assertNull(comment);
		Assert.assertTrue(isAdded);
		Assert.assertTrue(isDeleted);
	}

	/**
	 * Tests deassignCommentsFromNews method of CommentDAO.
	 * 
	 * @throws Exception
	 *             If Exception of some kind has occurred.
	 */
	public void testDeassignMethod() throws Exception {
		boolean isDeassigned = false;
		Long newsId = 10L;

		/* check that there are comments assigned to the given news message */
		Set<Comment> commentsList = commentDAO.findCommentsByNewsId(newsId);
		Assert.assertNotNull(commentsList);
		Assert.assertEquals(2, commentsList.size());
		
		/* perform deassignment */
		isDeassigned = commentDAO.deassignCommentsFromNews(newsId);
		
		/* tests the correctness of action */
		Set<Comment> newCommentsList = commentDAO.findCommentsByNewsId(newsId);
		Assert.assertTrue(newCommentsList.isEmpty());
		Assert.assertTrue(isDeassigned);
	}
}
