package com.epam.newsmanagement.dao.oracledao;

import java.util.Set;

import org.junit.Assert;

import com.epam.newsmanagement.bean.News;
import com.epam.newsmanagement.dao.AbstractDAOTest;

/**
 * Class {@code NewsDAOTest} contains a number of methods that test the proper
 * functioning of C.R.U.D operations realized by NewsDAO methods.
 * 
 * @author Vasili Andreev
 * @version 1.0
 * @see com.epam.newsmanagement.dao.AbstractDAOTest
 * @see com.epam.newsmanagement.dao.oracledao.OracleNewsDAO
 */
public class NewsDAOTest extends AbstractDAOTest {
	private static final String Q_TABLE_NAME = "SYS.NEWS";
	private OracleNewsDAO newsDAO;

	/**
	 * Constructs NewsDAOTest object
	 */
	public NewsDAOTest() {
		this.newsDAO = getCtx().getBean(OracleNewsDAO.class);
		newsDAO.setTestMode(true);
	}

	/**
	 * Tests the correctness of loaded datasets.
	 * 
	 * @throws Exception
	 *             If Exception of some kind has occurred.
	 */
	public void testDataLoaded() throws Exception {
		int rows = 10;
		Assert.assertNotNull(getDataSet());
		int rowCount = getDataSet().getTable(Q_TABLE_NAME).getRowCount();
		Assert.assertEquals(rows, rowCount);
	}

	/**
	 * Tests findEntityByID, findNewsIDByTitle and findAll methods of NewsDAO.
	 * 
	 * @throws Exception
	 *             If exception of some kind has occurred.
	 */
	public void testExctractMethods() throws Exception {
		Long newsId = 4L;
		String newsTitle = "Gazprom";
		Long expectedId = 10L;
		Set<News> newsList = newsDAO.findAll();
		News news = newsDAO.findEntityById(newsId);
		Long id = newsDAO.findNewsIdByTitle(newsTitle);

		// tests the correctness of gained information
		Assert.assertNotNull(newsList);
		Assert.assertFalse(newsList.isEmpty());
		Assert.assertNotNull(news);
		Assert.assertEquals("Встреча Путина и Обамы", news.getTitle());
		Assert.assertEquals(expectedId, id);
	}

	/**
	 * Tests updateEntity method of NewsDAO.
	 * 
	 * @throws Exception
	 *             If exception of some kind has occurred.
	 */
	public void testUpdateMethod() throws Exception {
		boolean isUpdated = false;
		Long newsId = 13L;
		String oldTitle = "Xodus wins TCO";
		String newTitle = "Xodus";

		News news = newsDAO.findEntityById(newsId);
		Assert.assertNotNull(news);
		Assert.assertEquals(oldTitle, news.getTitle());

		/* updating information */
		news.setTitle(newTitle);
		isUpdated = newsDAO.updateEntity(news, newsId);
		News updNews = newsDAO.findEntityById(newsId);

		/* testing the correctness of action */
		Assert.assertNotNull(updNews);
		Assert.assertEquals(newTitle, updNews.getTitle());
		Assert.assertTrue(isUpdated);
	}

	/**
	 * Tests addEntity and deleteEntity methods of NewsDAO.
	 * 
	 * @throws Exception
	 *             If Exception of some kind has occurred.
	 */
	public void testAddAndDeleteMethods() throws Exception {
		Long newsId = null;
		boolean isAdded = false;
		boolean isDeleted = false;
		String dummyText = "Dummy text";

		News news = new News();
		news.setTitle(dummyText);
		news.setShortText(dummyText);
		news.setFullText(dummyText);

		isAdded = newsDAO.addNewEntity(news);
		for (News entity : newsDAO.findAll()) {
			if (dummyText.equals(entity.getTitle())) {
				newsId = entity.getNewsId();
			}
		}
		
		// tests the correctness of action
		News news1 = newsDAO.findEntityById(newsId);
		Assert.assertNotNull(news1);
		Assert.assertEquals(dummyText, news1.getTitle());
		Assert.assertEquals(dummyText, news1.getShortText());
		Assert.assertEquals(dummyText, news1.getFullText());
		isDeleted = newsDAO.deleteEntity(newsId);
		
		// tests the correctness of action
		news = newsDAO.findEntityById(newsId);
		Assert.assertNull(news);
		Assert.assertTrue(isAdded);
		Assert.assertTrue(isDeleted);
	}
}
