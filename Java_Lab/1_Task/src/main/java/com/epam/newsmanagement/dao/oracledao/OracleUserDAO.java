package com.epam.newsmanagement.dao.oracledao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.epam.newsmanagement.bean.User;
import com.epam.newsmanagement.dao.AbstractDAO;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.util.DBUtils;

/**
 * Class {@code OracleUserDAO} contains methods allowing to extract information
 * about Users found in the application, add, update, and delete their data.
 * 
 * @author Vasili Andreev
 * @version 1.0
 * @see com.epam.newsmanagement.bean.User
 * @see com.epam.newsmanagement.dao.AbstractDAO
 */
@Component
@Scope("prototype")
public class OracleUserDAO extends AbstractDAO<User> {
	private static final String USER_ID = "USER_ID";
	private static final String USER_NAME = "USER_NAME";
	private static final String LOGIN = "LOGIN";
	private static final String PASS = "PASS";
	private static final String ROLE = "ROLE_NAME";
	private static final String SQL_FIND_USERS_QUERY = "SELECT USER_ID, USER_NAME, LOGIN, PASS FROM USERS";
	private static final String SQL_FIND_USER_BY_ID_QUERY = "SELECT USER_ID, USER_NAME, LOGIN, PASS FROM USERS"
			+ " WHERE USER_ID = ?";
	private static final String SQL_FIND_USER_ROLE_QUERY = "SELECT ROLE_NAME FROM USER_ROLES WHERE USER_ID = ?";
	private static final String SQL_ADD_USER_QUERY = "INSERT INTO USERS (USER_NAME, LOGIN, PASS) VALUES (?, ?, ?)";
	private static final String SQL_ADD_USER_ROLE_QUERY = "INSERT INTO USER_ROLES (USER_ID, ROLE_NAME) VALUES (?, ?)";
	private static final String SQL_UPDATE_USER_QUERY = "UPDATE USERS SET USER_NAME = ?, LOGIN = ?,"
			+ " PASS = ? WHERE USER_ID = ?";
	private static final String SQL_UPDATE_USER_ROLE_QUERY = "UPDATE USER_ROLES SET ROLE_NAME = ? WHERE USER_ID = ?";

	/* finds all Users in the application */
	@Override
	public Set<User> findAll() throws DAOException {
		ResultSet rs = null;
		PreparedStatement prepStatement = null;
		Set<User> usersList = new HashSet<User>();
		Connection conn = getConnection();

		try {
			prepStatement = conn.prepareStatement(SQL_FIND_USERS_QUERY);
			rs = prepStatement.executeQuery();
			while (rs.next()) {
				/* creating a new User bean and initializing its fields */
				User user = new User();
				user.setUserId(rs.getLong(USER_ID));
				user.setUserName(rs.getString(USER_NAME));
				user.setLogin(rs.getString(LOGIN));
				user.setPassword(rs.getString(PASS));
				usersList.add(user);
			}
		} catch (SQLException ex) {
			throw new DAOException("Database error", ex);
		} finally {
			DBUtils.close(rs, prepStatement, conn);
		}
		return usersList;
	}

	/* returns User with the given ID */
	@Override
	public User findEntityById(Long userId) throws DAOException {
		User user = null;
		ResultSet rs = null;
		PreparedStatement prepStatement = null;
		Connection conn = getConnection();
		
		try {
			prepStatement = conn.prepareStatement(SQL_FIND_USER_BY_ID_QUERY);
			prepStatement.setLong(1, userId);
			rs = prepStatement.executeQuery();
			while (rs.next()) {
				/* creating a new User bean and initializing its fields */
				user = new User();
				user.setUserId(rs.getLong(USER_ID));
				user.setUserName(rs.getString(USER_NAME));
				user.setLogin(rs.getString(LOGIN));
				user.setPassword(rs.getString(PASS));
			}
		} catch (SQLException ex) {
			throw new DAOException("Database error", ex);
		} finally {
			DBUtils.close(rs, prepStatement, conn);
		}
		return user;
	}

	/**
	 * Finds User's role name.
	 * 
	 * @param userId
	 *            The ID of the User
	 * @return the role of the given User
	 * @throws DAOException
	 *             If a database access/handling error occurs.
	 */
	public String findUserRole(Long userId) throws DAOException {
		String roleName = null;
		ResultSet rs = null;
		PreparedStatement prepStatement = null;
		Connection conn = getConnection();

		try {
			prepStatement = conn.prepareStatement(SQL_FIND_USER_ROLE_QUERY);
			prepStatement.setLong(1, userId);
			rs = prepStatement.executeQuery();
			while (rs.next()) {
				roleName = rs.getString(ROLE);
			}
		} catch (SQLException ex) {
			throw new DAOException("Database error", ex);
		} finally {
			DBUtils.close(rs, prepStatement, conn);
		}
		return roleName;
	}

	/* adds new User to the database */
	@Override
	public boolean addNewEntity(User user) throws DAOException {
		PreparedStatement prepStatement = null;
		boolean isAdded = false;
		Connection conn = getConnection();

		try {
			prepStatement = conn.prepareStatement(SQL_ADD_USER_QUERY);
			prepStatement.setString(1, user.getUserName());
			prepStatement.setString(2, user.getLogin());
			prepStatement.setString(3, user.getPassword());
			prepStatement.executeUpdate();
			isAdded = true;
		} catch (SQLException ex) {
			throw new DAOException("Database error", ex);
		} finally {
			DBUtils.close(prepStatement, conn);
		}
		return isAdded;
	}

	/**
	 * Adds User's role name.
	 * 
	 * @param userId
	 *            The ID of the User
	 * @param roleName
	 *            The role name of the User
	 * @return {@code true} if the role has been successfully added and
	 *         {@code false} otherwise
	 * @throws DAOException
	 *             If a database access/handling error occurs.
	 */
	public boolean addUserRole(Long userId, String roleName)
			throws DAOException {
		PreparedStatement prepStatement = null;
		boolean isAdded = false;
		Connection conn = getConnection();

		try {
			prepStatement = conn.prepareStatement(SQL_ADD_USER_ROLE_QUERY);
			prepStatement.setLong(1, userId);
			prepStatement.setString(2, roleName);
			prepStatement.executeUpdate();
			isAdded = true;
		} catch (SQLException ex) {
			throw new DAOException("Database error", ex);
		} finally {
			DBUtils.close(prepStatement, conn);
		}
		return isAdded;
	}

	/* updates User's data */
	@Override
	public boolean updateEntity(User user, Long userId) throws DAOException {
		PreparedStatement prepStatement = null;
		boolean isUpdated = false;
		Connection conn = getConnection();

		try {
			prepStatement = conn.prepareStatement(SQL_UPDATE_USER_QUERY);
			prepStatement.setString(1, user.getUserName());
			prepStatement.setString(2, user.getLogin());
			prepStatement.setString(3, user.getPassword());
			prepStatement.setLong(4, userId);
			prepStatement.executeUpdate();
			isUpdated = true;
		} catch (SQLException ex) {
			throw new DAOException("Database error", ex);
		} finally {
			DBUtils.close(prepStatement, conn);
		}
		return isUpdated;
	}

	/**
	 * Updates User's role name.
	 * 
	 * @param roleName
	 *            The role name of the User
	 * @param userId
	 *            The ID of the User
	 * @return {@code true} if the role has been successfully assigned and
	 *         {@code false} otherwise
	 * @throws DAOException
	 *             If a database access/handling error occurs.
	 */
	public boolean updateUserRole(String roleName, Long userId)
			throws DAOException {
		PreparedStatement prepStatement = null;
		boolean isUpdated = false;
		Connection conn = getConnection();

		try {
			prepStatement = conn.prepareStatement(SQL_UPDATE_USER_ROLE_QUERY);
			prepStatement.setString(1, roleName);
			prepStatement.setLong(2, userId);
			prepStatement.executeUpdate();
			isUpdated = true;
		} catch (SQLException ex) {
			throw new DAOException("Database error", ex);
		} finally {
			DBUtils.close(prepStatement, conn);
		}
		return isUpdated;
	}

	/* there is no need in such method in the application */
	@Override
	public boolean deleteEntity(Long userId) {
		throw new UnsupportedOperationException(
				"Error. This operation is not supported!");
	}
}
