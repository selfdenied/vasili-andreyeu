package com.epam.newsmanagement.search.news;

import static java.text.MessageFormat.format;

import com.epam.newsmanagement.bean.NewsVO;
import com.epam.newsmanagement.search.SearchResults;

/**
 * Class {@code NewsSearchResults} contains the information about the news
 * search results and the List of news found.
 * 
 * @author Vasili Andreev
 * @version 1.0
 * @see com.epam.newsmanagement.search.SearchResults
 * @see com.epam.newsmanagement.bean.NewsVO
 */
public class NewsSearchResults extends SearchResults<NewsVO> {

	/**
	 * Returns the message when displaying news search results.
	 * 
	 * @return the message when displaying news search results
	 */
	public static String getDataGridMessage(int start, int end, int total) {
		return format("Displaying {0} to {1} News of {2}", start, end, total);
	}
}
