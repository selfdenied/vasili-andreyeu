package com.epam.newsmanagement.search;

/**
 * Abstract class {@code SearchCriteria} contains a number of fields used to
 * control the process of search results displaying (like sort order or number
 * of results displayed per page).
 * 
 * @author Vasili Andreev
 * @version 1.0
 */
public abstract class SearchCriteria {
	private int resultsPerPage = 20;
	private SortOrder sortOrder;

	/**
	 * Enum {@code SortOrder} contains the list of constants, according to which
	 * a sort order of search results will be displayed.
	 * 
	 * @author Vasili Andreev
	 * @version 1.0
	 */
	public enum SortOrder {
		ASC, DESC;
	}

	/**
	 * Returns the number of search results displayed per page.
	 * 
	 * @return the number of results per page
	 */
	public int getResultsPerPage() {
		return resultsPerPage;
	}

	/**
	 * Sets the number of search results displayed per page.
	 * 
	 * @param resultsPerPage
	 *            the number of results per page
	 */
	public void setResultsPerPage(int resultsPerPage) {
		this.resultsPerPage = resultsPerPage;
	}

	/**
	 * Returns the sort order of results displayed.
	 * 
	 * @return the sort order of results displayed
	 */
	public SortOrder getSortOrder() {
		return sortOrder;
	}

	/**
	 * Sets the sort order of results displayed.
	 * 
	 * @param sortOrder
	 *            the sort order of results displayed
	 */
	public void setSortOrder(SortOrder sortOrder) {
		this.sortOrder = sortOrder;
	}
}
