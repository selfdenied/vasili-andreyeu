package com.epam.newsmanagement.service;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.bean.*;
import com.epam.newsmanagement.dao.oracledao.*;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.search.news.NewsSearchCriteria;
import com.epam.newsmanagement.search.news.NewsSearchCriteria.AuthorSearchType;
import com.epam.newsmanagement.search.news.NewsSearchResults;

/**
 * Class {@code NewsService} contains methods that use DAO layer to perform
 * various operations with news messages (add/delete news to/from the database,
 * update news data, count news, etc.).
 * 
 * @author Vasili Andreev
 * @version 1.0
 * @see com.epam.newsmanagement.dao.oracledao.OracleNewsDAO
 * @see com.epam.newsmanagement.bean.NewsVO
 */
@Component
@Scope("prototype")
public class NewsService {
	@Autowired
	private OracleNewsDAO newsDAO;
	@Autowired
	private OracleCommentDAO commentDAO;
	@Autowired
	private OracleTagDAO tagDAO;
	@Autowired
	private OracleAuthorDAO authorDAO;
	
	/*
	 * This comparator sorts news messages according to the number of comments
	 * written and by their modification date
	 */
	private class CommentComparator implements Comparator<NewsVO> {
		@Override
		public int compare(NewsVO message1, NewsVO message2) {
			int size1 = message1.getListOfComments().size();
			int size2 = message2.getListOfComments().size();
			Date date1 = message1.getNewsMessage().getModDate();
			Date date2 = message2.getNewsMessage().getModDate();

			if (size2 > size1) {
				return 1;
			} else if (size2 < size1) {
				return -1;
			} else {
				return date2.compareTo(date1);
			}
		}
	}

	/*
	 * This comparator sorts comments according to their creation date
	 */
	private class DateComparator implements Comparator<Comment> {
		@Override
		public int compare(Comment comment1, Comment comment2) {
			return comment1.getCreationDate().compareTo(
					comment2.getCreationDate());
		}
	}
	
	/**
	 * Sets the NewsDAO dependency.
	 * 
	 * @param newsDAO
	 *            the instance of NewsDAO
	 */
	public void setNewsDAO(OracleNewsDAO newsDAO) {
		this.newsDAO = newsDAO;
	}

	/**
	 * Sets the CommentDAO dependency.
	 * 
	 * @param commentDAO
	 *            the instance of CommentDAO
	 */
	public void setCommentDAO(OracleCommentDAO commentDAO) {
		this.commentDAO = commentDAO;
	}

	/**
	 * Sets the TagDAO dependency.
	 * 
	 * @param tagDAO
	 *            the instance of TagDAO
	 */
	public void setTagDAO(OracleTagDAO tagDAO) {
		this.tagDAO = tagDAO;
	}

	/**
	 * Sets the AuthorDAO dependency.
	 * 
	 * @param authorDAO
	 *            the instance of AuthorDAO
	 */
	public void setAuthorDAO(OracleAuthorDAO authorDAO) {
		this.authorDAO = authorDAO;
	}

	/**
	 * Counts all news messages available in the application.
	 * 
	 * @return The number of news messages
	 * @throws DAOException
	 *             If a DAO exception of some sort has occurred
	 */
	public int countNews() throws DAOException {
		Set<News> newsList = new HashSet<>();

		newsList = newsDAO.findAll();
		return newsList.size();
	}

	/**
	 * Returns the list of all News messages found in the application. The list
	 * of Tags and Comments assigned to the News message are set as well. The
	 * News messages are sorted according to the number of comments written for
	 * each message.
	 * 
	 * @return The sorted list of all News messages
	 * @throws DAOException
	 *             If a DAO exception of some sort has occurred
	 * @see com.epam.newsmanagement.bean.NewsVO
	 */
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED, rollbackFor = DAOException.class)
	public List<NewsVO> obtainAllNewsSorted() throws DAOException {
		List<NewsVO> newsVOList = new ArrayList<>();

		Set<News> tempList = newsDAO.findAll();
		for (News message : tempList) {
			Long newsId = message.getNewsId();
			NewsVO vo = new NewsVO();
			vo.setNewsMessage(message);
			vo.setAuthor(authorDAO.findAuthorByNewsId(newsId));
			vo.setListOfComments(commentDAO.findCommentsByNewsId(newsId));
			vo.setListOfTags(tagDAO.findAllTagsByNewsId(newsId));
			newsVOList.add(vo);
		}
		Collections.sort(newsVOList, new CommentComparator());
		return newsVOList;
	}

	/**
	 * Returns the News message with the given ID.
	 * 
	 * @param newsId
	 *            The ID of the News message
	 * @return News message with the given ID
	 * @throws DAOException
	 *             If a DAO exception of some sort has occurred
	 * @see com.epam.newsmanagement.bean.NewsVO
	 */
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED, rollbackFor = DAOException.class)
	public NewsVO obtainNewsMessage(Long newsId) throws DAOException {
		NewsVO vo = null;

		News message = newsDAO.findEntityById(newsId);
		if (message != null) {
			vo = new NewsVO();
			vo.setNewsMessage(message);
			vo.setAuthor(authorDAO.findAuthorByNewsId(newsId));
			vo.setListOfComments(commentDAO.findCommentsByNewsId(newsId));
			vo.setListOfTags(tagDAO.findAllTagsByNewsId(newsId));
		}
		return vo;
	}

	/**
	 * Returns the sorted list of Comments for the given News message.
	 * 
	 * @param newsId
	 *            The ID of the News message
	 * @return the sorted list of Comments
	 * @throws DAOException
	 *             If a DAO exception of some sort has occurred
	 */
	public List<Comment> obtainNewsCommentsSorted(Long newsId)
			throws DAOException {
		List<Comment> commentsList = new ArrayList<>();

		commentsList.addAll(commentDAO.findCommentsByNewsId(newsId));
		Collections.sort(commentsList, new DateComparator());
		return commentsList;
	}

	/**
	 * Method adds up-to-date News message to the database. An Author with the
	 * given ID and the selected Tags are saved as well. This set of operations
	 * is performed in one step.
	 * 
	 * @param news
	 *            com.epam.newsmanagement.bean.News
	 * @param authorId
	 *            the ID of the News Author
	 * @param listOfTagIds
	 *            The list of Tags IDs
	 * @return {@code true} when News message has been successfully added and
	 *         {@code false} otherwise
	 * @throws DAOException
	 *             If a DAO exception of some sort has occurred
	 */
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = DAOException.class)
	public boolean addNewsMessage(News news, Long authorId,
			Set<Long> listOfTagIds) throws DAOException {
		boolean isAdded = false;

		/* this adds News message */
		isAdded = newsDAO.addNewEntity(news);
		/* get the ID of the added News message */
		Long newsId = newsDAO.findNewsIdByTitle(news.getTitle());
		/* this assigns the author */
		isAdded = authorDAO.assignAuthorToNews(newsId, authorId);
		/* this assigns all tags to the newly created news message */
		for (Long tagId : listOfTagIds) {
			isAdded = tagDAO.assignTagToNews(newsId, tagId);
		}
		return isAdded;
	}

	/**
	 * Method updates data of the given News message.
	 * 
	 * @param news
	 *            com.epam.newsmanagement.bean.News
	 * @param newsId
	 *            The ID of the News message to be updated
	 * @return {@code true} when News message has been successfully updated and
	 *         {@code false} otherwise
	 * @throws DAOException
	 *             If a DAO exception of some sort has occurred
	 */
	public boolean updateNewsMessage(News news, Long newsId)
			throws DAOException {
		boolean isUpdated = false;

		isUpdated = newsDAO.updateEntity(news, newsId);
		return isUpdated;
	}

	/**
	 * Method deletes the News message with the given ID from the database.
	 * 
	 * @param newsId
	 *            the ID of the News message to be deleted
	 * @return {@code true} when News message has been successfully deleted and
	 *         {@code false} otherwise
	 * @throws DAOException
	 *             If a DAO exception of some sort has occurred
	 */
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = DAOException.class)
	public boolean deleteNewsMessage(Long newsId) throws DAOException {
		boolean isDeleted = false;

		/* delete associated comments */
		isDeleted = commentDAO.deassignCommentsFromNews(newsId);
		/* delete associated tags */
		isDeleted = tagDAO.deassignTagsFromNews(newsId);
		/* delete associated author */
		isDeleted = authorDAO.deassignAuthorFromNews(newsId);
		/* delete the news message itself */
		isDeleted = newsDAO.deleteEntity(newsId);
		return isDeleted;
	}

	/**
	 * Searches DB for News messages according to the given search criteria
	 * (Author name/ID, tags, etc.).
	 * 
	 * @param author
	 *            the Author of News message
	 * @param tagsNames
	 *            the list of News Tags Names
	 * @param authorSearch
	 *            the type of search (according to the Author's name or ID)
	 * @param resultsPerPage
	 *            the desired number of results displayed per page
	 * @return The results of News search (the list of found News messages)
	 * @throws DAOException
	 *             If a DAO exception of some sort has occurred
	 */
	public NewsSearchResults searchNews(Author author, Set<String> tagsNames,
			AuthorSearchType authorSearch, int resultsPerPage)
			throws DAOException {
		NewsSearchCriteria searchCriteria = new NewsSearchCriteria();
		NewsSearchResults searchResults = new NewsSearchResults();

		if (author != null) {
			searchCriteria.setAuthorId(author.getAuthorId());
			searchCriteria.setAuthorName(author.getAuthorName());
		}
		searchCriteria.setAuthorSearch(authorSearch);
		searchCriteria.setTagsNames(tagsNames);
		searchResults.setResultsPerPage(resultsPerPage);
		searchResults.setResults(findNews(searchCriteria));
		return searchResults;
	}

	/* searches DB for news according to the given search criteria */
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED, rollbackFor = DAOException.class)
	private List<NewsVO> findNews(NewsSearchCriteria searchCriteria)
			throws DAOException {
		List<NewsVO> newsVOList = new ArrayList<>();

		Set<News> tempList = newsDAO.searchNewsByAuthorAndTags(searchCriteria);
		for (News message : tempList) {
			Long newsId = message.getNewsId();
			NewsVO vo = new NewsVO();
			vo.setNewsMessage(message);
			vo.setAuthor(authorDAO.findAuthorByNewsId(newsId));
			vo.setListOfComments(commentDAO.findCommentsByNewsId(newsId));
			vo.setListOfTags(tagDAO.findAllTagsByNewsId(newsId));
			newsVOList.add(vo);
		}
		Collections.sort(newsVOList, new CommentComparator());
		return newsVOList;
	}
}
