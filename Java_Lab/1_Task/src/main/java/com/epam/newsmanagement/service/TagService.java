package com.epam.newsmanagement.service;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.bean.Tag;
import com.epam.newsmanagement.dao.oracledao.OracleTagDAO;
import com.epam.newsmanagement.exception.DAOException;

/**
 * Class {@code TagService} contains methods that use DAO layer to retrieve
 * information about the Tags from a database, add, update, and delete Tags.
 * 
 * @author Vasili Andreev
 * @version 1.0
 * @see com.epam.newsmanagement.dao.oracledao.OracleTagDAO
 */
@Component
@Scope("prototype")
public class TagService {
	@Autowired
	private OracleTagDAO tagDAO;

	/**
	 * Sets the TagDAO dependency.
	 * 
	 * @param tagDAO
	 *            the instance of TagDAO
	 */
	public void setTagDAO(OracleTagDAO tagDAO) {
		this.tagDAO = tagDAO;
	}
	
	/**
	 * Returns the set of all Tags found in the application.
	 * 
	 * @return The set of all Tags found in the application
	 * @throws DAOException
	 *             If a DAO exception of some sort has occurred
	 */
	public Set<Tag> findAllTags() throws DAOException {
		Set<Tag> tagsList = new HashSet<>();
		
		tagsList = tagDAO.findAll();
		return tagsList;
	}

	/**
	 * Method adds new Tag to the database
	 * 
	 * @param tag
	 *            com.epam.newsmanagement.bean.Tag
	 * @return {@code true} when Tag has been successfully added and
	 *         {@code false} otherwise
	 * @throws DAOException
	 *             If a DAO exception of some sort has occurred
	 */
	public boolean addNewTag(Tag tag) throws DAOException {
		boolean isAdded = false;

		isAdded = tagDAO.addNewEntity(tag);
		return isAdded;
	}

	/**
	 * Method updates Tag's data
	 * 
	 * @param tag
	 *            com.epam.newsmanagement.bean.Tag
	 * @param tagId
	 *            the ID of the Tag to be updated
	 * @return {@code true} when Tag has been successfully updated and
	 *         {@code false} otherwise
	 * @throws DAOException
	 *             If a DAO exception of some sort has occurred
	 */
	public boolean updateTag(Tag tag, Long tagId) throws DAOException {
		boolean isUpdated = false;
		
		isUpdated = tagDAO.updateEntity(tag, tagId);
		return isUpdated;
	}

	/**
	 * Method deletes the selected Tag
	 * 
	 * @param tagId
	 *            the ID of the Tag to be deleted
	 * @return {@code true} when Tag has been successfully deleted and
	 *         {@code false} otherwise
	 * @throws DAOException
	 *             If a DAO exception of some sort has occurred
	 */
	@Transactional(propagation = Propagation.MANDATORY, rollbackFor = DAOException.class)
	public boolean deleteTag(Long tagId) throws DAOException {
		boolean isDeleted = false;

		isDeleted = tagDAO.deleteEntity(tagId);
		return isDeleted;
	}

	/**
	 * Method updates Tags of the selected News message
	 * 
	 * @param newsId
	 *            the ID of the News message
	 * @param tagsIds
	 *            the Set of Tags' IDs to be assigned to message
	 * @return {@code true} when Tags have been successfully updated and
	 *         {@code false} otherwise
	 * @throws DAOException
	 *             If a DAO exception of some sort has occurred
	 */
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = DAOException.class)
	public boolean updateNewsTags(Long newsId, Set<Long> tagsIds)
			throws DAOException {
		boolean isUpdated = false;

		isUpdated = tagDAO.deassignTagsFromNews(newsId);
		for (Long tagId : tagsIds) {
			isUpdated = tagDAO.assignTagToNews(newsId, tagId);
		}
		return isUpdated;
	}
}
