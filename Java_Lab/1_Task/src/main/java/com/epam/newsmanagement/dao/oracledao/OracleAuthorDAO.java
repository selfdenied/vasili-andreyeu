package com.epam.newsmanagement.dao.oracledao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;
import java.util.TimeZone;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.epam.newsmanagement.bean.Author;
import com.epam.newsmanagement.dao.AbstractDAO;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.util.DBUtils;

/**
 * Class {@code OracleAuthorDAO} contains methods allowing to extract
 * information about Authors found in the application, add, update, and delete
 * their data.
 * 
 * @author Vasili Andreev
 * @version 1.0
 * @see com.epam.newsmanagement.bean.Author
 * @see com.epam.newsmanagement.dao.AbstractDAO
 */
@Component
@Scope("prototype")
public class OracleAuthorDAO extends AbstractDAO<Author> {
	private static final String AUTHOR_ID = "AUTHOR_ID";
	private static final String AUTHOR_NAME = "AUTHOR_NAME";
	private static final String EXPRIRED = "EXPIRED";
	private static final String SQL_FIND_AUTHORS_QUERY = "SELECT AUTHOR_ID, AUTHOR_NAME, EXPIRED FROM AUTHORS";
	private static final String SQL_FIND_AUTHOR_BY_ID_QUERY = "SELECT AUTHOR_ID, AUTHOR_NAME, EXPIRED FROM"
			+ " AUTHORS WHERE AUTHOR_ID = ?";
	private static final String SQL_FIND_AUTHOR_BY_NEWS_ID_QUERY = "SELECT AUTHORS.AUTHOR_ID, AUTHORS.AUTHOR_NAME,"
			+ " AUTHORS.EXPIRED FROM AUTHORS JOIN NEWS_AUTHORS ON AUTHORS.AUTHOR_ID = NEWS_AUTHORS.AUTHOR_ID"
			+ " WHERE NEWS_AUTHORS.NEWS_ID = ?";
	private static final String SQL_ADD_AUTHOR_QUERY = "INSERT INTO AUTHORS (AUTHOR_NAME) VALUES (?)";
	private static final String SQL_ASSIGN_AUTHOR_QUERY = "INSERT INTO NEWS_AUTHORS (NEWS_ID, AUTHOR_ID)"
			+ " VALUES (?, ?)";
	private static final String SQL_DEASSIGN_AUTHOR_QUERY = "DELETE FROM NEWS_AUTHORS WHERE NEWS_ID = ?";
	private static final String SQL_UPDATE_AUTHOR_QUERY = "UPDATE AUTHORS SET AUTHOR_NAME = ?, EXPIRED = ?"
			+ " WHERE AUTHOR_ID = ?";
	private static final String SQL_DELETE_AUTHOR_QUERY = "UPDATE AUTHORS SET EXPIRED = ? WHERE AUTHOR_ID = ?";

	/* finds all Authors in the application */
	@Override
	public Set<Author> findAll() throws DAOException {
		ResultSet rs = null;
		PreparedStatement prepStatement = null;
		Connection conn = getConnection();
		Set<Author> authorsList = new HashSet<Author>();

		try {
			prepStatement = conn.prepareStatement(SQL_FIND_AUTHORS_QUERY);
			rs = prepStatement.executeQuery();
			while (rs.next()) {
				/* creating a new Author bean and initializing its fields */
				Author author = new Author();
				author.setAuthorId(rs.getLong(AUTHOR_ID));
				author.setAuthorName(rs.getString(AUTHOR_NAME));
				if (rs.getTimestamp(EXPRIRED) != null) {
					author.setExpired(rs.getTimestamp(EXPRIRED));
				}
				authorsList.add(author);
			}
		} catch (SQLException ex) {
			throw new DAOException("Database error", ex);
		} finally {
			DBUtils.close(rs, prepStatement, conn);
		}
		return authorsList;
	}

	/* returns Author with the given ID */
	@Override
	public Author findEntityById(Long authorId) throws DAOException {
		Author author = null;
		ResultSet rs = null;
		PreparedStatement prepStatement = null;
		Connection conn = getConnection();

		try {
			prepStatement = conn.prepareStatement(SQL_FIND_AUTHOR_BY_ID_QUERY);
			prepStatement.setLong(1, authorId);
			rs = prepStatement.executeQuery();
			while (rs.next()) {
				/* creating a new Author bean and initializing its fields */
				author = new Author();
				author.setAuthorId(rs.getLong(AUTHOR_ID));
				author.setAuthorName(rs.getString(AUTHOR_NAME));
				if (rs.getTimestamp(EXPRIRED) != null) {
					author.setExpired(rs.getTimestamp(EXPRIRED));
				}
			}
		} catch (SQLException ex) {
			throw new DAOException("Database error", ex);
		} finally {
			DBUtils.close(rs, prepStatement, conn);
		}
		return author;
	}
	
	/**
	 * Finds the Author associated with the given news message.
	 * 
	 * @param newsId
	 *            The ID of the news message
	 * @return The Author associated with the news message
	 * @throws DAOException
	 *             If a database access/handling error occurs.
	 */
	public Author findAuthorByNewsId(Long newsId) throws DAOException {
		Author author = null;
		ResultSet rs = null;
		PreparedStatement prepStatement = null;
		Connection conn = getConnection();

		try {
			prepStatement = conn.prepareStatement(SQL_FIND_AUTHOR_BY_NEWS_ID_QUERY);
			prepStatement.setLong(1, newsId);
			rs = prepStatement.executeQuery();
			while (rs.next()) {
				/* creating a new Author bean and initializing its fields */
				author = new Author();
				author.setAuthorId(rs.getLong(AUTHOR_ID));
				author.setAuthorName(rs.getString(AUTHOR_NAME));
				if (rs.getTimestamp(EXPRIRED) != null) {
					author.setExpired(rs.getTimestamp(EXPRIRED));
				}
			}
		} catch (SQLException ex) {
			throw new DAOException("Database error", ex);
		} finally {
			DBUtils.close(rs, prepStatement, conn);
		}
		return author;
	}

	/* adds new Author to the database */
	@Override
	public boolean addNewEntity(Author author) throws DAOException {
		PreparedStatement prepStatement = null;
		boolean isAdded = false;
		Connection conn = getConnection();

		try {
			prepStatement = conn.prepareStatement(SQL_ADD_AUTHOR_QUERY);
			prepStatement.setString(1, author.getAuthorName());
			prepStatement.executeUpdate();
			isAdded = true;
		} catch (SQLException ex) {
			throw new DAOException("Database error", ex);
		} finally {
			DBUtils.close(prepStatement, conn);
		}
		return isAdded;
	}

	/**
	 * Assigns an Author with the given ID to the news message with the given
	 * ID.
	 * 
	 * @param authorId
	 *            The ID of the Author
	 * @param newsId
	 *            The ID of the News message
	 * @return {@code true} if the Author has been successfully assigned and
	 *         {@code false} otherwise
	 * @throws DAOException
	 *             If a database access/handling error occurs.
	 */
	public boolean assignAuthorToNews(Long newsId, Long authorId)
			throws DAOException {
		PreparedStatement prepStatement = null;
		boolean isAssigned = false;
		Connection conn = getConnection();

		try {
			prepStatement = conn.prepareStatement(SQL_ASSIGN_AUTHOR_QUERY);
			prepStatement.setLong(1, newsId);
			prepStatement.setLong(2, authorId);
			prepStatement.executeUpdate();
			isAssigned = true;
		} catch (SQLException ex) {
			throw new DAOException("Database error", ex);
		} finally {
			DBUtils.close(prepStatement, conn);
		}
		return isAssigned;
	}

	/**
	 * Deassigns an Author from the news message with the given ID.
	 * 
	 * @param newsId
	 *            The ID of the News message
	 * @return {@code true} if the Author has been successfully deassigned and
	 *         {@code false} otherwise
	 * @throws DAOException
	 *             If a database access/handling error occurs.
	 */
	public boolean deassignAuthorFromNews(Long newsId) throws DAOException {
		PreparedStatement prepStatement = null;
		boolean isDeAssigned = false;
		Connection conn = getConnection();

		try {
			prepStatement = conn.prepareStatement(SQL_DEASSIGN_AUTHOR_QUERY);
			prepStatement.setLong(1, newsId);
			prepStatement.executeUpdate();
			isDeAssigned = true;
		} catch (SQLException ex) {
			throw new DAOException("Database error", ex);
		} finally {
			DBUtils.close(prepStatement, conn);
		}
		return isDeAssigned;
	}

	/* updates Author's data */
	@Override
	public boolean updateEntity(Author author, Long authorId)
			throws DAOException {
		PreparedStatement prepStatement = null;
		boolean isUpdated = false;
		Connection conn = getConnection();

		try {
			prepStatement = conn.prepareStatement(SQL_UPDATE_AUTHOR_QUERY);
			prepStatement.setString(1, author.getAuthorName());
			if (author.getExpired() != null) {
				prepStatement.setTimestamp(2, obtainTimestamp());
			} else {
				prepStatement.setNull(2, Types.TIMESTAMP);
			}
			prepStatement.setLong(3, authorId);
			prepStatement.executeUpdate();
			isUpdated = true;
		} catch (SQLException ex) {
			throw new DAOException("Database error", ex);
		} finally {
			DBUtils.close(prepStatement, conn);
		}
		return isUpdated;
	}

	/* Deletes Author's data */
	@Override
	public boolean deleteEntity(Long authorId) throws DAOException {
		PreparedStatement prepStatement = null;
		boolean isDeleted = false;
		Connection conn = getConnection();

		try {
			prepStatement = conn.prepareStatement(SQL_DELETE_AUTHOR_QUERY);
			prepStatement.setTimestamp(1, obtainTimestamp());
			prepStatement.setLong(2, authorId);
			prepStatement.executeUpdate();
			isDeleted = true;
		} catch (SQLException ex) {
			throw new DAOException("Database error", ex);
		} finally {
			DBUtils.close(prepStatement, conn);
		}
		return isDeleted;
	}

	/* Gets the current Timestamp */
	private Timestamp obtainTimestamp() {
		TimeZone zone = TimeZone.getTimeZone("GMT+3");
		Calendar calendar = Calendar.getInstance(zone, Locale.getDefault());
		Date date = calendar.getTime();
		return new Timestamp(date.getTime());
	}
}
