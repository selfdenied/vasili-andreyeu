package com.epam.newsmanagement.service;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.epam.newsmanagement.bean.Author;
import com.epam.newsmanagement.dao.oracledao.OracleAuthorDAO;
import com.epam.newsmanagement.exception.DAOException;

/**
 * Class {@code AuthorService} contains methods that use DAO layer to retrieve
 * information about the Authors from a database, add, update or delete Authors.
 * 
 * @author Vasili Andreev
 * @version 1.0
 * @see com.epam.newsmanagement.dao.oracledao.OracleAuthorDAO
 */
@Component
@Scope("prototype")
public class AuthorService {
	@Autowired
	private OracleAuthorDAO authorDAO;

	/**
	 * Sets the AuthorDAO dependency.
	 * 
	 * @param authorDAO
	 *            the instance of AuthorDAO
	 */
	public void setAuthorDAO(OracleAuthorDAO authorDAO) {
		this.authorDAO = authorDAO;
	}
	
	/**
	 * Returns the set of Authors found in the application.
	 * 
	 * @return The set of Authors found in the application
	 * @throws DAOException
	 *             If a DAO exception of some sort has occurred
	 */
	public Set<Author> findAllAuthors() throws DAOException {
		Set<Author> authorsList = new HashSet<>();
		
		authorsList = authorDAO.findAll();
		return authorsList;
	}

	/**
	 * Returns the set of active Authors in the application.
	 * 
	 * @return The set of active Authors in the application
	 * @throws DAOException
	 *             If a DAO exception of some sort has occurred
	 */
	public Set<Author> findAllActiveAuthors() throws DAOException {
		Set<Author> authorsList = new HashSet<>();
		
		Set<Author> tempList = authorDAO.findAll();
			for (Author author : tempList) {
				if (author.getExpired() == null) {
					authorsList.add(author);
				}
			}
		return authorsList;
	}

	/**
	 * Adds new Author to the database.
	 * 
	 * @param author
	 *            com.epam.newsmanagement.bean.Author
	 * @return {@code true} when Author has been successfully added and
	 *         {@code false} otherwise
	 * @throws DAOException
	 *             If a DAO exception of some sort has occurred
	 */
	public boolean addNewAuthor(Author author) throws DAOException {
		boolean isAdded = false;

		isAdded = authorDAO.addNewEntity(author);
		return isAdded;
	}

	/**
	 * Method updates Author's data.
	 * 
	 * @param author
	 *            com.epam.newsmanagement.bean.Author
	 * @param authorId
	 *            The ID of the Author to be updated
	 * @return {@code true} when Author has been successfully updated and
	 *         {@code false} otherwise
	 * @throws DAOException
	 *             If a DAO exception of some sort has occurred
	 */
	public boolean updateAuthor(Author author, Long authorId)
			throws DAOException {
		boolean isUpdated = false;

		isUpdated = authorDAO.updateEntity(author, authorId);
		return isUpdated;
	}

	/**
	 * Method deletes the Author (sets his status to 'Expired').
	 * 
	 * @param authorId
	 *            The ID of the Author to be deleted
	 * @return {@code true} when Author has been successfully deleted and
	 *         {@code false} otherwise
	 * @throws DAOException
	 *             If a DAO exception of some sort has occurred
	 */
	public boolean deleteAuthor(Long authorId) throws DAOException {
		boolean isDeleted = false;

		isDeleted = authorDAO.deleteEntity(authorId);
		return isDeleted;
	}
}
