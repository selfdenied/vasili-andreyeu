package com.epam.newsmanagement.search;

import java.util.ArrayList;
import java.util.List;

/**
 * Abstract class {@code SearchResults} contains the information about the
 * search results of generic type T and the List of results itself.
 * 
 * @author Vasili Andreev
 * @version 1.0
 */
public abstract class SearchResults<T> {
	private int totalResults;
	private int resultsPerPage = 20;
	private List<T> results;

	/**
	 * Returns the total number of results obtained.
	 * 
	 * @return the total number of results obtained
	 */
	public int getTotalResults() {
		return totalResults;
	}

	/**
	 * Returns the number of results displayed per page.
	 * 
	 * @return the number of results displayed per page
	 */
	public int getResultsPerPage() {
		return resultsPerPage;
	}

	/**
	 * Sets the number of results displayed per page.
	 * 
	 * @param resultsPerPage
	 *            the number of results per page
	 */
	public void setResultsPerPage(int resultsPerPage) {
		this.resultsPerPage = resultsPerPage;
	}

	/**
	 * Returns the List of results.
	 * 
	 * @return the List of results
	 */
	public List<T> getResults() {
		if (results == null) {
			results = new ArrayList<T>();
		}
		return results;
	}

	/**
	 * Sets the List of search results.
	 * 
	 * @param results
	 *            the List of search results
	 */
	public void setResults(List<T> results) {
		if (results == null) {
			results = new ArrayList<T>();
		}
		this.results = results;
		this.totalResults = results.size();
	}

	/**
	 * Returns the number of pages with results.
	 * 
	 * @return the number of pages with results
	 */
	public int getNumberOfPages() {
		int numberOfPages;
		if (results == null || results.isEmpty()) {
			numberOfPages = 0;
		} else {
			numberOfPages = (totalResults / resultsPerPage)
					+ (totalResults % resultsPerPage > 0 ? 1 : 0);
		}
		return numberOfPages;
	}

	/**
	 * Returns the List of results for a selected page number.
	 * 
	 * @param pageNumber
	 *            the number of results page to be displayed
	 * @return the List of results
	 */
	public List<T> getResults(int pageNumber) {
		int page;
		List<T> resultsList = new ArrayList<T>();

		if (pageNumber <= 0 || pageNumber > getNumberOfPages()) {
			page = 1;
		} else {
			page = pageNumber;
		}

		int start = (page - 1) * resultsPerPage;
		int end = start + resultsPerPage;
		end = end > results.size() ? results.size() : end;

		for (int i = start; i < end; i++) {
			resultsList.add(results.get(i));
		}
		return resultsList;
	}
}
