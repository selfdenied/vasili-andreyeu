package com.epam.newsmanagement.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.epam.newsmanagement.command.ICommand;
import com.epam.newsmanagement.command.factory.CommandFactory;

import static com.epam.newsmanagement.util.RequestParameters.SERVLET_PATH;

/**
 * Servlet {@code ControllerServlet} is a Servlet implementation class that
 * controls communication between Command interface and JSPs. It calls the
 * proper Command class and then redirects the request to the proper JSP.
 * 
 * @author Vasili Andreev
 * @version 1.0
 * @see javax.servlet.http.HttpServlet
 */
@WebServlet(description = "The main Controller in the application", urlPatterns = { "/controller" })
public class ControllerServlet extends HttpServlet {
	private static final long serialVersionUID = 2976175680091426503L;
	private ClassPathXmlApplicationContext ctx;

	/**
	 * Constructs ControllerServlet object
	 * 
	 * @see javax.servlet.http.HttpServlet
	 */
	public ControllerServlet() {
		super();
	}

	/**
	 * Performs Servlet initialization.
	 * 
	 * @see javax.servlet.http.HttpServlet
	 */
	@Override
	public void init() {
		this.ctx = new ClassPathXmlApplicationContext("spring.xml");
	}

	/**
	 * Performs a set of actions when a HTTP GET method is used.
	 * 
	 * @param request
	 *            javax.servlet.http.HttpServletRequest
	 * @param response
	 *            javax.servlet.http.HttpServletResponse
	 * @throws ServletException
	 *             When a Servlet exception of some sort has occurred
	 * @throws IOException
	 *             When an I/O exception of some sort has occurred
	 * @see javax.servlet.http.HttpServlet
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * Performs a set of actions when a HTTP POST method is used.
	 * 
	 * @param request
	 *            javax.servlet.http.HttpServletRequest
	 * @param response
	 *            javax.servlet.http.HttpServletResponse
	 * @throws ServletException
	 *             When a Servlet exception of some sort has occurred
	 * @throws IOException
	 *             When an I/O exception of some sort has occurred
	 * 
	 * @see javax.servlet.http.HttpServlet
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * Processes the request.
	 * 
	 * @param request
	 *            javax.servlet.http.HttpServletRequest
	 * @param response
	 *            javax.servlet.http.HttpServletResponse
	 * @throws ServletException
	 *             When a Servlet exception of some sort has occurred
	 * @throws IOException
	 *             When an I/O exception of some sort has occurred
	 */
	protected void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		CommandFactory factory = new CommandFactory(ctx);
		ICommand command = factory.getCommand(request);
		String url = command.execute(request);
		/* putting the reconstructed ControllerServlet URL into request */
		request.setAttribute(SERVLET_PATH, request.getRequestURL().toString());
		/* forwarding the request to a proper JSP */
		request.getRequestDispatcher(url).forward(request, response);
	}

	/**
	 * Performs Servlet destruction.
	 * 
	 * @see javax.servlet.http.HttpServlet
	 */
	@Override
	public void destroy() {
		ctx.close();
	}
}
