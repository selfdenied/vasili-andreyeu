package com.epam.newsmanagement.command.factory;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.epam.newsmanagement.command.*;

import static com.epam.newsmanagement.util.RequestParameters.*;

/**
 * Class {@code CommandFactory} contains method that returns a proper ICommand
 * class depending on the value of "action" request parameter.
 * 
 * @author Vasili Andreev
 * @version 1.0
 * @see com.epam.newsmanagement.command.ICommand
 */
public class CommandFactory {
	private HashMap<String, ICommand> COMMAND_MAP = new HashMap<>();
	private ClassPathXmlApplicationContext ctx;

	/**
	 * Constructs CommandFactory instance.
	 * 
	 * @param ctx
	 *            ClassPathXmlApplicationContext
	 */
	public CommandFactory(ClassPathXmlApplicationContext ctx) {
		this.ctx = ctx;
		COMMAND_MAP.put(null, ctx.getBean(HomeCommandImpl.class));
		COMMAND_MAP.put("searchNews", ctx.getBean(SearchCommandImpl.class));
		COMMAND_MAP.put("viewNews", ctx.getBean(ViewNewsCommandImpl.class));
	}

	/**
	 * Returns a proper ICommand class depending on the value of "action"
	 * request parameter.
	 * 
	 * @param request
	 *            javax.servlet.http.HttpServletRequest
	 * @return Command class
	 */
	public ICommand getCommand(HttpServletRequest request) {
		ICommand command = null;
		String action = request.getParameter(ACTION);
		command = COMMAND_MAP.get(action);

		if (command == null) {
			command = ctx.getBean(HomeCommandImpl.class);
		}
		return command;
	}
}
