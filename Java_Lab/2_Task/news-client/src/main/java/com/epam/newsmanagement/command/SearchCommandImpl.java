package com.epam.newsmanagement.command;

import static com.epam.newsmanagement.util.RequestParameters.*;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.epam.newsmanagement.bean.Author;
import com.epam.newsmanagement.bean.Tag;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.search.news.NewsSearchCriteria.AuthorSearchType;
import com.epam.newsmanagement.search.news.NewsSearchResults;
import com.epam.newsmanagement.service.AuthorService;
import com.epam.newsmanagement.service.NewsService;
import com.epam.newsmanagement.service.TagService;

/**
 * Class {@code SearchCommandImpl} is invoked when the client tries to filter
 * (search) the news messages according to the given criteria (author id and/or
 * tag names).
 * 
 * @author Vasili Andreev
 * @version 1.0
 * @see com.epam.newsmanagement.command.ICommand
 */
public class SearchCommandImpl implements ICommand {
	private static final Logger LOG = Logger.getLogger(SearchCommandImpl.class);
	@Autowired
	private TagService tagService;
	@Autowired
	private AuthorService authorService;
	@Autowired
	private NewsService newsService;

	@Override
	public String execute(HttpServletRequest request) {
		String url = BUNDLE.getString("home");

		try {
			setAttrsToRequest(request);
			setNewsResultsToRequest(request);
		} catch (DAOException ex) {
			LOG.error(ex);
			request.setAttribute("exception", ex);
			url = BUNDLE.getString("error");
		}
		return url;
	}

	/* supplementary method that sets various attributes to request */
	private void setAttrsToRequest(HttpServletRequest request)
			throws DAOException {
		String locale = request.getParameter(LOCALE);
		String authorId = request.getParameter(AUTHOR_ID);
		String[] tagNames = request.getParameterValues(TAG_NAMES);
		Set<Author> authorsList = authorService.findAllAuthors();
		Set<Tag> tagsList = tagService.findAllTags();

		request.setAttribute(LOCALE, locale);
		request.setAttribute(AUTHOR_ID, authorId);
		request.setAttribute(TAG_NAMES, tagNames);
		request.setAttribute(AUTHORS_LIST, authorsList);
		request.setAttribute(TAGS_LIST, tagsList);
		request.setAttribute(ACTION, "searchNews");
	}

	/* supplementary method that searches the DB for news messages */
	private void setNewsResultsToRequest(HttpServletRequest request)
			throws DAOException {
		Author author = new Author();
		Set<String> tagsNames = new HashSet<>();
		String authorId = request.getParameter(AUTHOR_ID);
		String[] tagNames = request.getParameterValues(TAG_NAMES);
		String pageNumber = request.getParameter(PAGE_NUMBER);

		/* setting default page number value */
		if (pageNumber == null || pageNumber.isEmpty()) {
			pageNumber = DEFAULT_PAGE;
		}
		if (authorId != null && !authorId.isEmpty()) {
			author.setAuthorId(Long.valueOf(authorId));
		}
		if (tagNames != null && tagNames.length != 0) {
			for (String name : tagNames) {
				tagsNames.add(name);
			}
		}
		NewsSearchResults results = newsService.searchNews(author, tagsNames,
				AuthorSearchType.BY_ID, RESULTS_PER_PAGE);
		request.setAttribute(PAGE_NUMBER, pageNumber);
		request.setAttribute(NEWS_LIST,	results.getResults(Integer.parseInt(pageNumber)));
		request.setAttribute(PAGES_LIST, obtainPageNumbers(results));
	}

	/* supplementary method that gets the list of page numbers */
	private List<String> obtainPageNumbers(NewsSearchResults newsResults) {
		List<String> pageNumbers = new ArrayList<>();

		for (int i = 1; i <= newsResults.getNumberOfPages(); i++) {
			pageNumbers.add(String.valueOf(i));
		}
		return pageNumbers;
	}
}
