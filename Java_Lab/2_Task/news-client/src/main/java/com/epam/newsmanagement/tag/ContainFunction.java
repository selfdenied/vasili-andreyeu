package com.epam.newsmanagement.tag;

/**
 * Class {@code ContainFunction} contains method that determines whether a
 * String is a part of String array.
 * 
 * @author Vasili Andreev
 * @version 1.0
 */
public class ContainFunction {

	/**
	 * Determines whether a String is a part of String array.
	 * 
	 * @param array
	 *            The String array
	 * @param text
	 *            The String to be checked
	 * @return {@code true} if the chosen String array contains the given String
	 *         text
	 */
	public static boolean contains(String[] array, String text) {
		boolean contains = false;

		if (array != null) {
			for (String item : array) {
				if (item.equals(text)) {
					contains = true;
				}
			}
		}
		return contains;
	}
}
