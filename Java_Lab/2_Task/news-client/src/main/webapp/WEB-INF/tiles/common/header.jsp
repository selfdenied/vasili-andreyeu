<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<fmt:setLocale value="${locale}" scope="request" />
<fmt:setBundle basename="text" var="rb" />

<h1 class="blueC">
	<b><fmt:message key="news_portal" bundle="${rb}" /></b>
</h1>

<div class="right" id="19px">
	<form action="${base}" method="post" id="right">
		<input type="HIDDEN" name="action" value="${action}">
		<input type="HIDDEN" name="locale" value="ru_RU"> 
		<input type="HIDDEN" name="pageNumber" value="${pageNumber}"> 
		<input type="HIDDEN" name="authorId" value="${authorId}">
		<c:forEach var="tag" items="${tagNames}">
			<input type="HIDDEN" name="tagNames" value="${tag}">
		</c:forEach>
		<input type="HIDDEN" name="newsId" value="${newsId}">
		<input type="submit" value="РУС">
	</form>
	<form action="${base}" method="post" id="right">
		<input type="HIDDEN" name="action" value="${action}">
		<input type="HIDDEN" name="locale" value="en_US"> 
		<input type="HIDDEN" name="pageNumber" value="${pageNumber}">
		<input type="HIDDEN" name="authorId" value="${authorId}">
		<c:forEach var="tag" items="${tagNames}">
			<input type="HIDDEN" name="tagNames" value="${tag}">
		</c:forEach>
		<input type="HIDDEN" name="newsId" value="${newsId}">
		<input type="submit" value="ENG">
	</form>
</div>


