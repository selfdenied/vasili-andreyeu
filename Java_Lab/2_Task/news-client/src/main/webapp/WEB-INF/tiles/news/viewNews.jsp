<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<fmt:setLocale value="${locale}" scope="request" />
<fmt:setBundle basename="text" var="rb" />

<div class="center" id="24px">
	<form method="post">
		<input type="HIDDEN" name="action" value="searchNews">
		<input type="HIDDEN" name="locale" value="${locale}"> 
		<input type="HIDDEN" name="pageNumber" value="${pageNumber}"> 
		<input type="HIDDEN" name="authorId" value="${authorId}">
		<c:forEach var="tag" items="${tagNames}">
			<input type="HIDDEN" name="tagNames" value="${tag}">
		</c:forEach>
		<input type="submit" id="back" value="<fmt:message key='back_to_previous' bundle='${rb}' />">
	</form>
</div>

<br />

<table>
	<tr>
		<td id="news2">
			<div>
				<b><c:out value="${news.newsMessage.title}" /></b>
				&nbsp;&nbsp;
				<span class="red">
				(<c:out value="${news.author.authorName}" />)
				</span>
				<span id="underline">
					<fmt:formatDate value="${news.newsMessage.modDate}" />
				</span>
			</div>
		</td>
	</tr>
</table>

<table id="text">	
	<tr>
		<td id="text">
			<br />
			<c:out value="${news.newsMessage.fullText}" />
			<br />
		</td>
	</tr>
</table>

<br />
<br />

<table id="comment">
	<tr>
		<c:choose>
		<c:when test="${not empty listOfComments}">
			<td id="comment">
				<br />
				<c:forEach var="comment" items="${listOfComments}">
					<span id="underline">
						<fmt:formatDate value="${comment.creationDate}" />
					</span>
					<br />
					<div id="grayB">
						<c:out value="${comment.commentText}"></c:out>
					</div>
					<br />
				</c:forEach>
			</td>
		</c:when>
		<c:otherwise>
			<td id="comment">
				<h2 class="blueC">
					<fmt:message key="comment_list_empty" bundle="${rb}" />
				</h2>
			</td>
		</c:otherwise>
		</c:choose>
	</tr>
</table>

<br />

<div class="center">
	<form method="post">
		<input type="HIDDEN" name="action" value="viewNews">
		<input type="HIDDEN" name="locale" value="${locale}"> 
		<input type="HIDDEN" name="newsId" value="${newsId}">
		<input type="HIDDEN" name="pageNumber" value="${pageNumber}"> 
		<input type="HIDDEN" name="authorId" value="${authorId}">
		<c:forEach var="tag" items="${tagNames}">
			<input type="HIDDEN" name="tagNames" value="${tag}">
		</c:forEach>
		<textarea name="commentText" maxlength="100" required="required"></textarea>
		<br />
		<br />
		<input type="submit" value="<fmt:message key='post_comment' bundle='${rb}' />">
	</form>
</div>

<br />
<br />

<c:if test="${not empty prevNewsId}">
	<form method="post" id="left">
		<input type="HIDDEN" name="action" value="viewNews">
		<input type="HIDDEN" name="locale" value="${locale}">
		<input type="HIDDEN" name="newsId" value="${prevNewsId}">
		<input type="HIDDEN" name="pageNumber" value="${pageNumber}">
		<input type="HIDDEN" name="authorId" value="${authorId}">
		<c:forEach var="tag" items="${tagNames}">
			<input type="HIDDEN" name="tagNames" value="${tag}">
		</c:forEach>
		<input type="submit" id="view" value="<fmt:message key='previous' bundle='${rb}' />">
	</form>
</c:if>

<c:if test="${not empty nextNewsId}">
	<form method="post" id="right">
		<input type="HIDDEN" name="action" value="viewNews">
		<input type="HIDDEN" name="locale" value="${locale}">
		<input type="HIDDEN" name="newsId" value="${nextNewsId}">
		<input type="HIDDEN" name="pageNumber" value="${pageNumber}">
		<input type="HIDDEN" name="authorId" value="${authorId}">
		<c:forEach var="tag" items="${tagNames}">
			<input type="HIDDEN" name="tagNames" value="${tag}">
		</c:forEach>
		<input type="submit" id="view" value="<fmt:message key='next' bundle='${rb}' />">
	</form>
</c:if>