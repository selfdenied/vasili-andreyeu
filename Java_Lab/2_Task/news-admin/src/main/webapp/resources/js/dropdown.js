$(function() {

	$('#tags').multiselect({

		includeSelectAllOption : true
	});

	$("#reset").on("click", function () {
	    $('#author').prop('selectedIndex', 0);
	    $('#tags').multiselect('clearSelection');
	    $('#tags').multiselect('refresh');
	});
});

function showUpdate(id) {
	document.getElementById('text' + id).disabled = false;
	document.getElementById('text' + id).required = true;
	document.getElementById('update' + id).style.display = 'inline';
	document.getElementById('expire' + id).style.display = 'inline';
	document.getElementById('cancel' + id).style.display = 'inline';
	document.getElementById('edit' + id).style.display = 'none';
}

function cancelUpdate(id, name) {
	document.getElementById('text' + id).value = name;
	document.getElementById('text' + id).disabled = true;
	document.getElementById('text' + id).required = false;
	document.getElementById('update' + id).style.display = 'none';
	document.getElementById('expire' + id).style.display = 'none';
	document.getElementById('cancel' + id).style.display = 'none';
	document.getElementById('edit' + id).style.display = 'inline';
}