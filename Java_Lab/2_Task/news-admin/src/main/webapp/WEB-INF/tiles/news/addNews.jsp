<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<div class="center">
	<form method="post" action="addNews">
		<input type="HIDDEN" name="${_csrf.parameterName}" value="${_csrf.token}"/>
		<br />
		<b><spring:message code="author_tags"/></b>
		<br />
		<select	id="author" name="authorId" required="required">
			<option selected="selected" disabled="disabled">
				<spring:message code="select_author"/>
			</option>
			<c:forEach var="author" items="${listOfAuthors}">
				<option value="${author.authorId}">
					<c:out value="${author.authorName}" />
				</option>
			</c:forEach>
		</select> 
		&nbsp;
		<select id="tags" name="addTagsIds" multiple="multiple">
			<option disabled="disabled">
				<spring:message code="select_tags"/>
			</option>
    		<c:forEach var="tag" items="${listOfTags}">
   				<option value="${tag.tagId}">
    				<c:out value="${tag.tagName}" />
    			</option>
    		</c:forEach>
		</select>
		<br />
		<br />
		<b><spring:message code="title"/></b>
		<br />
		<input type="text" name="title" size="30" maxlength="50" required="required">
		<br />
		<br />
		<b><spring:message code="cr_date"/></b>
		<br />
		<input type="text" name="creationDate" size="20" disabled="disabled" 
		value="<fmt:formatDate value="${creationDate}" />">
		<br />
		<br />
		<b><spring:message code="short_text"/></b>
		<br />
		<textarea name="shortText" maxlength="100" required="required"></textarea>
		<br />
		<br />
		<b><spring:message code="full_text"/></b>
		<br />
		<textarea name="fullText" id="full" maxlength="2000" required="required"></textarea>
		<br />
		<br />
		<input type="submit" id="view" value="<spring:message code="save"/>">
	</form>
</div>