<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="my" uri="/WEB-INF/tld/custom.tld"%>

<div class="center">
	<form method="post" action="editNews">
		<input type="HIDDEN" name="${_csrf.parameterName}" value="${_csrf.token}"/>
		<input type="HIDDEN" name="newsId" value="${news.newsMessage.newsId}">
		<input type="HIDDEN" name="authorId" value="${authorId}">
		<c:forEach var="tag" items="${tagNames}">
			<input type="HIDDEN" name="tagNames" value="${tag}">
		</c:forEach>
		<br />
		<b><spring:message code="author_tags"/></b>
		<br />
		<select id="author">
			<option selected="selected" disabled="disabled">
				<c:out value="${author.authorName}" />
			</option>
		</select> 
		&nbsp;
		<select id="tags" name="editTagsIds" multiple="multiple">
			<option disabled="disabled">
				<spring:message code="select_tags"/>
			</option>
    		<c:forEach var="tag" items="${listOfTags}">
    			<c:choose>
    			<c:when test="${my:contains(newsTagNames, tag.tagName)}">
    				<option selected="selected" value="${tag.tagId}">
    					<c:out value="${tag.tagName}" />
    				</option>
    			</c:when>
    			<c:otherwise>
    			    <option value="${tag.tagId}">
    					<c:out value="${tag.tagName}" />
    				</option>
    			</c:otherwise>
    			</c:choose>
    		</c:forEach>
		</select>
		<br />
		<br />
		<b><spring:message code="title"/></b>
		<br />
		<input type="text" name="title" size="30" maxlength="50" 
		required="required" value="${news.newsMessage.title}">
		<br />
		<br />
		<b><spring:message code="mod_date"/></b>
		<br />
		<input type="text" name="modDate" size="20" disabled="disabled" 
		value="<fmt:formatDate value="${news.newsMessage.modDate}" />">
		<br />
		<br />
		<b><spring:message code="short_text"/></b>
		<br />
		<textarea name="shortText" maxlength="100" required="required">${news.newsMessage.shortText}</textarea>
		<br />
		<br />
		<b><spring:message code="full_text"/></b>
		<br />
		<textarea name="fullText" id="full" maxlength="2000" 
		required="required">${news.newsMessage.fullText}</textarea>
		<br />
		<br />
		<input type="submit" id="view" value="<spring:message code="update"/>">
	</form>
</div>