<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<h1 class="blueC">
	<b><spring:message code="news_portal_admin"/></b>
</h1>

<div class="right" id="19px">
	<c:choose>
	<c:when test="${action == 'login'}">
		<form action="login" id="right">
			<input type="HIDDEN" name="locale" value="ru_RU">
			<input type="submit" value="РУС">
		</form>
		<form action="login" id="right">
			<input type="HIDDEN" name="locale" value="en_US">
			<input type="submit" value="ENG">
		</form>
	</c:when>
	<c:otherwise>
		<form action="${action}" method="post" id="right">
			<input type="HIDDEN" name="${_csrf.parameterName}" value="${_csrf.token}"/>
			<input type="HIDDEN" name="locale" value="ru_RU">
			<input type="HIDDEN" name="pageNumber" value="${pageNumber}">
			<input type="HIDDEN" name="authorId" value="${authorId}">
			<c:forEach var="tag" items="${tagNames}">
				<input type="HIDDEN" name="tagNames" value="${tag}">
			</c:forEach>
			<input type="HIDDEN" name="newsId" value="${newsId}">  
			<input type="submit" value="РУС">
		</form>
		<form action="${action}" method="post" id="right">
			<input type="HIDDEN" name="${_csrf.parameterName}" value="${_csrf.token}"/>
			<input type="HIDDEN" name="locale" value="en_US">
			<input type="HIDDEN" name="pageNumber" value="${pageNumber}">
			<input type="HIDDEN" name="authorId" value="${authorId}">
			<c:forEach var="tag" items="${tagNames}">
				<input type="HIDDEN" name="tagNames" value="${tag}">
			</c:forEach>
			<input type="HIDDEN" name="newsId" value="${newsId}">
			<input type="submit" value="ENG">
		</form>
	</c:otherwise>
	</c:choose>
	
	<c:if test="${not empty sessionScope.userName && action != 'login'}">
		<form action="log_out" method="post" id="left" class="17px">
			<input type="HIDDEN" name="${_csrf.parameterName}" value="${_csrf.token}"/>
			<input type="submit" value="<spring:message code="logout"/>">
			&nbsp;&nbsp;			
			<b>
			<spring:message code="hello"/>
			<span class="red">
				<c:out value="${sessionScope.userName}"></c:out>
			</span>
			</b>
		</form>
	</c:if>
</div>


