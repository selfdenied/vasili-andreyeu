<%@ page language="java" isErrorPage="true" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<html id="error">
  <head>
  	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  	<link rel='stylesheet' href='resources/css/style.css' type='text/css' />
    <title><tiles:getAsString name="title"/></title>
  </head>
  <body>
    <table id="error">
      <tr>
        <td id="error">
          <tiles:insertAttribute name="body" />
        </td>
      </tr>
    </table>
  </body>
</html>