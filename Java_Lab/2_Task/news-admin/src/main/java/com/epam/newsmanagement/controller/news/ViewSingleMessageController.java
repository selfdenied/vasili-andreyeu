package com.epam.newsmanagement.controller.news;

import static com.epam.newsmanagement.search.news.NewsSearchCriteria.AuthorSearchType.BY_ID;
import static com.epam.newsmanagement.util.RequestParameters.*;

import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.epam.newsmanagement.bean.*;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.service.CommentService;
import com.epam.newsmanagement.service.NewsService;

/**
 * Class {@code ViewSingleMessageController} allows to view the selected news
 * message. It also deals with comments adding.
 * 
 * @author Vasili Andreev
 * @version 1.0
 */
@Controller
public class ViewSingleMessageController {
	private static final String VIEW_MESSAGE = "news/viewMessage";
	@Autowired
	private NewsService newsService;
	@Autowired
	private CommentService commentService;

	@RequestMapping(value = "/viewMessage", method = RequestMethod.POST)
	public String viewMessage(HttpServletRequest req, Model model,
			Locale locale, News news, Comment comment) throws DAOException {
		String url = VIEW_MESSAGE;
		String commentText = comment.getCommentText();
		HttpSession session = req.getSession(false);

		if (commentText != null && !commentText.isEmpty()) {
			Comment newComment = new Comment();
			newComment.setCommentText(commentText);
			newComment.setNewsId(news.getNewsId());
			commentService.addNewComment(newComment);
		}
		setAttrsToModel(req, model, news.getNewsId());
		session.setAttribute(LOCALE, locale);
		session.setAttribute(ACTION, "viewMessage");
		return url;
	}

	@RequestMapping(value = "/deleteComment", method = RequestMethod.POST)
	public String deleteComment(HttpServletRequest req, Model model,
			Comment comment, News news) throws DAOException {
		String url = VIEW_MESSAGE;

		commentService.deleteComment(comment.getCommentId());
		setAttrsToModel(req, model, news.getNewsId());
		return url;
	}

	/* supplementary method that sets various attributes to model */
	private void setAttrsToModel(HttpServletRequest req, Model model,
			Long newsId) throws DAOException {
		NewsVO news = null;
		String authorId = req.getParameter(AUTHOR_ID);
		String[] tagNames = req.getParameterValues(TAG_NAMES);
		news = newsService.obtainNewsMessage(newsId);
		List<Comment> listOfComments = newsService
				.obtainNewsCommentsSorted(newsId);

		setNewsIds(model, newsId, authorId, tagNames);

		model.addAttribute(NEWS_ID, newsId);
		model.addAttribute(NEWS, news);
		model.addAttribute(AUTHOR_ID, authorId);
		model.addAttribute(TAG_NAMES, tagNames);
		model.addAttribute(COMMENTS_LIST, listOfComments);
	}

	/* sets the IDs of the next and previous news messages to model */
	private void setNewsIds(Model model, Long newsId, String authorId,
			String[] tagNames) throws DAOException {
		List<NewsVO> newsList = allFoundNews(authorId, tagNames);
		int index = findNewsIndex(newsList, newsId);

		if (index > 0) {
			News message = newsList.get(index - 1).getNewsMessage();
			model.addAttribute(PREV_NEWS_ID, message.getNewsId());
		}
		if (index >= 0 && index < newsList.size() - 1) {
			News message = newsList.get(index + 1).getNewsMessage();
			model.addAttribute(NEXT_NEWS_ID, message.getNewsId());
		}
	}

	/* finds the index of news in the list */
	private int findNewsIndex(List<NewsVO> newsList, Long newsId) {
		int index = -1;
		for (int i = 0; i < newsList.size(); i++) {
			News message = newsList.get(i).getNewsMessage();
			if (message.getNewsId() == newsId) {
				index = i;
			}
		}
		return index;
	}

	/* finds all news according to previous search criteria */
	private List<NewsVO> allFoundNews(String authorId, String[] tagNames)
			throws DAOException {
		Author author = new Author();
		Set<String> tagsNames = new HashSet<>();

		if (authorId != null && !authorId.isEmpty()) {
			author.setAuthorId(Long.valueOf(authorId));
		}
		if (tagNames != null && tagNames.length != 0) {
			for (String name : tagNames) {
				tagsNames.add(name);
			}
		}
		return newsService.searchNews(author, tagsNames, BY_ID,
				RESULTS_PER_PAGE).getResults();
	}
}
