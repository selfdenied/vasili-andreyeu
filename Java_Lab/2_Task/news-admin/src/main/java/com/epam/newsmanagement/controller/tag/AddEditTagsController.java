package com.epam.newsmanagement.controller.tag;

import static com.epam.newsmanagement.util.RequestParameters.*;

import java.util.Locale;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.epam.newsmanagement.bean.Tag;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.service.TagService;

/**
 * Class {@code AddEditTagsController} deals with adding, updating, and deleting
 * of the existing tags.
 * 
 * @author Vasili Andreev
 * @version 1.0
 */
@Controller
public class AddEditTagsController {
	private static final String ADD_EDIT_TAGS = "tag/addEditTags";
	@Autowired
	private TagService tagService;

	@RequestMapping(value = "/addTags", method = RequestMethod.POST)
	public String addTags(HttpServletRequest req, Model model, Locale locale,
			Tag tag) throws DAOException {
		String url = ADD_EDIT_TAGS;
		String tagName = tag.getTagName();
		HttpSession session = req.getSession(false);
		
		if (tagName != null && !tagName.isEmpty()) {
			tagService.addNewTag(tag);
		}
		setAttrsToModel(model);
		session.setAttribute(ACTION, "addTags");
		session.setAttribute(LOCALE, locale);
		return url;
	}
	
	@RequestMapping(value = "/editTag", method = RequestMethod.POST)
	public String editTag(HttpServletRequest req, Model model, Locale locale,
			Tag tag) throws DAOException {
		String url = ADD_EDIT_TAGS;
		String tagName = tag.getTagName();
		HttpSession session = req.getSession(false);
		
		if (tagName != null && !tagName.isEmpty()) {
			tagService.updateTag(tag, tag.getTagId());
		}
		setAttrsToModel(model);
		session.setAttribute(LOCALE, locale);
		return url;
	}
	
	@RequestMapping(value = "/deleteTag", method = RequestMethod.POST)
	public String deleteTag(HttpServletRequest req, Model model, Locale locale,
			Tag tag) throws DAOException {
		String url = ADD_EDIT_TAGS;
		Long tagId = tag.getTagId();
		HttpSession session = req.getSession(false);
		
		if (tagId != null) {
			tagService.deleteTag(tagId);
		}
		setAttrsToModel(model);
		session.setAttribute(LOCALE, locale);
		return url;
	}
	
	/* supplementary method that sets attributes to model */
	private void setAttrsToModel(Model model) throws DAOException {
		Set<Tag> tagsList = tagService.findAllTags();
		model.addAttribute(TAGS_LIST, tagsList);
	}
}
