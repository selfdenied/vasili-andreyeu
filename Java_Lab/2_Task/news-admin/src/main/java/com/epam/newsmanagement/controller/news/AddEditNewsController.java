package com.epam.newsmanagement.controller.news;

import static com.epam.newsmanagement.util.RequestParameters.*;

import java.sql.Date;
import java.util.Calendar;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;
import java.util.TimeZone;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.epam.newsmanagement.bean.Author;
import com.epam.newsmanagement.bean.News;
import com.epam.newsmanagement.bean.NewsVO;
import com.epam.newsmanagement.bean.Tag;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.service.*;

/**
 * Class {@code AddEditNewsController} deals with adding and updating of the
 * existing news messages.
 * 
 * @author Vasili Andreev
 * @version 1.0
 */
@Controller
public class AddEditNewsController {
	private static final String EDIT_NEWS = "news/editNews";
	private static final String VIEW_NEWS = "redirect:viewNews";
	private static final String ADD_NEWS = "news/addNews";
	@Autowired
	private TagService tagService;
	@Autowired
	private AuthorService authorService;
	@Autowired
	private NewsService newsService;

	@RequestMapping(value = "/editNews", method = RequestMethod.POST)
	public String editNews(HttpServletRequest req, Model model, Locale locale,
			News news) throws DAOException {
		String url = EDIT_NEWS;
		String title = news.getTitle();
		HttpSession session = req.getSession(false);

		if (title != null && !title.isEmpty()) {
			newsService.updateNewsMessage(news, news.getNewsId());
			tagService.updateNewsTags(news.getNewsId(),
					obtainTagsIds(req, EDIT_TAGS_IDS));
			url = VIEW_NEWS;
		} else {
			setAddAttrsToModel(model);
			model.addAttribute(NEWS_ID, news.getNewsId());
			session.setAttribute(ACTION, "editNews");
		}
		setAttrsToModel(req, model, news);
		session.setAttribute(LOCALE, locale);
		return url;
	}

	@RequestMapping(value = "/addNews", method = RequestMethod.POST)
	public String addNews(HttpServletRequest req, Model model, Locale locale,
			News news, Author author) throws DAOException {
		String url = ADD_NEWS;
		String title = news.getTitle();
		HttpSession session = req.getSession(false);

		if (title != null && !title.isEmpty()) {
			newsService.addNewsMessage(news, author.getAuthorId(),
					obtainTagsIds(req, ADD_TAGS_IDS));
			url = VIEW_NEWS;
		} else {
			setAddAttrsToModel(model);
			session.setAttribute(ACTION, "addNews");
		}
		session.setAttribute(LOCALE, locale);
		return url;
	}

	/* supplementary method that sets attributes to model */
	private void setAttrsToModel(HttpServletRequest req, Model model, News news)
			throws DAOException {
		String authorId = req.getParameter(AUTHOR_ID);
		String[] tagNames = req.getParameterValues(TAG_NAMES);
		NewsVO newsMessage = newsService.obtainNewsMessage(news.getNewsId());
		String[] newsTagNames = getTagNamesForNewsMessage(newsMessage);

		model.addAttribute(AUTHOR_ID, authorId);
		model.addAttribute(AUTHOR, newsMessage.getAuthor());
		model.addAttribute(TAG_NAMES, tagNames);
		model.addAttribute(NEWS, newsMessage);
		model.addAttribute(NEWS_TAG_NAMES, newsTagNames);
	}

	/* supplementary method that sets add. attributes to model */
	private void setAddAttrsToModel(Model model) throws DAOException {
		Set<Author> authorsList = authorService.findAllActiveAuthors();
		Set<Tag> tagsList = tagService.findAllTags();

		model.addAttribute(CR_DATE, obtainDate());
		model.addAttribute(AUTHORS_LIST, authorsList);
		model.addAttribute(TAGS_LIST, tagsList);
	}

	/* gets the set of Tag Ids (if there are any) */
	private Set<Long> obtainTagsIds(HttpServletRequest req, String paramName) {
		Set<Long> tagsIds = new HashSet<>();
		String[] addTagsIds = req.getParameterValues(paramName);

		if (addTagsIds != null && addTagsIds.length != 0) {
			for (String tagId : addTagsIds) {
				tagsIds.add(Long.valueOf(tagId));
			}
		}
		return tagsIds;
	}

	/* finds the tag names of the selected news message */
	private String[] getTagNamesForNewsMessage(NewsVO newsMessage) {
		Set<Tag> tagsList = newsMessage.getListOfTags();
		String[] newsTagNames = null;

		if (tagsList != null && !tagsList.isEmpty()) {
			int i = 0;
			newsTagNames = new String[tagsList.size()];
			for (Tag tag : tagsList) {
				newsTagNames[i] = tag.getTagName();
				i++;
			}
		}
		return newsTagNames;
	}

	/* Gets the current Date */
	private Date obtainDate() {
		TimeZone zone = TimeZone.getTimeZone("GMT+3");
		Calendar calendar = Calendar.getInstance(zone, Locale.getDefault());
		java.util.Date date = calendar.getTime();
		return new Date(date.getTime());
	}
}
