package com.epam.newsmanagement.util;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import org.apache.log4j.Logger;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import static com.epam.newsmanagement.util.HashPassword.*;

/**
 * Class {@code PBKDF2PasswordEncoder} is used as a custom PBKDF2 password
 * encoder. It hashes passwords and validates the entered passwords against the
 * hash stored in the database. Needed by Spring Security.
 * 
 * @author Vasili Andreev
 * @version 1.0
 */
@Service("PBKDF2PasswordEncoder")
public class PBKDF2PasswordEncoder implements PasswordEncoder {
	private static final Logger LOG = Logger.getLogger(PBKDF2PasswordEncoder.class);

	/* Returns the hash of the entered password */
	@Override
	public String encode(CharSequence password) {
		String hashedPassword = null;

		try {
			hashedPassword = createHash(password.toString());
		} catch (NoSuchAlgorithmException | InvalidKeySpecException ex) {
			LOG.error("Error. Unable to hash password!", ex);
		}
		return hashedPassword;
	}

	/* Validates the entered password against the hash */
	@Override
	public boolean matches(CharSequence password, String correctHash) {
		boolean isPassOk = false;

		try {
			isPassOk = validatePassword(password.toString(), correctHash);
		} catch (NoSuchAlgorithmException | InvalidKeySpecException ex) {
			LOG.error("Error. Unable to validate password!", ex);
		}
		return isPassOk;
	}
}
