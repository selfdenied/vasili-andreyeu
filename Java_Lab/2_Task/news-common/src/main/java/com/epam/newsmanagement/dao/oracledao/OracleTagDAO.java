package com.epam.newsmanagement.dao.oracledao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.bean.Tag;
import com.epam.newsmanagement.dao.AbstractDAO;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.util.DBUtils;

/**
 * Class {@code OracleTagDAO} contains methods allowing to extract information
 * about Tags found in the application, add, update, and delete their data.
 * 
 * @author Vasili Andreev
 * @version 1.0
 * @see com.epam.newsmanagement.bean.Tag
 * @see com.epam.newsmanagement.dao.AbstractDAO
 */
@Component
@Scope("prototype")
public class OracleTagDAO extends AbstractDAO<Tag> {
	private static final String TAG_ID = "TAG_ID";
	private static final String TAG_NAME = "TAG_NAME";
	private static final String SQL_FIND_TAGS_QUERY = "SELECT TAG_ID, TAG_NAME FROM TAGS";
	private static final String SQL_FIND_TAG_BY_ID_QUERY = "SELECT TAG_ID, TAG_NAME FROM TAGS WHERE TAG_ID = ?";
	private static final String SQL_FIND_TAGS_BY_NEWS_ID_QUERY = "SELECT TAGS.TAG_ID, TAGS.TAG_NAME FROM TAGS"
			+ " JOIN NEWS_TAGS ON TAGS.TAG_ID = NEWS_TAGS.TAG_ID WHERE NEWS_TAGS.NEWS_ID = ?";
	private static final String SQL_ADD_TAG_QUERY = "INSERT INTO TAGS (TAG_NAME) VALUES (?)";
	private static final String SQL_UPDATE_TAG_QUERY = "UPDATE TAGS SET TAG_NAME = ? WHERE TAG_ID = ?";
	private static final String SQL_DELETE_TAG_DEPEN_QUERY = "DELETE FROM NEWS_TAGS WHERE TAG_ID = ?";
	private static final String SQL_DELETE_TAG_QUERY = "DELETE FROM TAGS WHERE TAG_ID = ?";
	private static final String SQL_ASSIGN_TAG_QUERY = "INSERT INTO NEWS_TAGS (NEWS_ID, TAG_ID) VALUES (?, ?)";
	private static final String SQL_DEASSIGN_TAGS_QUERY = "DELETE FROM NEWS_TAGS WHERE NEWS_ID = ?";

	/* finds all Tags in the application */
	@Override
	public Set<Tag> findAll() throws DAOException {
		ResultSet rs = null;
		PreparedStatement prepStatement = null;
		Set<Tag> tagsList = new HashSet<Tag>();
		Connection conn = getConnection();

		try {
			prepStatement = conn.prepareStatement(SQL_FIND_TAGS_QUERY);
			rs = prepStatement.executeQuery();
			while (rs.next()) {
				/* creating a new Tag bean and initializing its fields */
				Tag tag = new Tag();
				tag.setTagId(rs.getLong(TAG_ID));
				tag.setTagName(rs.getString(TAG_NAME));
				tagsList.add(tag);
			}
		} catch (SQLException ex) {
			throw new DAOException("Database error", ex);
		} finally {
			DBUtils.close(rs, prepStatement, conn);
		}
		return tagsList;
	}

	/**
	 * Finds all Tags associated with the news message having the given ID.
	 * 
	 * @param newsId
	 *            The ID of the news message
	 * @return The list of Tags associated with the news message
	 * @throws DAOException
	 *             If a database access/handling error occurs.
	 */
	public Set<Tag> findAllTagsByNewsId(Long newsId) throws DAOException {
		ResultSet rs = null;
		PreparedStatement prepStatement = null;
		Set<Tag> tagsList = new HashSet<Tag>();
		Connection conn = getConnection();

		try {
			prepStatement = conn.prepareStatement(SQL_FIND_TAGS_BY_NEWS_ID_QUERY);
			prepStatement.setLong(1, newsId);
			rs = prepStatement.executeQuery();
			while (rs.next()) {
				/* creating a new Tag bean and initializing its fields */
				Tag tag = new Tag();
				tag.setTagId(rs.getLong(TAG_ID));
				tag.setTagName(rs.getString(TAG_NAME));
				tagsList.add(tag);
			}
		} catch (SQLException ex) {
			throw new DAOException("Database error", ex);
		} finally {
			DBUtils.close(rs, prepStatement, conn);
		}
		return tagsList;
	}

	/* returns Tag with the given ID */
	@Override
	public Tag findEntityById(Long tagId) throws DAOException {
		Tag tag = null;
		ResultSet rs = null;
		PreparedStatement prepStatement = null;
		Connection conn = getConnection();
		
		try {
			prepStatement = conn.prepareStatement(SQL_FIND_TAG_BY_ID_QUERY);
			prepStatement.setLong(1, tagId);
			rs = prepStatement.executeQuery();
			while (rs.next()) {
				/* creating a new Tag bean and initializing its fields */
				tag = new Tag();
				tag.setTagId(rs.getLong(TAG_ID));
				tag.setTagName(rs.getString(TAG_NAME));
			}
		} catch (SQLException ex) {
			throw new DAOException("Database error", ex);
		} finally {
			DBUtils.close(rs, prepStatement, conn);
		}
		return tag;
	}

	/* adds new Tag to the database */
	@Override
	public boolean addNewEntity(Tag tag) throws DAOException {
		PreparedStatement prepStatement = null;
		boolean isAdded = false;
		Connection conn = getConnection();

		try {
			prepStatement = conn.prepareStatement(SQL_ADD_TAG_QUERY);
			prepStatement.setString(1, tag.getTagName());
			prepStatement.executeUpdate();
			isAdded = true;
		} catch (SQLException ex) {
			throw new DAOException("Database error", ex);
		} finally {
			DBUtils.close(prepStatement, conn);
		}
		return isAdded;
	}

	/* updates Tag's data */
	@Override
	public boolean updateEntity(Tag tag, Long tagId) throws DAOException {
		PreparedStatement prepStatement = null;
		boolean isUpdated = false;
		Connection conn = getConnection();

		try {
			prepStatement = conn.prepareStatement(SQL_UPDATE_TAG_QUERY);
			prepStatement.setString(1, tag.getTagName());
			prepStatement.setLong(2, tagId);
			prepStatement.executeUpdate();
			isUpdated = true;
		} catch (SQLException ex) {
			throw new DAOException("Database error", ex);
		} finally {
			DBUtils.close(prepStatement, conn);
		}
		return isUpdated;
	}

	/* deletes a given Tag from the database */
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = DAOException.class)
	public boolean deleteEntity(Long tagId) throws DAOException {
		PreparedStatement prepStatement = null;
		boolean isDeleted = false;
		Connection conn = getConnection();

		try {
			prepStatement = conn.prepareStatement(SQL_DELETE_TAG_DEPEN_QUERY);
			prepStatement.setLong(1, tagId);
			prepStatement.executeUpdate();
			DBUtils.close(prepStatement, null);
			prepStatement = conn.prepareStatement(SQL_DELETE_TAG_QUERY);
			prepStatement.setLong(1, tagId);
			prepStatement.executeUpdate();
			isDeleted = true;
		} catch (SQLException ex) {
			throw new DAOException("Database error", ex);
		} finally {
			DBUtils.close(prepStatement, conn);
		}
		return isDeleted;
	}

	/**
	 * Assigns a Tag with the given ID to the news message with the given ID.
	 * 
	 * @param newsId
	 *            The ID of the News message
	 * @param tagId
	 *            The ID of the Tag
	 * @return {@code true} if the tag has been successfully assigned and
	 *         {@code false} otherwise
	 * @throws DAOException
	 *             If a database access/handling error occurs.
	 */
	public boolean assignTagToNews(Long newsId, Long tagId) throws DAOException {
		PreparedStatement prepStatement = null;
		boolean isAssigned = false;
		Connection conn = getConnection();

		try {
			prepStatement = conn.prepareStatement(SQL_ASSIGN_TAG_QUERY);
			prepStatement.setLong(1, newsId);
			prepStatement.setLong(2, tagId);
			prepStatement.executeUpdate();
			isAssigned = true;
		} catch (SQLException ex) {
			throw new DAOException("Database error", ex);
		} finally {
			DBUtils.close(prepStatement, conn);
		}
		return isAssigned;
	}

	/**
	 * Deassigns all Tags from the given news message.
	 * 
	 * @param newsId
	 *            The ID of the News message
	 * @return {@code true} if tags have been successfully deassigned and
	 *         {@code false} otherwise
	 * @throws DAOException
	 *             If a database access/handling error occurs.
	 */
	public boolean deassignTagsFromNews(Long newsId) throws DAOException {
		PreparedStatement prepStatement = null;
		boolean isDeassigned = false;
		Connection conn = getConnection();

		try {
			prepStatement = conn.prepareStatement(SQL_DEASSIGN_TAGS_QUERY);
			prepStatement.setLong(1, newsId);
			prepStatement.executeUpdate();
			isDeassigned = true;
		} catch (SQLException ex) {
			throw new DAOException("Database error", ex);
		} finally {
			DBUtils.close(prepStatement, conn);
		}
		return isDeassigned;
	}
}
