package com.epam.newsmanagement.dao.oracledao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;
import java.util.TimeZone;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.epam.newsmanagement.bean.Comment;
import com.epam.newsmanagement.dao.AbstractDAO;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.util.DBUtils;

/**
 * Class {@code OracleCommentDAO} contains methods allowing to extract
 * information about Comments found in the application, add, update, and delete
 * their data.
 * 
 * @author Vasili Andreev
 * @version 1.0
 * @see com.epam.newsmanagement.bean.Comment
 * @see com.epam.newsmanagement.dao.AbstractDAO
 */
@Component
@Scope("prototype")
public class OracleCommentDAO extends AbstractDAO<Comment> {
	private static final String COMMENT_ID = "COMMENT_ID";
	private static final String NEWS_ID = "NEWS_ID";
	private static final String COMMENT_TEXT = "COMMENT_TEXT";
	private static final String CR_DATE = "CREATION_DATE";
	private static final String SQL_FIND_COMMENTS_BY_NEWS_ID_QUERY = "SELECT COMMENT_ID, NEWS_ID, COMMENT_TEXT,"
			+ " CREATION_DATE FROM COMMENTS WHERE NEWS_ID = ?";
	private static final String SQL_FIND_COMMENT_BY_ID_QUERY = "SELECT COMMENT_ID, NEWS_ID, COMMENT_TEXT,"
			+ " CREATION_DATE FROM COMMENTS WHERE COMMENT_ID = ?";
	private static final String SQL_ADD_COMMENT_QUERY = "INSERT INTO COMMENTS (NEWS_ID, COMMENT_TEXT, CREATION_DATE)"
			+ " VALUES (?, ?, ?)";
	private static final String SQL_DELETE_COMMENT_QUERY = "DELETE FROM COMMENTS WHERE COMMENT_ID = ?";
	private static final String SQL_DEASSIGN_COMMENTS_QUERY = "DELETE FROM COMMENTS WHERE NEWS_ID = ?";

	/* there is no need in such method in the application */
	@Override
	public Set<Comment> findAll() {
		throw new UnsupportedOperationException(
				"Error. This operation is not supported!");
	}

	/**
	 * Finds all Comments associated with the news message having the given ID.
	 * 
	 * @param newsId
	 *            The ID of the news message
	 * @return The list of Comments associated with the news message
	 * @throws DAOException
	 *             If a database access/handling error occurs.
	 */
	public Set<Comment> findCommentsByNewsId(Long newsId) throws DAOException {
		ResultSet rs = null;
		PreparedStatement prepStatement = null;
		Set<Comment> commentsList = new HashSet<Comment>();
		Connection conn = getConnection();

		try {
			prepStatement = conn.prepareStatement(SQL_FIND_COMMENTS_BY_NEWS_ID_QUERY);
			prepStatement.setLong(1, newsId);
			rs = prepStatement.executeQuery();
			while (rs.next()) {
				/* creating a new Comment bean and initializing its fields */
				Comment comment = new Comment();
				comment.setCommentId(rs.getLong(COMMENT_ID));
				comment.setNewsId(rs.getLong(NEWS_ID));
				comment.setCommentText(rs.getString(COMMENT_TEXT));
				comment.setCreationDate(rs.getTimestamp(CR_DATE));
				commentsList.add(comment);
			}
		} catch (SQLException ex) {
			throw new DAOException("Database error", ex);
		} finally {
			DBUtils.close(rs, prepStatement, conn);
		}
		return commentsList;
	}

	/* returns Comment with the given ID */
	@Override
	public Comment findEntityById(Long commentId) throws DAOException {
		Comment comment = null;
		ResultSet rs = null;
		PreparedStatement prepStatement = null;
		Connection conn = getConnection();

		try {
			prepStatement = conn.prepareStatement(SQL_FIND_COMMENT_BY_ID_QUERY);
			prepStatement.setLong(1, commentId);
			rs = prepStatement.executeQuery();
			while (rs.next()) {
				/* creating a new Comment bean and initializing its fields */
				comment = new Comment();
				comment.setCommentId(rs.getLong(COMMENT_ID));
				comment.setNewsId(rs.getLong(NEWS_ID));
				comment.setCommentText(rs.getString(COMMENT_TEXT));
				comment.setCreationDate(rs.getTimestamp(CR_DATE));
			}
		} catch (SQLException ex) {
			throw new DAOException("Database error", ex);
		} finally {
			DBUtils.close(rs, prepStatement, conn);
		}
		return comment;
	}

	/* adds new Comment to the database */
	@Override
	public boolean addNewEntity(Comment comment) throws DAOException {
		PreparedStatement prepStatement = null;
		boolean isAdded = false;
		Connection conn = getConnection();

		try {
			prepStatement = conn.prepareStatement(SQL_ADD_COMMENT_QUERY);
			prepStatement.setLong(1, comment.getNewsId());
			prepStatement.setString(2, comment.getCommentText());
			prepStatement.setTimestamp(3, obtainTimestamp());
			prepStatement.executeUpdate();
			isAdded = true;
		} catch (SQLException ex) {
			throw new DAOException("Database error", ex);
		} finally {
			DBUtils.close(prepStatement, conn);
		}
		return isAdded;
	}

	/* there is no need in such method in the application */
	@Override
	public boolean updateEntity(Comment comment, Long commentId) {
		throw new UnsupportedOperationException(
				"Error. This operation is not supported!");
	}

	/* deletes comment from the database */
	@Override
	public boolean deleteEntity(Long commentId) throws DAOException {
		PreparedStatement prepStatement = null;
		boolean isDeleted = false;
		Connection conn = getConnection();

		try {
			prepStatement = conn.prepareStatement(SQL_DELETE_COMMENT_QUERY);
			prepStatement.setLong(1, commentId);
			prepStatement.executeUpdate();
			isDeleted = true;
		} catch (SQLException ex) {
			throw new DAOException("Database error", ex);
		} finally {
			DBUtils.close(prepStatement, conn);
		}
		return isDeleted;
	}
	
	/**
	 * Deassigns all Comments from the news message with the given ID.
	 * 
	 * @param newsId
	 *            The ID of the News message
	 * @return {@code true} if Comments have been successfully deassigned and
	 *         {@code false} otherwise
	 * @throws DAOException
	 *             If a database access/handling error occurs.
	 */
	public boolean deassignCommentsFromNews(Long newsId) throws DAOException {
		PreparedStatement prepStatement = null;
		boolean isDeAssigned = false;
		Connection conn = getConnection();

		try {
			prepStatement = conn.prepareStatement(SQL_DEASSIGN_COMMENTS_QUERY);
			prepStatement.setLong(1, newsId);
			prepStatement.executeUpdate();
			isDeAssigned = true;
		} catch (SQLException ex) {
			throw new DAOException("Database error", ex);
		} finally {
			DBUtils.close(prepStatement, conn);
		}
		return isDeAssigned;
	}

	/* Gets the current Timestamp */
	private Timestamp obtainTimestamp() {
		TimeZone zone = TimeZone.getTimeZone("GMT+3");
		Calendar calendar = Calendar.getInstance(zone, Locale.getDefault());
		Date date = calendar.getTime();
		return new Timestamp(date.getTime());
	}
}
