package com.epam.newsmanagement.bean;

import java.io.Serializable;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Class {@code NewsVO} is a Value Object that stores the extended data of news
 * messages (the news message itself, its author, list of tags and list of
 * comments).
 * 
 * @author Vasili Andreev
 * @version 1.0
 * @see com.epam.newsmanagement.bean.News
 */
@Component
@Scope("prototype")
public class NewsVO implements Serializable {
	private static final long serialVersionUID = 4556269555477843455L;
	private News newsMessage;
	private Author author;
	private Set<Comment> listOfComments;
	private Set<Tag> listOfTags;

	/**
	 * Returns the news message itself.
	 * 
	 * @return News newsMessage
	 * @see com.epam.newsmanagement.bean.News
	 */
	public News getNewsMessage() {
		return newsMessage;
	}

	/**
	 * Sets the news message.
	 * 
	 * @param newsMessage
	 *            the news message
	 * @see com.epam.newsmanagement.bean.News
	 */
	@Autowired
	public void setNewsMessage(News newsMessage) {
		this.newsMessage = newsMessage;
	}

	/**
	 * Returns the news author.
	 * 
	 * @return Author author
	 * @see com.epam.newsmanagement.bean.Author
	 */
	public Author getAuthor() {
		return author;
	}

	/**
	 * Sets the news author.
	 * 
	 * @param author
	 *            the news author
	 * @see com.epam.newsmanagement.bean.Author
	 */
	@Autowired
	public void setAuthor(Author author) {
		this.author = author;
	}

	/**
	 * Returns the list of comments to the piece of news.
	 * 
	 * @return the list of Comments
	 * @see com.epam.newsmanagement.bean.Comment
	 * @see java.util.Set
	 */
	public Set<Comment> getListOfComments() {
		return listOfComments;
	}

	/**
	 * Sets the list of comments to the piece of news.
	 * 
	 * @param listOfComments
	 *            the list of comments
	 * @see com.epam.newsmanagement.bean.Comment
	 * @see java.util.Set
	 */
	public void setListOfComments(Set<Comment> listOfComments) {
		this.listOfComments = listOfComments;
	}

	/**
	 * Returns the list of news tags.
	 * 
	 * @return the list of news tags
	 * @see com.epam.newsmanagement.bean.Tag
	 * @see java.util.Set
	 */
	public Set<Tag> getListOfTags() {
		return listOfTags;
	}

	/**
	 * Sets the list of news tags.
	 * 
	 * @param listOfTags
	 *            the list of news tags
	 * @see com.epam.newsmanagement.bean.Tag
	 * @see java.util.Set
	 */
	public void setListOfTags(Set<Tag> listOfTags) {
		this.listOfTags = listOfTags;
	}
}
