package com.epam.newsmanagement.util;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.log4j.Logger;

/**
 * Abstract class {@code DBUtils} contains supplementary static methods that
 * allow to close ResultSet and/or Statement.
 * 
 * @author Vasili Andreev
 * @version 1.0
 */
public abstract class DBUtils {
	private static final Logger LOG = Logger.getLogger(DBUtils.class);

	/**
	 * Closes the opened ResultSet, Statement and Connection.
	 * 
	 * @param resultSet
	 *            java.sql.ResultSet
	 * @param statement
	 *            java.sql.Statement
	 * @param connection
	 *            java.sql.Connection
	 */
	public static void close(ResultSet resultSet, Statement statement,
			Connection connection) {
		try {
			if (resultSet != null) {
				resultSet.close();
			}
			if (statement != null) {
				statement.close();
			}
			if (connection != null) {
				connection.close();
			}
		} catch (SQLException ex) {
			LOG.error("Error. Database access failure!", ex);
		}
	}

	/**
	 * Closes the opened Statement and Connection.
	 * 
	 * @param statement
	 *            java.sql.Statement
	 * @param connection
	 *            java.sql.Connection
	 */
	public static void close(Statement statement, Connection connection) {
		try {
			if (statement != null) {
				statement.close();
			}
			if (connection != null) {
				connection.close();
			}
		} catch (SQLException ex) {
			LOG.error("Error. Database access failure!", ex);
		}
	}
}
