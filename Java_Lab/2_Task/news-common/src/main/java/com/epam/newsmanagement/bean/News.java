package com.epam.newsmanagement.bean;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Class {@code News} is a Java Bean that stores the data of news messages (ID,
 * title, text, creation date, etc.).
 * 
 * @author Vasili Andreev
 * @version 1.0
 */
@Component
@Scope("prototype")
public class News implements Serializable {
	private static final long serialVersionUID = -1454437699161184243L;
	private Long newsId;
	private String title;
	private String shortText;
	private String fullText;
	private Timestamp creationDate;
	private Date modDate;

	/**
	 * Returns the ID of the news message.
	 * 
	 * @return news message ID
	 */
	public Long getNewsId() {
		return newsId;
	}

	/**
	 * Sets the ID of the news message.
	 * 
	 * @param newsId
	 *            the news message ID
	 */
	public void setNewsId(Long newsId) {
		this.newsId = newsId;
	}

	/**
	 * Returns the news title.
	 * 
	 * @return news title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Sets the news title.
	 * 
	 * @param title
	 *            the news title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Returns the news short text.
	 * 
	 * @return news short text
	 */
	public String getShortText() {
		return shortText;
	}

	/**
	 * Sets the news short text.
	 * 
	 * @param shortText
	 *            the news short text
	 */
	public void setShortText(String shortText) {
		this.shortText = shortText;
	}

	/**
	 * Returns the news full text.
	 * 
	 * @return news full text
	 */
	public String getFullText() {
		return fullText;
	}

	/**
	 * Sets the news full text.
	 * 
	 * @param fullText
	 *            the news full text
	 */
	public void setFullText(String fullText) {
		this.fullText = fullText;
	}

	/**
	 * Returns the news creation date.
	 * 
	 * @return news creation date
	 */
	public Timestamp getCreationDate() {
		return creationDate;
	}

	/**
	 * Sets the the news creation date.
	 * 
	 * @param creationDate
	 *            the news creation date
	 */
	public void setCreationDate(Timestamp creationDate) {
		this.creationDate = creationDate;
	}

	/**
	 * Returns the news modif. date.
	 * 
	 * @return news modif. date
	 */
	public Date getModDate() {
		return modDate;
	}

	/**
	 * Sets the the news modif. date.
	 * 
	 * @param modDate
	 *            the news modif. date
	 */
	public void setModDate(Date modDate) {
		this.modDate = modDate;
	}

	@Override
	public boolean equals(Object object) {
		if (object == this) {
			return true;
		}
		if (object == null || object.getClass() != this.getClass()) {
			return false;
		}
		News otherNews = (News) object;
		if (newsId == null) {
			if (otherNews.getNewsId() != null) {
				return false;
			}
		} else if (newsId != otherNews.getNewsId()) {
			return false;
		}
		if (title == null) {
			if (otherNews.getTitle() != null) {
				return false;
			}
		} else if (!title.equals(otherNews.getTitle())) {
			return false;
		}
		if (shortText == null) {
			if (otherNews.getShortText() != null) {
				return false;
			}
		} else if (!shortText.equals(otherNews.getShortText())) {
			return false;
		}
		if (fullText == null) {
			if (otherNews.getFullText() != null) {
				return false;
			}
		} else if (!fullText.equals(otherNews.getFullText())) {
			return false;
		}
		if (creationDate == null) {
			if (otherNews.getCreationDate() != null) {
				return false;
			}
		} else if (!creationDate.equals(otherNews.getCreationDate())) {
			return false;
		}
		if (modDate == null) {
			if (otherNews.getModDate() != null) {
				return false;
			}
		} else if (!modDate.equals(otherNews.getModDate())) {
			return false;
		}
		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * ((newsId == null) ? 0 : newsId.intValue())
				+ ((title == null) ? 0 : title.hashCode())
				+ ((shortText == null) ? 0 : shortText.hashCode())
				+ ((fullText == null) ? 0 : fullText.hashCode())
				+ ((creationDate == null) ? 0 : creationDate.hashCode())
				+ ((modDate == null) ? 0 : modDate.hashCode());
		return result;
	}
}
