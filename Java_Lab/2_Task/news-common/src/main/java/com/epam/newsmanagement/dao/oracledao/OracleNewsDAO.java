package com.epam.newsmanagement.dao.oracledao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.TimeZone;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.epam.newsmanagement.bean.News;
import com.epam.newsmanagement.dao.AbstractDAO;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.search.news.NewsSearchCriteria;
import com.epam.newsmanagement.search.news.NewsSearchCriteria.AuthorSearchType;
import com.epam.newsmanagement.util.DBUtils;

import static com.epam.newsmanagement.search.news.NewsSearchCriteria.AuthorSearchType.*;

/**
 * Class {@code OracleNewsDAO} contains methods allowing to extract information
 * about News found in the application, add, update, and delete their data.
 * 
 * @author Vasili Andreev
 * @version 1.0
 * @see com.epam.newsmanagement.bean.News
 * @see com.epam.newsmanagement.dao.AbstractDAO
 */
@Component
@Scope("prototype")
public class OracleNewsDAO extends AbstractDAO<News> {
	private static final String NEWS_ID = "NEWS_ID";
	private static final String TITLE = "TITLE";
	private static final String SHORT_TEXT = "SHORT_TEXT";
	private static final String FULL_TEXT = "FULL_TEXT";
	private static final String CR_DATE = "CREATION_DATE";
	private static final String MOD_DATE = "MODIFICATION_DATE";
	private static final String SQL_FIND_NEWS_QUERY = "SELECT NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT,"
			+ " CREATION_DATE, MODIFICATION_DATE FROM NEWS";
	private static final String SQL_FIND_NEWS_BY_ID_QUERY = "SELECT NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT,"
			+ " CREATION_DATE, MODIFICATION_DATE FROM NEWS WHERE NEWS_ID = ?";
	private static final String SQL_FIND_NEWS_ID_BY_TITLE_QUERY = "SELECT NEWS_ID FROM NEWS WHERE TITLE = ?";
	private static final String SQL_ADD_NEWS_QUERY = "INSERT INTO NEWS (TITLE, SHORT_TEXT, FULL_TEXT, "
			+ "CREATION_DATE, MODIFICATION_DATE) VALUES (?, ?, ?, ?, ?)";
	private static final String SQL_UPDATE_NEWS_QUERY = "UPDATE NEWS SET TITLE = ?, SHORT_TEXT = ?, "
			+ "FULL_TEXT = ?, MODIFICATION_DATE = ? WHERE NEWS_ID = ?";
	private static final String SQL_DELETE_NEWS_QUERY = "DELETE FROM NEWS WHERE NEWS_ID = ?";
	private static final String SQL_SEARCH_PART = "SELECT NEWS.NEWS_ID, NEWS.TITLE, NEWS.SHORT_TEXT, "
			+ "NEWS.FULL_TEXT, NEWS.CREATION_DATE, NEWS.MODIFICATION_DATE, AUTHORS.AUTHOR_ID, AUTHORS.AUTHOR_NAME, "
			+ "TAGS.TAG_NAME FROM NEWS JOIN NEWS_AUTHORS ON NEWS_AUTHORS.NEWS_ID = NEWS.NEWS_ID "
			+ "JOIN AUTHORS ON AUTHORS.AUTHOR_ID = NEWS_AUTHORS.AUTHOR_ID LEFT OUTER JOIN NEWS_TAGS "
			+ "ON NEWS_TAGS.NEWS_ID = NEWS.NEWS_ID LEFT OUTER JOIN TAGS ON TAGS.TAG_ID = NEWS_TAGS.TAG_ID";

	/* finds all News in the application */
	@Override
	public Set<News> findAll() throws DAOException {
		ResultSet rs = null;
		PreparedStatement prepStatement = null;
		Set<News> newsList = new HashSet<>();
		Connection conn = getConnection();

		try {
			prepStatement = conn.prepareStatement(SQL_FIND_NEWS_QUERY);
			rs = prepStatement.executeQuery();
			while (rs.next()) {
				/* creating a new News message bean and initializing its fields */
				News news = new News();
				initializeNewsFields(rs, news);
				newsList.add(news);
			}
		} catch (SQLException ex) {
			throw new DAOException("Database error", ex);
		} finally {
			DBUtils.close(rs, prepStatement, conn);
		}
		return newsList;
	}

	/* returns News message with the given ID */
	@Override
	public News findEntityById(Long newsId) throws DAOException {
		News news = null;
		ResultSet rs = null;
		PreparedStatement prepStatement = null;
		Connection conn = getConnection();

		try {
			prepStatement = conn.prepareStatement(SQL_FIND_NEWS_BY_ID_QUERY);
			prepStatement.setLong(1, newsId);
			rs = prepStatement.executeQuery();
			while (rs.next()) {
				/* creating a new News message bean and initializing its fields */
				news = new News();
				initializeNewsFields(rs, news);
			}
		} catch (SQLException ex) {
			throw new DAOException("Database error", ex);
		} finally {
			DBUtils.close(rs, prepStatement, conn);
		}
		return news;
	}

	/**
	 * Finds the ID of the News message by its unique title.
	 * 
	 * @param title
	 *            The title of the News message
	 * @return The ID of the News message
	 * @throws DAOException
	 *             If a database access/handling error occurs.
	 */
	public Long findNewsIdByTitle(String title) throws DAOException {
		Long newsID = null;
		ResultSet rs = null;
		PreparedStatement prepStatement = null;
		Connection conn = getConnection();

		try {
			prepStatement = conn.prepareStatement(SQL_FIND_NEWS_ID_BY_TITLE_QUERY);
			prepStatement.setString(1, title);
			rs = prepStatement.executeQuery();
			while (rs.next()) {
				newsID = rs.getLong(NEWS_ID);
			}
		} catch (SQLException ex) {
			throw new DAOException("Database error", ex);
		} finally {
			DBUtils.close(rs, prepStatement, conn);
		}
		return newsID;
	}

	/* adds News message to the database */
	@Override
	public boolean addNewEntity(News news) throws DAOException {
		PreparedStatement prepStatement = null;
		boolean isAdded = false;
		Connection conn = getConnection();

		try {
			prepStatement = conn.prepareStatement(SQL_ADD_NEWS_QUERY);
			prepStatement.setString(1, news.getTitle());
			prepStatement.setString(2, news.getShortText());
			prepStatement.setString(3, news.getFullText());
			prepStatement.setTimestamp(4, obtainTimestamp());
			prepStatement.setDate(5, obtainDate());
			prepStatement.executeUpdate();
			isAdded = true;
		} catch (SQLException ex) {
			throw new DAOException("Database error", ex);
		} finally {
			DBUtils.close(prepStatement, conn);
		}
		return isAdded;
	}

	/* updates News message data */
	@Override
	public boolean updateEntity(News news, Long newsId) throws DAOException {
		PreparedStatement prepStatement = null;
		boolean isUpdated = false;
		Connection conn = getConnection();

		try {
			prepStatement = conn.prepareStatement(SQL_UPDATE_NEWS_QUERY);
			prepStatement.setString(1, news.getTitle());
			prepStatement.setString(2, news.getShortText());
			prepStatement.setString(3, news.getFullText());
			prepStatement.setDate(4, obtainDate());
			prepStatement.setLong(5, newsId);
			prepStatement.executeUpdate();
			isUpdated = true;
		} catch (SQLException ex) {
			throw new DAOException("Database error", ex);
		} finally {
			DBUtils.close(prepStatement, conn);
		}
		return isUpdated;
	}

	/* deletes news message and dependencies from the database */
	@Override
	public boolean deleteEntity(Long newsId) throws DAOException {
		PreparedStatement prepStatement = null;
		boolean isDeleted = false;
		Connection conn = getConnection();

		try {
			prepStatement = conn.prepareStatement(SQL_DELETE_NEWS_QUERY);
			prepStatement.setLong(1, newsId);
			prepStatement.executeUpdate();
			isDeleted = true;
		} catch (SQLException ex) {
			throw new DAOException("Database error", ex);
		} finally {
			DBUtils.close(prepStatement, conn);
		}
		return isDeleted;
	}

	/**
	 * Finds News messages according to a given search criteria.
	 * 
	 * @param searchCriteria
	 *            the Search criteria
	 * @return The list of news messages
	 * @throws DAOException
	 *             If a database access/handling error occurs.
	 */
	public Set<News> searchNewsByAuthorAndTags(NewsSearchCriteria searchCriteria)
			throws DAOException {
		ResultSet rs = null;
		PreparedStatement prepStatement = null;
		Set<News> newsList = new HashSet<>();
		String query = buildSearchQuery(searchCriteria);
		Connection conn = getConnection();

		try {
			prepStatement = conn.prepareStatement(query);
			rs = prepStatement.executeQuery();
			while (rs.next()) {
				/* creating a new News message bean and initializing its fields */
				News news = new News();
				initializeNewsFields(rs, news);
				newsList.add(news);
			}
		} catch (SQLException ex) {
			throw new DAOException("Database error", ex);
		} finally {
			DBUtils.close(rs, prepStatement, conn);
		}
		return newsList;
	}

	/* builds query on the basis of search criteria */
	private String buildSearchQuery(NewsSearchCriteria searchCriteria) {
		StringBuilder builder = new StringBuilder();
		builder.append(SQL_SEARCH_PART);
		Set<String> tagsList = searchCriteria.getTagsNames();
		String authorName = searchCriteria.getAuthorName();
		Long authorId = searchCriteria.getAuthorId();
		AuthorSearchType searchType = searchCriteria.getAuthorSearch();
		boolean isSearched = false;
		boolean tagsPresent = tagsList != null && !tagsList.isEmpty();

		if ((searchType == BY_NAME && authorName != null)
				|| (searchType == BY_ID && authorId != null) 
				|| tagsPresent) {
			builder.append(" WHERE ");
		}
		isSearched = addAuthorSearch(searchType, authorName, authorId, builder);
		if (isSearched && tagsPresent) {
			builder.append(" OR ");
		}
		addTagsSearch(tagsList, builder);
		return builder.toString();
	}

	/* adds author search criterion */
	private boolean addAuthorSearch(AuthorSearchType searchType,
			String authorName, Long authorId, StringBuilder builder) {
		boolean isAuthorSearched = false;

		if (searchType == BY_NAME && authorName != null) {
			builder.append("AUTHORS.AUTHOR_NAME = '");
			builder.append(authorName).append("'");
			isAuthorSearched = true;
		} else if (searchType == BY_ID && authorId != null) {
			builder.append("AUTHORS.AUTHOR_ID = ");
			builder.append(authorId);
			isAuthorSearched = true;
		}
		return isAuthorSearched;
	}

	/* adds tags search criterion */
	private void addTagsSearch(Set<String> tagsList, StringBuilder builder) {
		if (tagsList != null && !tagsList.isEmpty()) {
			List<String> tempList = new ArrayList<>();
			tempList.addAll(tagsList);

			for (int i = 0; i < tempList.size(); i++) {
				builder.append("TAGS.TAG_NAME = '");
				builder.append(tempList.get(i)).append("'");
				if (i + 1 < tempList.size()) {
					builder.append(" OR ");
				}
			}
		}
	}

	/* initializes news fields */
	private void initializeNewsFields(ResultSet rs, News news)
			throws SQLException, DAOException {
		news.setNewsId(rs.getLong(NEWS_ID));
		news.setTitle(rs.getString(TITLE));
		news.setShortText(rs.getString(SHORT_TEXT));
		news.setFullText(rs.getString(FULL_TEXT));
		news.setCreationDate(rs.getTimestamp(CR_DATE));
		news.setModDate(rs.getDate(MOD_DATE));
	}

	/* Gets the current Timestamp */
	private Timestamp obtainTimestamp() {
		TimeZone zone = TimeZone.getTimeZone("GMT+3");
		Calendar calendar = Calendar.getInstance(zone, Locale.getDefault());
		java.util.Date date = calendar.getTime();
		return new Timestamp(date.getTime());
	}

	/* Gets the current Date */
	private Date obtainDate() {
		TimeZone zone = TimeZone.getTimeZone("GMT+3");
		Calendar calendar = Calendar.getInstance(zone, Locale.getDefault());
		java.util.Date date = calendar.getTime();
		return new Date(date.getTime());
	}
}
