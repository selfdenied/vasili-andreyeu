package com.epam.newsmanagement.bean;

import java.io.Serializable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Class {@code UserVO} is a Value Object that stores the extended data of users
 * (the user data plus his/her role).
 * 
 * @author Vasili Andreev
 * @version 1.0
 * @see com.epam.newsmanagement.bean.User
 */
@Component
@Scope("prototype")
public class UserVO implements Serializable {
	private static final long serialVersionUID = -723420828378947863L;
	private User user;
	private String roleName;

	/**
	 * Returns the user itself.
	 * 
	 * @return user
	 * @see com.epam.newsmanagement.bean.User
	 */
	public User getUser() {
		return user;
	}

	/**
	 * Sets the user.
	 * 
	 * @param user
	 *            the user
	 * @see com.epam.newsmanagement.bean.User
	 */
	@Autowired
	public void setUser(User user) {
		this.user = user;
	}

	/**
	 * Returns the user role name.
	 * 
	 * @return user's role name
	 */
	public String getRoleName() {
		return roleName;
	}

	/**
	 * Sets the user role name.
	 * 
	 * @param roleName
	 *            user's role name
	 */
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
}
