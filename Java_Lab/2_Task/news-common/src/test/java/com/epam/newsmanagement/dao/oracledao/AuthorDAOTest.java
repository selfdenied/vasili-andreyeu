package com.epam.newsmanagement.dao.oracledao;

import java.util.Set;

import org.junit.Assert;

import com.epam.newsmanagement.bean.Author;
import com.epam.newsmanagement.dao.AbstractDAOTest;

/**
 * Class {@code AuthorDAOTest} contains a number of methods that test the proper
 * functioning of C.R.U.D operations realized by AuthorDAO methods.
 * 
 * @author Vasili Andreev
 * @version 1.0
 * @see com.epam.newsmanagement.dao.AbstractDAOTest
 * @see com.epam.newsmanagement.dao.oracledao.OracleAuthorDAO
 */
public class AuthorDAOTest extends AbstractDAOTest {
	private static final String Q_TABLE_NAME = "SYS.AUTHORS";
	private OracleAuthorDAO authorDAO;
	
	/**
	 * Constructs AuthorDAOTest object
	 */
	public AuthorDAOTest() {
		this.authorDAO = getCtx().getBean(OracleAuthorDAO.class);
		authorDAO.setTestMode(true);
	}

	/**
	 * Tests the correctness of loaded dataset.
	 * 
	 * @throws Exception
	 *             If Exception of some kind has occurred.
	 */
	public void testDataLoaded() throws Exception {
		int rows = 20;
		Assert.assertNotNull(getDataSet());
		int rowCount = getDataSet().getTable(Q_TABLE_NAME).getRowCount();
		Assert.assertEquals(rows, rowCount);
	}

	/**
	 * Tests findEntityByID, findAuthorByNewsId and findAll methods of
	 * AuthorDAO.
	 * 
	 * @throws Exception
	 *             If exception of some kind has occurred.
	 */
	public void testExctractMethods() throws Exception {
		Long authorId = 1L;
		Long newsId = 11L;
		Set<Author> authorsList = authorDAO.findAll();
		Author author = authorDAO.findEntityById(authorId);
		Author newsAuthor = authorDAO.findAuthorByNewsId(newsId);

		// tests the correctness of gained information
		Assert.assertNotNull(authorsList);
		Assert.assertFalse(authorsList.isEmpty());
		Assert.assertNotNull(author);
		Assert.assertEquals("Василий Андреев", author.getAuthorName());
		Assert.assertNull(author.getExpired());
		Assert.assertNotNull(newsAuthor);
		Assert.assertEquals("Arnie Schwarz", newsAuthor.getAuthorName());
		Assert.assertNull(newsAuthor.getExpired());
	}

	/**
	 * Tests updateEntity method of AuthorDAO.
	 * 
	 * @throws Exception
	 *             If exception of some kind has occurred.
	 */
	public void testUpdateMethod() throws Exception {
		Long authorId = 14L;
		boolean isUpdated = false;
		Author author = authorDAO.findEntityById(authorId);
		Assert.assertNotNull(author);
		Assert.assertEquals("David Gill", author.getAuthorName());
		Assert.assertNull(author.getExpired());

		// update author's name
		author.setAuthorName("Andrew Kit");
		isUpdated = authorDAO.updateEntity(author, authorId);

		// test the correctness of update method
		Author newAuthor = authorDAO.findEntityById(authorId);
		Assert.assertEquals("Andrew Kit", newAuthor.getAuthorName());
		Assert.assertTrue(isUpdated);
	}

	/**
	 * Tests addNewEntity method of AuthorDAO.
	 * 
	 * @throws Exception
	 *             If exception of some kind has occurred.
	 */
	public void testAddMethod() throws Exception {
		String name = "Michael Carrick";
		boolean isAdded = false;
		boolean isPresent = false;

		/* tests that no authors with such name exist */
		Set<Author> authorsList = authorDAO.findAll();
		for (Author auth : authorsList) {
			Assert.assertNotEquals(name, auth.getAuthorName());
		}
		Author author = new Author();
		author.setAuthorName(name);

		// add new author
		isAdded = authorDAO.addNewEntity(author);

		// test the correctness of add method
		Set<Author> newAuthorsList = authorDAO.findAll();
		Assert.assertEquals(authorsList.size() + 1, newAuthorsList.size());
		for (Author auth : newAuthorsList) {
			if (name.equals(auth.getAuthorName())) {
				isPresent = true;
			}
		}
		Assert.assertTrue(isAdded);
		Assert.assertTrue(isPresent);
	}

	/**
	 * Tests deleteEntity method of AuthorDAO.
	 * 
	 * @throws Exception
	 *             If exception of some kind has occurred.
	 */
	public void testDeleteMethod() throws Exception {
		Long authorId = 20L;
		boolean isDeleted = false;
		Author author = authorDAO.findEntityById(authorId);
		Assert.assertNotNull(author);
		Assert.assertNull(author.getExpired());

		// delete author (set his status to 'Expired')
		isDeleted = authorDAO.deleteEntity(authorId);

		// test the correctness of delete method
		Author newAuthor = authorDAO.findEntityById(authorId);
		Assert.assertNotNull(newAuthor);
		Assert.assertNotNull(newAuthor.getExpired());
		Assert.assertTrue(isDeleted);
	}

	/**
	 * Tests assignAuthorToNews and deassignAuthorFromNews methods of AuthorDAO.
	 * 
	 * @throws Exception
	 *             If exception of some kind has occurred.
	 */
	public void testAssignMethods() throws Exception {
		Long authorId = 1L;
		Long newsId = 4L; // no authors assigned for this id
		boolean isDeassigned = false;
		Author author = authorDAO.findAuthorByNewsId(newsId);
		Assert.assertNull(author);

		// assign the author to the given news message
		authorDAO.assignAuthorToNews(newsId, authorId);

		// test that the method works properly
		author = authorDAO.findAuthorByNewsId(newsId);
		Assert.assertNotNull(author);
		Assert.assertEquals("Василий Андреев", author.getAuthorName());

		// deassign the author from the given news message
		isDeassigned = authorDAO.deassignAuthorFromNews(newsId);

		// test that the method works properly
		author = authorDAO.findAuthorByNewsId(newsId);
		Assert.assertNull(author);
		Assert.assertTrue(isDeassigned);
	}
}
