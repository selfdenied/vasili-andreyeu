package com.epam.newsmanagement.dao;

import java.io.File;
import java.net.URL;

import oracle.ucp.jdbc.PoolDataSource;

import org.dbunit.DatabaseTestCase;
import org.dbunit.database.DatabaseConfig;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.operation.DatabaseOperation;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Abstract class {@code AbstractDAOTest} provides realization of a number of
 * common methods needed for proper functioning of DBUnit. The methods are used
 * by DBUnit to establish database connection, perform set up and tear down
 * operations, etc.
 * 
 * @author Vasili Andreev
 * @version 1.0
 */
public abstract class AbstractDAOTest extends DatabaseTestCase {
	private static final String USER = "sys as sysdba";
	private static final String FILE_NAME = "/db_data.xml";
	private static ClassPathXmlApplicationContext ctx;
	private IDataSet loadedDataSet;

	static {
		ctx = new ClassPathXmlApplicationContext("spring.xml");
	}
	
	/**
	 * Returns the instance of Spring Context.
	 * 
	 * @return org.springframework.context.support.ClassPathXmlApplicationContext
	 */
	protected static ClassPathXmlApplicationContext getCtx() {
		return ctx;
	}
	
	/**
	 * Establishes connection to the database.
	 * 
	 * @return org.dbunit.database.DatabaseConnection
	 * @throws Exception
	 *             If exception of some kind has occurred.
	 */
	@Override
	protected IDatabaseConnection getConnection() throws Exception {
		PoolDataSource poolDataSource = ctx.getBean(PoolDataSource.class);
		poolDataSource.setUser(USER);
		return new DatabaseConnection(poolDataSource.getConnection());
	}

	/**
	 * Loads dataset from XML file into the database.
	 * 
	 * @return org.dbunit.dataset.IDataSet (loaded dataset)
	 * @throws Exception
	 *             If exception of some kind has occurred.
	 */
	@Override
	protected IDataSet getDataSet() throws Exception {
		URL url = this.getClass().getResource(FILE_NAME);
		File xmlFile = new File(url.getFile());
		loadedDataSet = new FlatXmlDataSetBuilder().build(xmlFile);
		return loadedDataSet;
	}

	/**
	 * Performs set up operations with the database.
	 * 
	 * @return org.dbunit.operation.DatabaseOperation
	 * @throws Exception
	 *             If exception of some kind has occurred.
	 */
	@Override
	protected DatabaseOperation getSetUpOperation() throws Exception {
		return DatabaseOperation.CLEAN_INSERT;
	}

	/**
	 * Performs tear down operations with the database.
	 * 
	 * @return org.dbunit.operation.DatabaseOperation
	 * @throws Exception
	 *             If exception of some kind has occurred.
	 */
	@Override
	protected DatabaseOperation getTearDownOperation() throws Exception {
		return DatabaseOperation.NONE;
	}

	/**
	 * Performs additional set up operations with the database. Sets additional
	 * features and properties.
	 * 
	 * @param config
	 *            org.dbunit.database.DatabaseConfig object
	 */
	@Override
	protected void setUpDatabaseConfig(DatabaseConfig config) {
		config.setProperty(DatabaseConfig.FEATURE_QUALIFIED_TABLE_NAMES, true);
	}
}
