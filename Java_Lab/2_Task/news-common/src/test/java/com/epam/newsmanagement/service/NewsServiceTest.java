package com.epam.newsmanagement.service;

import java.sql.Timestamp;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.*;

import com.epam.newsmanagement.bean.*;
import com.epam.newsmanagement.dao.oracledao.*;
import com.epam.newsmanagement.exception.*;

/**
 * Class {@code NewsServiceTest} contains a number of methods that test the
 * proper functioning of service methods of NewsService class.
 * 
 * @author Vasili Andreev
 * @version 1.0
 * @see com.epam.newsmanagement.service.NewsService
 */
public class NewsServiceTest {
	@Mock
	private OracleTagDAO tagDAO;
	@Mock
	private OracleNewsDAO newsDAO;
	@Mock
	private OracleCommentDAO commentDAO;
	@Mock
	private OracleAuthorDAO authorDAO;
	private NewsService newsService;

	/**
	 * Sets up the mock instance and initializes the NewsService object.
	 */
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		this.newsService = new NewsService();
		newsService.setTagDAO(tagDAO);
		newsService.setNewsDAO(newsDAO);
		newsService.setCommentDAO(commentDAO);
		newsService.setAuthorDAO(authorDAO);
	}

	/**
	 * Tests the proper functioning of countNews() method of NewsService class
	 * in a trouble-free case.
	 */
	@Test
	public void shouldCountNewsTest() throws Exception {
		int size = 0;
		Set<News> newsList = new HashSet<>();
		News news = new News();
		newsList.add(news);
		/* stubbing */
		when(newsDAO.findAll()).thenReturn(newsList);
		/* setting expectations and verifying */
		size = newsService.countNews();
		Assert.assertEquals(1, size);
		verify(newsDAO, times(1)).findAll();
	}

	/**
	 * Tests the proper functioning of countNews() method of NewsService class
	 * in the case when DAOException is thrown.
	 */
	@Test
	public void shouldNotCountNewsTest() throws DAOException {
		int size = 0;
		Set<News> newsList = null;
		/* stubbing */
		when(newsDAO.findAll()).thenThrow(new DAOException("Database error"));
		/* setting expectations and verifying */
		try {
			size = newsService.countNews();
		} catch (DAOException ex) {
			Assert.assertEquals("Database error", ex.getMessage());
		}
		Assert.assertNull(newsList);
		Assert.assertEquals(0, size);
		verify(newsDAO, times(1)).findAll();
	}

	/**
	 * Tests the proper functioning of obtainAllNewsSorted() method of
	 * NewsService class in a trouble-free case.
	 */
	@Test
	public void shouldFindAllNewsTest() throws Exception {
		Long newsId = 1L;
		Long newsId2 = 2L;
		Set<News> newsList = new HashSet<>();
		Set<Tag> tagsList = new HashSet<>();
		Set<Comment> commentsList = new HashSet<>();
		News news1 = new News();
		News news2 = new News();
		news1.setNewsId(newsId);
		news2.setNewsId(newsId2);
		Author author = new Author();
		newsList.add(news2);
		newsList.add(news1);
		tagsList.add(new Tag());
		commentsList.add(new Comment());
		/* stubbing */
		when(newsDAO.findAll()).thenReturn(newsList);
		when(tagDAO.findAllTagsByNewsId(newsId)).thenReturn(tagsList);
		when(tagDAO.findAllTagsByNewsId(newsId2)).thenReturn(tagsList);
		when(commentDAO.findCommentsByNewsId(newsId)).thenReturn(commentsList);
		when(commentDAO.findCommentsByNewsId(newsId2)).thenReturn(new HashSet<Comment>());
		when(authorDAO.findAuthorByNewsId(newsId)).thenReturn(author);
		when(authorDAO.findAuthorByNewsId(newsId2)).thenReturn(author);
		/* setting expectations and verifying */
		List<NewsVO> resultList = newsService.obtainAllNewsSorted();
		/* there should be 2 news messages */
		Assert.assertEquals(2, resultList.size());
		Assert.assertNotNull(resultList.get(0).getAuthor());
		Assert.assertEquals(1, resultList.get(0).getListOfTags().size());
		/* 1 comment for news1 message that should be the first one in the list */
		Assert.assertEquals(1, resultList.get(0).getListOfComments().size());
		Assert.assertNotNull(resultList.get(1).getAuthor());
		Assert.assertEquals(1, resultList.get(1).getListOfTags().size());
		/* 0 comments for news2 message that should be the second in the list */
		Assert.assertEquals(0, resultList.get(1).getListOfComments().size());
		verify(newsDAO, times(1)).findAll();
		verify(tagDAO, times(1)).findAllTagsByNewsId(newsId);
		verify(tagDAO, times(1)).findAllTagsByNewsId(newsId2);
		verify(commentDAO, times(1)).findCommentsByNewsId(newsId);
		verify(commentDAO, times(1)).findCommentsByNewsId(newsId2);
		verify(authorDAO, times(1)).findAuthorByNewsId(newsId);
		verify(authorDAO, times(1)).findAuthorByNewsId(newsId2);
	}

	/**
	 * Tests the proper functioning of obtainAllNewsSorted() method of
	 * NewsService class in the case when DAOException is thrown.
	 */
	@Test
	public void shouldNotFindAllNewsTest() throws DAOException {
		Long newsId = 1L;
		List<NewsVO> newsList = null;
		/* stubbing */
		when(newsDAO.findAll()).thenThrow(new DAOException("Database error"));
		/* setting expectations and verifying */
		try {
			newsList = newsService.obtainAllNewsSorted();
		} catch (DAOException ex) {
			Assert.assertEquals("Database error", ex.getMessage());
		}
		Assert.assertNull(newsList);
		verify(newsDAO, times(1)).findAll();
		verify(tagDAO, never()).findAllTagsByNewsId(newsId);
		verify(commentDAO, never()).findCommentsByNewsId(newsId);
		verify(authorDAO, never()).findAuthorByNewsId(newsId);
	}

	/**
	 * Tests the proper functioning of obtainNewsMessage method of NewsService
	 * class in a trouble-free case.
	 */
	@Test
	public void shouldFindNewsMessageTest() throws Exception {
		Long newsId = 11L;
		Set<Tag> tagsList = new HashSet<>();
		Set<Comment> commentsList = new HashSet<>();
		News news = new News();
		Author author = new Author();
		tagsList.add(new Tag());
		commentsList.add(new Comment());
		/* stubbing */
		when(newsDAO.findEntityById(newsId)).thenReturn(news);
		when(tagDAO.findAllTagsByNewsId(newsId)).thenReturn(tagsList);
		when(commentDAO.findCommentsByNewsId(newsId)).thenReturn(commentsList);
		when(authorDAO.findAuthorByNewsId(newsId)).thenReturn(author);
		/* setting expectations and verifying */
		NewsVO resultNews = newsService.obtainNewsMessage(newsId);
		Assert.assertNotNull(resultNews);
		Assert.assertNotNull(resultNews.getListOfTags());
		Assert.assertNotNull(resultNews.getListOfComments());
		Assert.assertNotNull(resultNews.getAuthor());
		Assert.assertEquals(1, resultNews.getListOfTags().size());
		Assert.assertEquals(1, resultNews.getListOfComments().size());
		verify(newsDAO, times(1)).findEntityById(newsId);
		verify(tagDAO, times(1)).findAllTagsByNewsId(newsId);
		verify(commentDAO, times(1)).findCommentsByNewsId(newsId);
		verify(authorDAO, times(1)).findAuthorByNewsId(newsId);
	}
	
	/**
	 * Tests the proper functioning of obtainNewsMessage method of NewsService
	 * class in the case when DAOException is thrown.
	 */
	@Test
	public void shouldNotFindNewsMessageTest() throws DAOException {
		NewsVO newsVO = null;
		Long newsId = 11L;
		/* stubbing */
		when(newsDAO.findEntityById(newsId)).thenThrow(
				new DAOException("Database error"));
		/* setting expectations and verifying */
		try {
			newsVO = newsService.obtainNewsMessage(newsId);
		} catch (DAOException ex) {
			Assert.assertEquals("Database error", ex.getMessage());
		}
		Assert.assertNull(newsVO);
		verify(newsDAO, times(1)).findEntityById(newsId);
		verify(tagDAO, never()).findAllTagsByNewsId(newsId);
		verify(commentDAO, never()).findCommentsByNewsId(newsId);
		verify(authorDAO, never()).findAuthorByNewsId(newsId);
	}
	
	/**
	 * Tests the proper functioning of obtainNewsCommentsSorted method of 
	 * NewsService class in a trouble-free case.
	 */
	@Test
	public void shouldFindNewsCommentsTest() throws Exception {
		Long newsId = 13L;
		Set<Comment> commentsList = new HashSet<>();
		Comment comment1 = new Comment();
		Comment comment2 = new Comment();
		comment1.setCreationDate(new Timestamp(1000L));
		comment2.setCreationDate(new Timestamp(1L)); //earlier cr. date
		commentsList.add(comment1);
		commentsList.add(comment2);
		/* stubbing */
		when(commentDAO.findCommentsByNewsId(newsId)).thenReturn(commentsList);
		/* setting expectations and verifying */
		List<Comment> list = newsService.obtainNewsCommentsSorted(newsId);
		Assert.assertNotNull(list);
		Assert.assertFalse(list.isEmpty());
		/* the second comment should be the first in the list */
		/* since its creation date is more recent */
		Assert.assertEquals(0, list.indexOf(comment2));
		Assert.assertEquals(1, list.indexOf(comment1));
		verify(commentDAO, times(1)).findCommentsByNewsId(newsId);
	}
	
	/**
	 * Tests the proper functioning of obtainNewsCommentsSorted method of 
	 * NewsService class in the case when DAOException is thrown.
	 */
	@Test
	public void shouldNotFindNewsCommentsTest() throws Exception {
		Long newsId = 13L;
		List<Comment> list = null;
		/* stubbing */
		when(commentDAO.findCommentsByNewsId(newsId)).thenThrow(
				new DAOException("Database error"));
		/* setting expectations and verifying */
		try {
			list = newsService.obtainNewsCommentsSorted(newsId);
		} catch (DAOException ex) {
			Assert.assertEquals("Database error", ex.getMessage());
		}
		Assert.assertNull(list);
		verify(commentDAO, times(1)).findCommentsByNewsId(newsId);
	}

	/**
	 * Tests the proper functioning of addNewsMessage method of NewsService
	 * class in a trouble-free case.
	 */
	@Test
	public void shouldAddNewsTest() throws Exception {
		boolean isAdded = false;
		Long newsId = 1L;
		Long authorId = 10L;
		String title = "Title";
		News news = new News();
		news.setTitle(title);
		Set<Long> tagsIdsList = new HashSet<>();
		tagsIdsList.add(1L);
		tagsIdsList.add(2L);
		/* stubbing */
		when(newsDAO.addNewEntity(news)).thenReturn(true);
		when(newsDAO.findNewsIdByTitle(title)).thenReturn(newsId);
		when(authorDAO.assignAuthorToNews(newsId, authorId)).thenReturn(true);
		when(tagDAO.assignTagToNews(newsId, 1L)).thenReturn(true);
		when(tagDAO.assignTagToNews(newsId, 2L)).thenReturn(true);
		/* setting expectations and verifying */
		isAdded = newsService.addNewsMessage(news, authorId, tagsIdsList);
		Assert.assertTrue(isAdded);
		verify(newsDAO, times(1)).addNewEntity(news);
		verify(newsDAO, times(1)).findNewsIdByTitle(title);
		verify(authorDAO, times(1)).assignAuthorToNews(newsId, authorId);
		verify(tagDAO, times(1)).assignTagToNews(newsId, 1L);
		verify(tagDAO, times(1)).assignTagToNews(newsId, 2L);
	}

	/**
	 * Tests the proper functioning of addNewsMessage method of NewsService
	 * class in the case when DAOException is thrown.
	 */
	@Test
	public void shouldNotAddNewsTest() throws DAOException {
		boolean isAdded = true;
		Long newsId = 1L;
		Long authorId = 10L;
		String title = "Title";
		News news = new News();
		Set<Long> tagsIdsList = new HashSet<>();
		/* stubbing */
		when(newsDAO.addNewEntity(news)).thenThrow(
				new DAOException("Database error"));
		/* setting expectations and verifying */
		try {
			isAdded = newsService.addNewsMessage(news, authorId, tagsIdsList);
		} catch (DAOException ex) {
			isAdded = false;
			Assert.assertEquals("Database error", ex.getMessage());
		}
		Assert.assertFalse(isAdded);
		verify(newsDAO, times(1)).addNewEntity(news);
		verify(newsDAO, never()).findNewsIdByTitle(title);
		verify(authorDAO, never()).assignAuthorToNews(newsId, authorId);
		verify(tagDAO, never()).assignTagToNews(newsId, null);
	}

	/**
	 * Tests the proper functioning of updateNewsMessage method of NewsService
	 * class in a trouble-free case.
	 */
	@Test
	public void shouldUpdateNewsTest() throws Exception {
		boolean isUpdated = false;
		Long newsId = 11L;
		News news = new News();
		/* stubbing */
		when(newsDAO.updateEntity(news, newsId)).thenReturn(true);
		/* setting expectations and verifying */
		isUpdated = newsService.updateNewsMessage(news, newsId);
		Assert.assertTrue(isUpdated);
		verify(newsDAO, times(1)).updateEntity(news, newsId);
	}

	/**
	 * Tests the proper functioning of updateNewsMessage method of NewsService
	 * class in the case when DAOException is thrown.
	 */
	@Test
	public void shouldNotUpdateNewsTest() throws DAOException {
		boolean isUpdated = true;
		Long newsId = 11L;
		News news = new News();
		/* stubbing */
		when(newsDAO.updateEntity(news, newsId)).thenThrow(
				new DAOException("Database error"));
		/* setting expectations and verifying */
		try {
			isUpdated = newsService.updateNewsMessage(news, newsId);
		} catch (DAOException ex) {
			isUpdated = false;
			Assert.assertEquals("Database error", ex.getMessage());
		}
		Assert.assertFalse(isUpdated);
		verify(newsDAO, times(1)).updateEntity(news, newsId);
	}

	/**
	 * Tests the proper functioning of deleteNewsMessage method of NewsService
	 * class in a trouble-free case.
	 */
	@Test
	public void shouldDeleteNewsTest() throws Exception {
		boolean isDeleted = false;
		Long newsId = 19L;
		/* stubbing */
		when(commentDAO.deassignCommentsFromNews(newsId)).thenReturn(true);
		when(authorDAO.deassignAuthorFromNews(newsId)).thenReturn(true);
		when(tagDAO.deassignTagsFromNews(newsId)).thenReturn(true);
		when(newsDAO.deleteEntity(newsId)).thenReturn(true);
		/* setting expectations and verifying */
		isDeleted = newsService.deleteNewsMessage(newsId);
		Assert.assertTrue(isDeleted);
		verify(commentDAO, times(1)).deassignCommentsFromNews(newsId);
		verify(authorDAO, times(1)).deassignAuthorFromNews(newsId);
		verify(tagDAO, times(1)).deassignTagsFromNews(newsId);
		verify(newsDAO, times(1)).deleteEntity(newsId);
	}

	/**
	 * Tests the proper functioning of deleteNewsMessage method of NewsService
	 * class in the case when DAOException is thrown.
	 */
	@Test
	public void shouldNotDeleteNewsTest() throws DAOException {
		boolean isDeleted = true;
		Long newsId = 19L;
		/* stubbing */
		when(tagDAO.deassignTagsFromNews(newsId)).thenThrow(
				new DAOException("Database error"));
		/* setting expectations and verifying */
		try {
			isDeleted = newsService.deleteNewsMessage(newsId);
		} catch (DAOException ex) {
			isDeleted = false;
			Assert.assertEquals("Database error", ex.getMessage());
		}
		Assert.assertFalse(isDeleted);
		verify(commentDAO, times(1)).deassignCommentsFromNews(newsId);
		verify(tagDAO, times(1)).deassignTagsFromNews(newsId);
		verify(authorDAO, never()).deassignAuthorFromNews(newsId);
		verify(newsDAO, never()).deleteEntity(newsId);
	}
}
