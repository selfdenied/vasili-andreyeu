package com.epam.newsmanagement.service;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.never;

import java.util.HashSet;
import java.util.Set;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.epam.newsmanagement.bean.Tag;
import com.epam.newsmanagement.dao.oracledao.OracleTagDAO;
import com.epam.newsmanagement.exception.DAOException;

/**
 * Class {@code TagServiceTest} contains a number of methods that test the
 * proper functioning of service methods of TagService class.
 * 
 * @author Vasili Andreev
 * @version 1.0
 * @see com.epam.newsmanagement.service.TagService
 */
public class TagServiceTest {
	@Mock
	private OracleTagDAO tagDAO;
	private TagService tagService;

	/**
	 * Sets up the mock instance and initializes the TagService object.
	 */
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		this.tagService = new TagService();
		tagService.setTagDAO(tagDAO);
	}

	/**
	 * Tests the proper functioning of addNewTag method of TagService class in a
	 * trouble-free case.
	 */
	@Test
	public void shouldAddTagTest() throws Exception {
		Tag tag = new Tag();
		tag.setTagName("Politics");
		/* stubbing */
		when(tagDAO.addNewEntity(tag)).thenReturn(true);
		/* setting expectations and verifying */
		boolean isAdded = tagService.addNewTag(tag);
		Assert.assertTrue(isAdded);
		verify(tagDAO, times(1)).addNewEntity(tag);
	}

	/**
	 * Tests the proper functioning of addNewTag method of TagService class in
	 * the case when DAOException is thrown.
	 */
	@Test
	public void shouldNotAddTagTest() throws DAOException {
		boolean isAdded = true;
		Tag tag = new Tag();
		tag.setTagName("Politics");
		/* stubbing */
		when(tagDAO.addNewEntity(tag)).thenThrow(
				new DAOException("Database error"));
		/* setting expectations and verifying */
		try {
			isAdded = tagService.addNewTag(tag);
		} catch (DAOException ex) {
			isAdded = false;
			Assert.assertEquals("Database error", ex.getMessage());
		}
		Assert.assertFalse(isAdded);
		verify(tagDAO, times(1)).addNewEntity(tag);
	}

	/**
	 * Tests the proper functioning of updateTag method of TagService class in a
	 * trouble-free case.
	 */
	@Test
	public void shouldUpdateTagTest() throws Exception {
		Long tagId = 1L;
		Tag tag = new Tag();
		tag.setTagName("Economy");
		/* stubbing */
		when(tagDAO.updateEntity(tag, tagId)).thenReturn(true);
		/* setting expectations and verifying */
		boolean isUpdated = tagService.updateTag(tag, tagId);
		Assert.assertTrue(isUpdated);
		verify(tagDAO, times(1)).updateEntity(tag, tagId);
	}

	/**
	 * Tests the proper functioning of updateTag method of TagService class in
	 * the case when DAOException is thrown.
	 */
	@Test
	public void shouldNotUpdateTagTest() throws DAOException {
		boolean isUpdated = true;
		Long tagId = 1L;
		Tag tag = new Tag();
		tag.setTagName("Economy");
		/* stubbing */
		when(tagDAO.updateEntity(tag, tagId)).thenThrow(
				new DAOException("Database error"));
		/* setting expectations and verifying */
		try {
			isUpdated = tagService.updateTag(tag, tagId);
		} catch (DAOException ex) {
			isUpdated = false;
			Assert.assertEquals("Database error", ex.getMessage());
		}
		Assert.assertFalse(isUpdated);
		verify(tagDAO, times(1)).updateEntity(tag, tagId);
	}

	/**
	 * Tests the proper functioning of deleteTag method of TagService class in a
	 * trouble-free case.
	 */
	@Test
	public void shouldDeleteTagTest() throws Exception {
		Long tagId = 1L;
		/* stubbing */
		when(tagDAO.deleteEntity(tagId)).thenReturn(true);
		/* setting expectations and verifying */
		boolean isDeleted = tagService.deleteTag(tagId);
		Assert.assertTrue(isDeleted);
		verify(tagDAO, times(1)).deleteEntity(tagId);
	}

	/**
	 * Tests the proper functioning of deleteTag method of TagService class in
	 * the case when DAOException is thrown.
	 */
	@Test
	public void shouldNotDeleteTagTest() throws DAOException {
		boolean isDeleted = true;
		Long tagId = 1L;
		/* stubbing */
		when(tagDAO.deleteEntity(tagId)).thenThrow(
				new DAOException("Database error"));
		/* setting expectations and verifying */
		try {
			isDeleted = tagService.deleteTag(tagId);
		} catch (DAOException ex) {
			isDeleted = false;
			Assert.assertEquals("Database error", ex.getMessage());
		}
		Assert.assertFalse(isDeleted);
		verify(tagDAO, times(1)).deleteEntity(tagId);
	}

	/**
	 * Tests the proper functioning of findAllTags() method of TagService class
	 * in a trouble-free case.
	 */
	@Test
	public void shouldFindTagsTest() throws Exception {
		Set<Tag> tagsList = new HashSet<>();
		Tag tag = new Tag();
		tagsList.add(tag);
		/* stubbing */
		when(tagDAO.findAll()).thenReturn(tagsList);
		/* setting expectations and verifying */
		Set<Tag> resultList = tagService.findAllTags();
		Assert.assertNotNull(resultList);
		Assert.assertEquals(1, resultList.size());
		verify(tagDAO, times(1)).findAll();

	}

	/**
	 * Tests the proper functioning of findAllTags() method of TagService class
	 * in the case when DAOException is thrown.
	 */
	@Test
	public void shouldNotFindTagsTest() throws DAOException {
		Set<Tag> tagsList = null;
		/* stubbing */
		when(tagDAO.findAll()).thenThrow(new DAOException("Database error"));
		/* setting expectations and verifying */
		try {
			tagsList = tagService.findAllTags();
		} catch (DAOException ex) {
			Assert.assertEquals("Database error", ex.getMessage());
		}
		Assert.assertNull(tagsList);
		verify(tagDAO, times(1)).findAll();
	}
	
	/**
	 * Tests the proper functioning of updateNewsTags method of TagService class
	 * in in a trouble-free case.
	 */
	@Test
	public void shouldUpdateNewsTagsTest() throws Exception {
		boolean isUpdated = false;
		Set<Long> tagsIds = new HashSet<>();
		Long newsId = 1L;
		Long tagId1 = 16L;
		Long tagId2 = 5L;
		tagsIds.add(tagId1);
		tagsIds.add(tagId2);
		/* stubbing */
		when(tagDAO.deassignTagsFromNews(newsId)).thenReturn(true);
		when(tagDAO.assignTagToNews(newsId, tagId1)).thenReturn(true);
		when(tagDAO.assignTagToNews(newsId, tagId2)).thenReturn(true);
		/* setting expectations and verifying */
		isUpdated = tagService.updateNewsTags(newsId, tagsIds);
		Assert.assertTrue(isUpdated);
		verify(tagDAO, times(1)).deassignTagsFromNews(newsId);
		verify(tagDAO, times(1)).assignTagToNews(newsId, tagId1);
		verify(tagDAO, times(1)).assignTagToNews(newsId, tagId2);
	}
	
	/**
	 * Tests the proper functioning of updateNewsTags method of TagService class
	 * in the case when DAOException is thrown.
	 */
	@Test
	public void shouldNotUpdateNewsTagsTest() throws DAOException {
		boolean isUpdated = true;
		Set<Long> tagsIds = null;
		Long newsId = 1L;
		/* stubbing */
		when(tagDAO.deassignTagsFromNews(newsId)).thenThrow(new DAOException("Database error"));
		/* setting expectations and verifying */
		try {
			isUpdated = tagService.updateNewsTags(newsId, tagsIds);
		} catch (DAOException ex) {
			isUpdated = false;
			Assert.assertEquals("Database error", ex.getMessage());
		}
		Assert.assertFalse(isUpdated);
		verify(tagDAO, times(1)).deassignTagsFromNews(newsId);
		verify(tagDAO, never()).assignTagToNews(newsId, null);
	}
}
