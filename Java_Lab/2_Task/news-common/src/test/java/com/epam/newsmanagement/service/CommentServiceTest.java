package com.epam.newsmanagement.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.*;

import com.epam.newsmanagement.bean.Comment;
import com.epam.newsmanagement.dao.oracledao.OracleCommentDAO;
import com.epam.newsmanagement.exception.DAOException;

/**
 * Class {@code CommentServiceTest} contains a number of methods that test the
 * proper functioning of service methods of CommentService class.
 * 
 * @author Vasili Andreev
 * @version 1.0
 * @see com.epam.newsmanagement.service.CommentService
 */
public class CommentServiceTest {
	@Mock
	private OracleCommentDAO commentDAO;
	private CommentService commentService;

	/**
	 * Sets up the mock instance and initializes the CommentService object.
	 */
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		this.commentService = new CommentService();
		commentService.setCommentDAO(commentDAO);
	}

	/**
	 * Tests the proper functioning of addNewComment method of CommentService
	 * class in a trouble-free case.
	 */
	@Test
	public void shouldAddCommentTest() throws Exception {
		Comment comment = new Comment();
		comment.setCommentText("This is a pretty cool comment!");
		/* stubbing */
		when(commentDAO.addNewEntity(comment)).thenReturn(true);
		/* setting expectations and verifying */
		boolean isAdded = commentService.addNewComment(comment);
		Assert.assertTrue(isAdded);
		verify(commentDAO, times(1)).addNewEntity(comment);
	}

	/**
	 * Tests the proper functioning of addNewComment method of CommentService
	 * class in the case when DAOException is thrown.
	 */
	@Test
	public void shouldNotAddCommentTest() throws DAOException {
		boolean isAdded = true;
		Comment comment = new Comment();
		comment.setCommentText("This is a pretty cool comment!");
		/* stubbing */
		when(commentDAO.addNewEntity(comment)).thenThrow(
				new DAOException("Database error"));
		/* setting expectations and verifying */
		try {
			isAdded = commentService.addNewComment(comment);
		} catch (DAOException ex) {
			isAdded = false;
			Assert.assertEquals("Database error", ex.getMessage());
		}
		Assert.assertFalse(isAdded);
		verify(commentDAO, times(1)).addNewEntity(comment);
	}

	/**
	 * Tests the proper functioning of deleteComment method of CommentService
	 * class in a trouble-free case.
	 */
	@Test
	public void shouldDeleteCommentTest() throws Exception {
		Long commentId = 5L;
		/* stubbing */
		when(commentDAO.deleteEntity(commentId)).thenReturn(true);
		/* setting expectations and verifying */
		boolean isDeleted = commentService.deleteComment(commentId);
		Assert.assertTrue(isDeleted);
		verify(commentDAO, times(1)).deleteEntity(commentId);
	}

	/**
	 * Tests the proper functioning of deleteComment method of CommentService
	 * class in the case when DAOException is thrown.
	 */
	@Test
	public void shouldNotDeleteCommentTest() throws DAOException {
		boolean isDeleted = true;
		Long commentId = 5L;
		/* stubbing */
		when(commentDAO.deleteEntity(commentId)).thenThrow(
				new DAOException("Database error"));
		/* setting expectations and verifying */
		try {
			isDeleted = commentService.deleteComment(commentId);
		} catch (DAOException ex) {
			isDeleted = false;
			Assert.assertEquals("Database error", ex.getMessage());
		}
		Assert.assertFalse(isDeleted);
		verify(commentDAO, times(1)).deleteEntity(commentId);
	}
}
