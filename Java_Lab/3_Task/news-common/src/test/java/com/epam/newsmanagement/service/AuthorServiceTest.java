package com.epam.newsmanagement.service;

import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import static org.mockito.Mockito.*;

import org.mockito.MockitoAnnotations;

import com.epam.newsmanagement.bean.Author;
import com.epam.newsmanagement.dao.oracledao.OracleAuthorDAO;
import com.epam.newsmanagement.exception.DAOException;

/**
 * Class {@code AuthorServiceTest} contains a number of methods that test the
 * proper functioning of service methods of AuthorService class.
 * 
 * @author Vasili Andreev
 * @version 1.0
 * @see com.epam.newsmanagement.service.AuthorService
 */
public class AuthorServiceTest {
	@Mock
	private OracleAuthorDAO authorDAO;
	private AuthorService authorService;

	/**
	 * Sets up the mock instance and initializes the AuthorService object.
	 */
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		this.authorService = new AuthorService();
		authorService.setAuthorDAO(authorDAO);
	}

	/**
	 * Tests the proper functioning of deleteAuthor method of AuthorService
	 * class in a trouble-free case.
	 */
	@Test
	public void shouldDeleteAuthorTest() throws Exception {
		Long authorId = 19L;
		/* stubbing */
		when(authorDAO.deleteEntity(authorId)).thenReturn(true);
		/* setting expectations and verifying */
		boolean isDeleted = authorService.deleteAuthor(authorId);
		Assert.assertTrue(isDeleted);
		verify(authorDAO, times(1)).deleteEntity(authorId);
	}

	/**
	 * Tests the proper functioning of deleteAuthor method of AuthorService
	 * class in the case when DAOException is thrown.
	 */
	@Test
	public void shouldNotDeleteAuthorTest() throws DAOException {
		boolean isDeleted = true;
		Long authorId = 5L;
		/* stubbing */
		when(authorDAO.deleteEntity(authorId)).thenThrow(
				new DAOException("Database error"));
		/* setting expectations and verifying */
		try {
			isDeleted = authorService.deleteAuthor(authorId);
		} catch (DAOException ex) {
			isDeleted = false;
			Assert.assertEquals("Database error", ex.getMessage());
		}
		Assert.assertFalse(isDeleted);
		verify(authorDAO, times(1)).deleteEntity(authorId);
	}

	/**
	 * Tests the proper functioning of addNewAuthor method of AuthorService
	 * class in a trouble-free case.
	 */
	@Test
	public void shouldAddNewAuthorTest() throws Exception {
		Author author = new Author();
		author.setAuthorName("Anton Gribov");
		/* stubbing */
		when(authorDAO.addNewEntity(author)).thenReturn(true);
		/* setting expectations and verifying */
		boolean isAdded = authorService.addNewAuthor(author);
		Assert.assertTrue(isAdded);
		verify(authorDAO, times(1)).addNewEntity(author);
	}

	/**
	 * Tests the proper functioning of addNewAuthor method of AuthorService
	 * class in the case when DAOException is thrown.
	 */
	@Test
	public void shouldNotAddNewAuthorTest() throws DAOException {
		boolean isAdded = true;
		Author author = new Author();
		author.setAuthorName("Artem Zatsepin");
		/* stubbing */
		when(authorDAO.addNewEntity(author)).thenThrow(
				new DAOException("Database error"));
		/* setting expectations and verifying */
		try {
			isAdded = authorService.addNewAuthor(author);
		} catch (DAOException ex) {
			isAdded = false;
			Assert.assertEquals("Database error", ex.getMessage());
		}
		Assert.assertFalse(isAdded);
		verify(authorDAO, times(1)).addNewEntity(author);
	}

	/**
	 * Tests the proper functioning of updateAuthor method of AuthorService
	 * class in a trouble-free case.
	 */
	@Test
	public void shouldUpdateAuthorTest() throws Exception {
		Long authorId = 11L;
		Author author = new Author();
		author.setAuthorName("David Burry");
		/* stubbing */
		when(authorDAO.updateEntity(author, authorId)).thenReturn(true);
		/* setting expectations and verifying */
		boolean isUpdated = authorService.updateAuthor(author, authorId);
		Assert.assertTrue(isUpdated);
		verify(authorDAO, times(1)).updateEntity(author, authorId);
	}

	/**
	 * Tests the proper functioning of updateAuthor method of AuthorService
	 * class in the case when DAOException is thrown.
	 */
	@Test
	public void shouldNotUpdateAuthorTest() throws DAOException {
		Long authorId = 11L;
		boolean isUpdated = true;
		Author author = new Author();
		author.setAuthorName("David Burry");
		/* stubbing */
		when(authorDAO.updateEntity(author, authorId)).thenThrow(
				new DAOException("Database error"));
		/* setting expectations and verifying */
		try {
			isUpdated = authorService.updateAuthor(author, authorId);
		} catch (DAOException ex) {
			isUpdated = false;
			Assert.assertEquals("Database error", ex.getMessage());
		}
		Assert.assertFalse(isUpdated);
		verify(authorDAO, times(1)).updateEntity(author, authorId);
	}

	/**
	 * Tests the proper functioning of findAllActiveAuthors() method of
	 * AuthorService class in a trouble-free case.
	 */
	@Test
	public void shouldFindActiveAuthorsTest() throws Exception {
		Set<Author> authorsList = new HashSet<>();
		Author author1 = new Author();
		Author author2 = new Author();
		authorsList.add(author1);
		/* this one won't be included into result list by service method */
		author2.setExpired(new Timestamp(0L));
		authorsList.add(author2);
		/* stubbing */
		when(authorDAO.findAll()).thenReturn(authorsList);
		/* setting expectations and verifying */
		Set<Author> resultList = authorService.findAllActiveAuthors();
		Assert.assertEquals(1, resultList.size()); // size should be 1 (not 2)
		verify(authorDAO, times(1)).findAll();
	}

	/**
	 * Tests the proper functioning of findAllActiveAuthors() method of
	 * AuthorService class in the case when DAOException is thrown.
	 */
	@Test
	public void shouldNotFindActiveAuthorsTest() throws DAOException {
		Set<Author> authorsList = null;
		/* stubbing */
		when(authorDAO.findAll()).thenThrow(new DAOException("Database error"));
		/* setting expectations and verifying */
		try {
			authorsList = authorService.findAllActiveAuthors();
		} catch (DAOException ex) {
			Assert.assertEquals("Database error", ex.getMessage());
		}
		Assert.assertNull(authorsList);
		verify(authorDAO, times(1)).findAll();
	}
	
	/**
	 * Tests the proper functioning of findAllAuthors() method of
	 * AuthorService class in a trouble-free case.
	 */
	@Test
	public void shouldFindAuthorsTest() throws Exception {
		Set<Author> authorsList = new HashSet<>();
		Author author1 = new Author();
		Author author2 = new Author();
		authorsList.add(author1);
		/* this one still will be included in the result list */
		author2.setExpired(new Timestamp(0L));
		authorsList.add(author2);
		/* stubbing */
		when(authorDAO.findAll()).thenReturn(authorsList);
		/* setting expectations and verifying */
		Set<Author> resultList = authorService.findAllAuthors();
		Assert.assertEquals(2, resultList.size()); // size should 3
		verify(authorDAO, times(1)).findAll();
	}

	/**
	 * Tests the proper functioning of findAllAuthors() method of
	 * AuthorService class in the case when DAOException is thrown.
	 */
	@Test
	public void shouldNotFindAuthorsTest() throws DAOException {
		Set<Author> authorsList = null;
		/* stubbing */
		when(authorDAO.findAll()).thenThrow(new DAOException("Database error"));
		/* setting expectations and verifying */
		try {
			authorsList = authorService.findAllAuthors();
		} catch (DAOException ex) {
			Assert.assertEquals("Database error", ex.getMessage());
		}
		Assert.assertNull(authorsList);
		verify(authorDAO, times(1)).findAll();
	}
}
