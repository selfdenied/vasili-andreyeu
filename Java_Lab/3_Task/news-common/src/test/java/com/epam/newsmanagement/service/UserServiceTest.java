package com.epam.newsmanagement.service;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.HashSet;
import java.util.Set;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.epam.newsmanagement.bean.User;
import com.epam.newsmanagement.dao.oracledao.OracleUserDAO;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.util.HashPassword;

/**
 * Class {@code UserServiceTest} contains a number of methods that test the
 * proper functioning of service methods of UserService class.
 * 
 * @author Vasili Andreev
 * @version 1.0
 * @see com.epam.newsmanagement.service.UserService
 */
public class UserServiceTest {
	@Mock
	private OracleUserDAO userDAO;
	private UserService userService;

	/**
	 * Sets up the mock instance and initializes the UserService object.
	 */
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		this.userService = new UserService();
		userService.setUserDAO(userDAO);
	}

	/**
	 * Tests the proper functioning of checkUserData method of UserService class
	 * in a case when User's data is correct.
	 */
	@Test
	public void checkCorrectDataTest() throws Exception {
		String login = "self";
		String password = "pass";
		User user1 = new User();
		user1.setLogin(login);
		user1.setPassword(HashPassword.createHash(password));
		Set<User> usersList = new HashSet<>();
		usersList.add(user1);

		/* stubbing */
		when(userDAO.findAll()).thenReturn(usersList);
		/* setting expectations and verifying */
		boolean isDataCorrect = userService.checkUserData("self", "pass");
		Assert.assertTrue(isDataCorrect);
		verify(userDAO, times(1)).findAll();
	}

	/**
	 * Tests the proper functioning of checkUserData method of UserService class
	 * in a case when User's data is incorrect.
	 */
	@Test
	public void checkIncorrectDataTest() throws Exception {
		String login = "self";
		String password = "pass";
		User user1 = new User();
		user1.setLogin(login);
		user1.setPassword(HashPassword.createHash(password));
		Set<User> usersList = new HashSet<>();
		usersList.add(user1);

		/* stubbing */
		when(userDAO.findAll()).thenReturn(usersList);
		/* setting expectations and verifying */
		boolean isDataCorrect = userService.checkUserData("self", "fdrt444");
		Assert.assertFalse(isDataCorrect);
		isDataCorrect = userService.checkUserData("admin", "pass");
		Assert.assertFalse(isDataCorrect);
		verify(userDAO, times(2)).findAll();
	}

	/**
	 * Tests the proper functioning of checkUserData method of UserService class
	 * in the case when DAOException is thrown.
	 */
	@Test
	public void shouldNotCheckDataTest() throws Exception {
		boolean isChecked = true;

		/* stubbing */
		when(userDAO.findAll()).thenThrow(new DAOException("Database error"));
		/* setting expectations and verifying */
		try {
			isChecked = userService.checkUserData("login", "pass");
		} catch (DAOException ex) {
			isChecked = false;
			Assert.assertEquals("Database error", ex.getMessage());
		}
		Assert.assertFalse(isChecked);
		verify(userDAO, times(1)).findAll();
	}

	/**
	 * Tests the proper functioning of obtainUserRole method of UserService
	 * class in a trouble-free case.
	 */
	@Test
	public void shouldObtainRoleTest() throws Exception {
		String login = "selfdenied";
		String role = "Admin";
		User user1 = new User();
		user1.setLogin(login);
		user1.setRoleName(role);
		Set<User> usersList = new HashSet<>();
		usersList.add(user1);

		/* stubbing */
		when(userDAO.findAll()).thenReturn(usersList);
		/* setting expectations and verifying */
		String userRole = userService.obtainUserRole(login);
		Assert.assertNotNull(userRole);
		Assert.assertEquals("Admin", userRole);
		verify(userDAO, times(1)).findAll();
	}

	/**
	 * Tests the proper functioning of obtainUserRole method of UserService
	 * class in the case when DAOException is thrown.
	 */
	@Test
	public void shouldNotObtainRoleTest() throws DAOException {
		String role = null;
		String login = "self";

		/* stubbing */
		when(userDAO.findAll()).thenThrow(new DAOException("Database error"));
		/* setting expectations and verifying */
		try {
			role = userService.obtainUserRole(login);
		} catch (DAOException ex) {
			Assert.assertEquals("Database error", ex.getMessage());
		}
		Assert.assertNull(role);
		verify(userDAO, times(1)).findAll();
	}
	
	/**
	 * Tests the proper functioning of obtainUserByLogin method of UserService
	 * class in a trouble-free case.
	 */
	@Test
	public void shouldObtainUserTest() throws Exception {
		String login = "adam13";
		String name = "Adam Lalana";
		User user1 = new User();
		user1.setLogin(login);
		user1.setUserName(name);
		Set<User> usersList = new HashSet<>();
		usersList.add(user1);

		/* stubbing */
		when(userDAO.findAll()).thenReturn(usersList);
		/* setting expectations and verifying */
		String userName = userService.obtainUserByLogin(login).getUserName();
		Assert.assertNotNull(userName);
		Assert.assertEquals("Adam Lalana", userName);
		verify(userDAO, times(1)).findAll();
	}

	/**
	 * Tests the proper functioning of obtainUserByLogin method of UserService
	 * class in the case when DAOException is thrown.
	 */
	@Test
	public void shouldNotObtainUserTest() throws DAOException {
		String name = null;
		String login = "user111";

		/* stubbing */
		when(userDAO.findAll()).thenThrow(new DAOException("Database error"));
		/* setting expectations and verifying */
		try {
			name = userService.obtainUserByLogin(login).getUserName();
		} catch (DAOException ex) {
			Assert.assertEquals("Database error", ex.getMessage());
		}
		Assert.assertNull(name);
		verify(userDAO, times(1)).findAll();
	}
}
