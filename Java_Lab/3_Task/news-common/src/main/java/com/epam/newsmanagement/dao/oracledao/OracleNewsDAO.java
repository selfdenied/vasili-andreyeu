package com.epam.newsmanagement.dao.oracledao;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.TimeZone;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.epam.newsmanagement.bean.News;
import com.epam.newsmanagement.dao.AbstractDAO;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.search.news.NewsSearchCriteria;
import com.epam.newsmanagement.search.news.NewsSearchCriteria.AuthorSearchType;

import static com.epam.newsmanagement.search.news.NewsSearchCriteria.AuthorSearchType.*;

/**
 * Class {@code OracleNewsDAO} contains methods allowing to extract information
 * about News found in the application, add, update, and delete their data.
 * 
 * @author Vasili Andreev
 * @version 1.0
 * @see com.epam.newsmanagement.bean.News
 * @see com.epam.newsmanagement.dao.AbstractDAO
 */
@Component
@Scope("prototype")
public class OracleNewsDAO extends AbstractDAO<News> {
	private static final String NEWS_TITLE = "title";

	/* finds all News in the application */
	@Override
	@SuppressWarnings("unchecked")
	public Set<News> findAll() throws DAOException {
		Set<News> newsList = new HashSet<>();

		try {
			Session session = getSession();
			Criteria cr = session.createCriteria(News.class);
			newsList.addAll(cr.list());
		} catch (HibernateException ex) {
			throw new DAOException("Database error", ex);
		} finally {
			closeSession();
		}
		return newsList;
	}

	/* returns News message with the given ID */
	@Override
	public News findEntityById(Long newsId) throws DAOException {
		News news = null;

		try {
			Session session = getSession();
			news = session.get(News.class, newsId);
		} catch (HibernateException ex) {
			throw new DAOException("Database error", ex);
		} finally {
			closeSession();
		}
		return news;
	}

	/**
	 * Finds the ID of the News message by its unique title.
	 * 
	 * @param title
	 *            The title of the News message
	 * @return The ID of the News message
	 * @throws DAOException
	 *             If a database access/handling error occurs.
	 */
	public Long findNewsIdByTitle(String title) throws DAOException {
		Long newsID = null;
		News news = null;

		try {
			Session session = getSession();
			Criteria cr = session.createCriteria(News.class);
			cr.add(Restrictions.eq(NEWS_TITLE, title));
			if (!cr.list().isEmpty()) {
				news = (News) cr.list().get(0);
				newsID = news.getNewsId();
			}
		} catch (HibernateException ex) {
			throw new DAOException("Database error", ex);
		} finally {
			closeSession();
		}
		return newsID;
	}

	/* adds News message to the database */
	@Override
	public boolean addNewEntity(News news) throws DAOException {
		Transaction tx = null;
		boolean isAdded = false;

		try {
			Session session = getSession();
			tx = session.beginTransaction();
			news.setCreationDate(obtainTimestamp());
			session.save(news);
			tx.commit();
			isAdded = true;
		} catch (HibernateException ex) {
			if (tx != null) {
				tx.rollback();
			}
			throw new DAOException("Database error", ex);
		} finally {
			closeSession();
		}
		return isAdded;
	}

	/* updates News message data */
	@Override
	public boolean updateEntity(News news, Long newsId) throws DAOException {
		Transaction tx = null;
		boolean isUpdated = false;

		try {
			Session session = getSession();
			tx = session.beginTransaction();
			News oldNews = session.get(News.class, newsId);
			oldNews.setTitle(news.getTitle());
			oldNews.setShortText(news.getShortText());
			oldNews.setFullText(news.getFullText());
			session.update(oldNews);
			tx.commit();
			isUpdated = true;
		} catch (HibernateException ex) {
			if (tx != null) {
				tx.rollback();
			}
			throw new DAOException("Database error", ex);
		} finally {
			closeSession();
		}
		return isUpdated;
	}

	/* deletes news message and dependencies from the database */
	@Override
	public boolean deleteEntity(Long newsId) throws DAOException {
		Transaction tx = null;
		boolean isDeleted = false;

		try {
			Session session = getSession();
			tx = session.beginTransaction();
			News news = session.get(News.class, newsId);
			session.delete(news);
			tx.commit();
			isDeleted = true;
		} catch (HibernateException ex) {
			if (tx != null) {
				tx.rollback();
			}
			throw new DAOException("Database error", ex);
		} finally {
			closeSession();
		}
		return isDeleted;
	}

	/**
	 * Finds News messages according to a given search criteria.
	 * 
	 * @param searchCriteria
	 *            the Search criteria
	 * @return The list of news messages
	 * @throws DAOException
	 *             If a database access/handling error occurs.
	 */
	@SuppressWarnings("unchecked")
	public List<News> searchNewsByAuthorAndTags(
			NewsSearchCriteria searchCriteria) throws DAOException {
		List<News> tempList;
		List<News> newsList = new ArrayList<>();
		/* extracting required data from searchCriteria object */
		Set<String> tagsList = searchCriteria.getTagsNames();
		String authorName = searchCriteria.getAuthorName();
		Long authorID = searchCriteria.getAuthorId();
		AuthorSearchType searchType = searchCriteria.getAuthorSearch();

		try {
			/* starting Hibernate Session */
			Session session = getSession();
			/* initializing Criteria */
			Criteria criteria = initCriteria(session);
			/* we can choose between 'OR' & 'AND' search conditions */
			Disjunction or = Restrictions.disjunction();
			/* adding Author name/ID search criteria */
			addAuthorSearch(searchType, authorName, authorID, or);
			/* adding Tag search criteria */
			addTagSearch(tagsList, or);
			criteria.add(or);
			/* getting results */
			tempList = criteria.list();
			session.clear();
			for (News news : tempList) {
				newsList.add(session.get(News.class, news.getNewsId()));
			}
		} catch (HibernateException ex) {
			throw new DAOException("Database error", ex);
		} finally {
			closeSession();
		}
		return newsList;
	}

	/* initializes Hibernate Criteria instance */
	private static Criteria initCriteria(Session session) {
		Criteria criteria = session.createCriteria(News.class, "news");
		criteria.createAlias("news.author", "author");
		criteria.createAlias("news.listOfTags", "tags", JoinType.LEFT_OUTER_JOIN);
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		return criteria;
	}

	/* adds Author name/ID search criterion */
	private void addAuthorSearch(AuthorSearchType searchType,
			String authorName, Long authorId, Disjunction or) {
		if (searchType == BY_NAME && authorName != null) {
			or.add(Restrictions.eq("author.authorName", authorName));
		} else if (searchType == BY_ID && authorId != null) {
			or.add(Restrictions.eq("author.authorId", authorId));
		}
	}

	/* adds Tags search criteria */
	private void addTagSearch(Set<String> tagsList, Disjunction or) {
		if (tagsList != null && !tagsList.isEmpty()) {
			for (String tag : tagsList) {
				or.add(Restrictions.eq("tags.tagName", tag));
			}
		}
	}

	/* Gets the current Timestamp */
	private Timestamp obtainTimestamp() {
		TimeZone zone = TimeZone.getTimeZone("GMT+3");
		Calendar calendar = Calendar.getInstance(zone, Locale.getDefault());
		Date date = calendar.getTime();
		return new Timestamp(date.getTime());
	}
}
