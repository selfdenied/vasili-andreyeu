package com.epam.newsmanagement.bean;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Set;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Class {@code Author} is a Java Bean that stores the data of news authors (ID,
 * Name and Expired status).
 * 
 * @author Vasili Andreev
 * @version 1.0
 */
@Component
@Scope("prototype")
public class Author implements Serializable {
	private static final long serialVersionUID = -291541131304068416L;
	private Long authorId;
	private String authorName;
	private Timestamp expired;
	private Set<News> listOfNews;

	/**
	 * Returns the ID of the author.
	 * 
	 * @return author's ID
	 */
	public Long getAuthorId() {
		return authorId;
	}

	/**
	 * Sets the ID of the author.
	 * 
	 * @param authorId
	 *            the author's ID
	 */
	public void setAuthorId(Long authorId) {
		this.authorId = authorId;
	}

	/**
	 * Returns the name of the author.
	 * 
	 * @return author's name
	 */
	public String getAuthorName() {
		return authorName;
	}

	/**
	 * Sets the name of the author.
	 * 
	 * @param authorName
	 *            the author's name
	 */
	public void setAuthorName(String authorName) {
		this.authorName = authorName;
	}

	/**
	 * Returns the status of author (is Expired or not).
	 * 
	 * @return expired status
	 */
	public Timestamp getExpired() {
		return expired;
	}

	/**
	 * Sets the Expired status of the author.
	 * 
	 * @param expired
	 *            the author's Expired status
	 */
	public void setExpired(Timestamp expired) {
		this.expired = expired;
	}

	/**
	 * Returns the list of news messages this author has written.
	 * 
	 * @return the list of news messages
	 */
	public Set<News> getListOfNews() {
		return listOfNews;
	}

	/**
	 * Sets the the list of news messages this author has written.
	 * 
	 * @param listOfNews
	 *            the list of news messages
	 */
	public void setListOfNews(Set<News> listOfNews) {
		this.listOfNews = listOfNews;
	}

	@Override
	public boolean equals(Object object) {
		if (object == this) {
			return true;
		}
		if (object == null || object.getClass() != this.getClass()) {
			return false;
		}
		Author otherAuthor = (Author) object;
		if (authorId == null) {
			if (otherAuthor.getAuthorId() != null) {
				return false;
			}
		} else if (authorId != otherAuthor.getAuthorId()) {
			return false;
		}
		if (authorName == null) {
			if (otherAuthor.getAuthorName() != null) {
				return false;
			}
		} else if (!authorName.equals(otherAuthor.getAuthorName())) {
			return false;
		}
		if (expired == null) {
			if (otherAuthor.getExpired() != null) {
				return false;
			}
		} else if (!expired.equals(otherAuthor.getExpired())) {
			return false;
		}
		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * ((authorId == null) ? 0 : authorId.intValue())
				+ ((authorName == null) ? 0 : authorName.hashCode())
				+ ((expired == null) ? 0 : expired.hashCode());
		return result;
	}
}
