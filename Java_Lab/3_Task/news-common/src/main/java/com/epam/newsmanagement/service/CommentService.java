package com.epam.newsmanagement.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.epam.newsmanagement.bean.Comment;
import com.epam.newsmanagement.dao.oracledao.OracleCommentDAO;
import com.epam.newsmanagement.exception.DAOException;

/**
 * Class {@code CommentService} contains methods that use DAO layer to
 * add/delete Comments to/from a database.
 * 
 * @author Vasili Andreev
 * @version 1.0
 * @see com.epam.newsmanagement.dao.oracledao.OracleCommentDAO
 */
@Component
@Scope("prototype")
public class CommentService {
	@Autowired
	private OracleCommentDAO commentDAO;

	/**
	 * Sets the CommentDAO dependency.
	 * 
	 * @param commentDAO
	 *            the instance of CommentDAO
	 */
	public void setCommentDAO(OracleCommentDAO commentDAO) {
		this.commentDAO = commentDAO;
	}
	
	/**
	 * Method adds new Comment to the database.
	 * 
	 * @param comment
	 *            com.epam.newsmanagement.bean.Comment
	 * @return {@code true} when Comment has been successfully added and
	 *         {@code false} otherwise
	 * @throws DAOException
	 *             If a DAO exception of some sort has occurred
	 */
	public boolean addNewComment(Comment comment) throws DAOException {
		boolean isAdded = false;

			isAdded = commentDAO.addNewEntity(comment);
		return isAdded;
	}

	/**
	 * Method deletes the Comment with the given ID from the database.
	 * 
	 * @param id
	 *            the ID of the Comment to be deleted
	 * @return {@code true} when Comment has been successfully deleted and
	 *         {@code false} otherwise
	 * @throws DAOException
	 *             If a DAO exception of some sort has occurred
	 */
	public boolean deleteComment(long id) throws DAOException {
		boolean isDeleted = false;

		isDeleted = commentDAO.deleteEntity(id);
		return isDeleted;
	}
}
