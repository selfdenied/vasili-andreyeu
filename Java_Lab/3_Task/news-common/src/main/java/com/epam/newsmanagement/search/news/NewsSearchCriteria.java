package com.epam.newsmanagement.search.news;

import java.util.Set;

import com.epam.newsmanagement.search.SearchCriteria;

/**
 * Class {@code NewsSearchCriteria} contains a number of fields used as search
 * criteria to perform the news search procedure (author name/ID, tag names,
 * etc.).
 * 
 * @author Vasili Andreev
 * @version 1.0
 * @see com.epam.newsmanagement.search.SearchCriteria
 */
public class NewsSearchCriteria extends SearchCriteria {
	private AuthorSearchType authorSearch = AuthorSearchType.BY_NAME;
	private Long authorId;
	private String authorName;
	private Set<String> tagsNames;

	/**
	 * Enum {@code AuthorSearchType} contains the list of constants, according
	 * to which the type of search either by author's name or by author's ID is
	 * chosen.
	 * 
	 * @author Vasili Andreev
	 * @version 1.0
	 */
	public enum AuthorSearchType {
		BY_ID, BY_NAME; // can be updated with other variants
	}

	/**
	 * Returns the type of "author" search.
	 * 
	 * @return the type of "author" search
	 */
	public AuthorSearchType getAuthorSearch() {
		return authorSearch;
	}

	/**
	 * Sets the type of "author" search.
	 * 
	 * @param authorSearch
	 *            the type of "author" search
	 */
	public void setAuthorSearch(AuthorSearchType authorSearch) {
		this.authorSearch = authorSearch;
	}

	/**
	 * Returns the ID of news author.
	 * 
	 * @return the ID of news author
	 */
	public Long getAuthorId() {
		return authorId;
	}

	/**
	 * Sets the ID of news author.
	 * 
	 * @param authorId
	 *            the ID of news author
	 */
	public void setAuthorId(Long authorId) {
		this.authorId = authorId;
	}

	/**
	 * Returns the author's name.
	 * 
	 * @return the name of news author
	 */
	public String getAuthorName() {
		return authorName;
	}

	/**
	 * Sets the author's name.
	 * 
	 * @param authorName
	 *            the name of news author
	 */
	public void setAuthorName(String authorName) {
		this.authorName = authorName;
	}

	/**
	 * Returns the Set of news tags names.
	 * 
	 * @return the Set of news tags names
	 */
	public Set<String> getTagsNames() {
		return tagsNames;
	}

	/**
	 * Sets the list of news tags names.
	 * 
	 * @param tagsNames
	 *            the set of news tags names
	 */
	public void setTagsNames(Set<String> tagsNames) {
		this.tagsNames = tagsNames;
	}
}
