package com.epam.newsmanagement.dao.oracledao;

import java.util.HashSet;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.epam.newsmanagement.bean.News;
import com.epam.newsmanagement.bean.Tag;
import com.epam.newsmanagement.dao.AbstractDAO;
import com.epam.newsmanagement.exception.DAOException;

/**
 * Class {@code OracleTagDAO} contains methods allowing to extract information
 * about Tags found in the application, add, update, and delete their data.
 * 
 * @author Vasili Andreev
 * @version 1.0
 * @see com.epam.newsmanagement.bean.Tag
 * @see com.epam.newsmanagement.dao.AbstractDAO
 */
@Component
@Scope("prototype")
public class OracleTagDAO extends AbstractDAO<Tag> {

	/* finds all Tags in the application */
	@Override
	@SuppressWarnings("unchecked")
	public Set<Tag> findAll() throws DAOException {
		Set<Tag> tagsList = new HashSet<Tag>();

		try {
			Session session = getSession();
			Criteria cr = session.createCriteria(Tag.class);
			tagsList.addAll(cr.list());
		} catch (HibernateException ex) {
			throw new DAOException("Database error", ex);
		} finally {
			closeSession();
		}
		return tagsList;
	}

	/**
	 * Finds all Tags associated with the news message having the given ID.
	 * 
	 * @param newsId
	 *            The ID of the news message
	 * @return The list of Tags associated with the news message
	 * @throws DAOException
	 *             If a database access/handling error occurs.
	 */
	public Set<Tag> findAllTagsByNewsId(Long newsId) throws DAOException {
		Set<Tag> tagsList = new HashSet<Tag>();

		try {
			Session session = getSession();
			News news = session.get(News.class, newsId);
			if (news!= null && news.getListOfTags() != null) {
				tagsList.addAll(news.getListOfTags());
			}
		} catch (HibernateException ex) {
			throw new DAOException("Database error", ex);
		} finally {
			closeSession();
		}
		return tagsList;
	}

	/* returns Tag with the given ID */
	@Override
	public Tag findEntityById(Long tagId) throws DAOException {
		Tag tag = null;
		
		try {
			Session session = getSession();
			tag = session.get(Tag.class, tagId);
		} catch (HibernateException ex) {
			throw new DAOException("Database error", ex);
		} finally {
			closeSession();
		}
		return tag;
	}

	/* adds new Tag to the database */
	@Override
	public boolean addNewEntity(Tag tag) throws DAOException {
		Transaction tx = null;
		boolean isAdded = false;

		try {
			Session session = getSession();
			tx = session.beginTransaction();
			session.save(tag);
			tx.commit();
			isAdded = true;
		} catch (HibernateException ex) {
			if (tx != null) {
				tx.rollback();
			}
			throw new DAOException("Database error", ex);
		} finally {
			closeSession();
		}
		return isAdded;
	}

	/* updates Tag's data */
	@Override
	public boolean updateEntity(Tag tag, Long tagId) throws DAOException {
		Transaction tx = null;
		boolean isUpdated = false;

		try {
			Session session = getSession();
			tx = session.beginTransaction();
			Tag oldTag = session.get(Tag.class, tagId);
			oldTag.setTagName(tag.getTagName());
			session.update(oldTag);
			tx.commit();
			isUpdated = true;
		} catch (HibernateException ex) {
			if (tx != null) {
				tx.rollback();
			}
			throw new DAOException("Database error", ex);
		} finally {
			closeSession();
		}
		return isUpdated;
	}

	/* deletes a given Tag from the database */
	@Override
	public boolean deleteEntity(Long tagId) throws DAOException {
		Transaction tx = null;
		boolean isDeleted = false;

		try {
			Session session = getSession();
			tx = session.beginTransaction();
			Tag tag = session.get(Tag.class, tagId);
			session.delete(tag);
			tx.commit();
			isDeleted = true;
		} catch (HibernateException ex) {
			if (tx != null) {
				tx.rollback();
			}
			throw new DAOException("Database error", ex);
		} finally {
			closeSession();
		}
		return isDeleted;
	}

	/**
	 * Assigns a Tag with the given ID to the news message with the given ID.
	 * 
	 * @param newsId
	 *            The ID of the News message
	 * @param tagId
	 *            The ID of the Tag
	 * @return {@code true} if the tag has been successfully assigned and
	 *         {@code false} otherwise
	 * @throws DAOException
	 *             If a database access/handling error occurs.
	 */
	public boolean assignTagToNews(Long newsId, Long tagId) throws DAOException {
		Transaction tx = null;
		boolean isAssigned = false;

		try {
			Session session = getSession();
			tx = session.beginTransaction();
			News news = session.get(News.class, newsId);
			Tag tag = session.get(Tag.class, tagId);
			tag.getListOfNews().add(news);
			session.update(tag);
			tx.commit();
			isAssigned = true;
		} catch (HibernateException ex) {
			if (tx != null) {
				tx.rollback();
			}
			throw new DAOException("Database error", ex);
		} finally {
			closeSession();
		}
		return isAssigned;
	}

	/**
	 * Deassigns all Tags from the given news message.
	 * 
	 * @param newsId
	 *            The ID of the News message
	 * @return {@code true} if tags have been successfully deassigned and
	 *         {@code false} otherwise
	 * @throws DAOException
	 *             If a database access/handling error occurs.
	 */
	public boolean deassignTagsFromNews(Long newsId) throws DAOException {
		Transaction tx = null;
		boolean isDeassigned = false;

		try {
			Session session = getSession();
			tx = session.beginTransaction();
			News news = session.get(News.class, newsId);
			for (Tag tag : news.getListOfTags()) {
				tag.getListOfNews().remove(news);
				session.update(tag);
			}
			tx.commit();
			isDeassigned = true;
		} catch (HibernateException ex) {
			if (tx != null) {
				tx.rollback();
			}
			throw new DAOException("Database error", ex);
		} finally {
			closeSession();
		}
		return isDeassigned;
	}
}
