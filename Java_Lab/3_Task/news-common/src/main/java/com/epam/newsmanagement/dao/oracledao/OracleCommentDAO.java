package com.epam.newsmanagement.dao.oracledao;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;
import java.util.TimeZone;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.epam.newsmanagement.bean.Comment;
import com.epam.newsmanagement.bean.News;
import com.epam.newsmanagement.dao.AbstractDAO;
import com.epam.newsmanagement.exception.DAOException;

/**
 * Class {@code OracleCommentDAO} contains methods allowing to extract
 * information about Comments found in the application, add, update, and delete
 * their data.
 * 
 * @author Vasili Andreev
 * @version 1.0
 * @see com.epam.newsmanagement.bean.Comment
 * @see com.epam.newsmanagement.dao.AbstractDAO
 */
@Component
@Scope("prototype")
public class OracleCommentDAO extends AbstractDAO<Comment> {

	/* there is no need in such method in the application */
	@Override
	public Set<Comment> findAll() {
		throw new UnsupportedOperationException(
				"Error. This operation is not supported!");
	}

	/**
	 * Finds all Comments associated with the news message having the given ID.
	 * 
	 * @param newsId
	 *            The ID of the news message
	 * @return The list of Comments associated with the news message
	 * @throws DAOException
	 *             If a database access/handling error occurs.
	 */
	public Set<Comment> findCommentsByNewsId(Long newsId) throws DAOException {
		Set<Comment> commentsList = new HashSet<Comment>();

		try {
			Session session = getSession();
			News news = session.get(News.class, newsId);
			if (news != null && news.getListOfComments() != null) {
				commentsList.addAll(news.getListOfComments());				
			}
		} catch (HibernateException ex) {
			throw new DAOException("Database error", ex);
		} finally {
			closeSession();
		}
		return commentsList;
	}

	/* returns Comment with the given ID */
	@Override
	public Comment findEntityById(Long commentId) throws DAOException {
		Comment comment = null;

		try {
			Session session = getSession();
			comment = session.get(Comment.class, commentId);
		} catch (HibernateException ex) {
			throw new DAOException("Database error", ex);
		} finally {
			closeSession();
		}
		return comment;
	}

	/* adds new Comment to the database */
	@Override
	public boolean addNewEntity(Comment comment) throws DAOException {
		Transaction tx = null;
		boolean isAdded = false;

		try {
			Session session = getSession();
			tx = session.beginTransaction();
			comment.setCreationDate(obtainTimestamp());
			session.save(comment);
			tx.commit();
			isAdded = true;
		} catch (HibernateException ex) {
			if (tx != null) {
				tx.rollback();
			}
			throw new DAOException("Database error", ex);
		} finally {
			closeSession();
		}
		return isAdded;
	}

	/* there is no need in such method in the application */
	@Override
	public boolean updateEntity(Comment comment, Long commentId) {
		throw new UnsupportedOperationException(
				"Error. This operation is not supported!");
	}

	/* deletes comment from the database */
	@Override
	public boolean deleteEntity(Long commentId) throws DAOException {
		Transaction tx = null;
		boolean isDeleted = false;

		try {
			Session session = getSession();
			tx = session.beginTransaction();
			Comment comment = session.get(Comment.class, commentId);
			session.delete(comment);
			tx.commit();
			isDeleted = true;
		} catch (HibernateException ex) {
			if (tx != null) {
				tx.rollback();
			}
			throw new DAOException("Database error", ex);
		} finally {
			closeSession();
		}
		return isDeleted;
	}
	
	/**
	 * Deassigns all Comments from the news message with the given ID.
	 * 
	 * @param newsId
	 *            The ID of the News message
	 * @return {@code true} if Comments have been successfully deassigned and
	 *         {@code false} otherwise
	 * @throws DAOException
	 *             If a database access/handling error occurs.
	 */
	public boolean deassignCommentsFromNews(Long newsId) throws DAOException {
		Transaction tx = null;
		boolean isDeAssigned = false;

		try {
			Session session = getSession();
			tx = session.beginTransaction();
			News news = session.get(News.class, newsId);
			for (Comment comment : news.getListOfComments()) {
				session.delete(comment);
			}
			tx.commit();
			isDeAssigned = true;
		} catch (HibernateException ex) {
			if (tx != null) {
				tx.rollback();
			}
			throw new DAOException("Database error", ex);
		} finally {
			closeSession();
		}
		return isDeAssigned;
	}

	/* Gets the current Timestamp */
	private Timestamp obtainTimestamp() {
		TimeZone zone = TimeZone.getTimeZone("GMT+3");
		Calendar calendar = Calendar.getInstance(zone, Locale.getDefault());
		Date date = calendar.getTime();
		return new Timestamp(date.getTime());
	}
}
