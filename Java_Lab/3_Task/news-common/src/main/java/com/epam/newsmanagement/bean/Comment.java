package com.epam.newsmanagement.bean;

import java.io.Serializable;
import java.sql.Timestamp;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Class {@code Comment} is a Java Bean that stores the data of comments (ID,
 * text, newsID and creation date).
 * 
 * @author Vasili Andreev
 * @version 1.0
 */
@Component
@Scope("prototype")
public class Comment implements Serializable {
	private static final long serialVersionUID = -4880860760885238892L;
	private Long commentId;
	private News newsMessage;
	private String commentText;
	private Timestamp creationDate;

	/**
	 * Returns the ID of the comment.
	 * 
	 * @return comment's ID
	 */
	public Long getCommentId() {
		return commentId;
	}

	/**
	 * Sets the ID of the comment.
	 * 
	 * @param commentId
	 *            the comment's ID
	 */
	public void setCommentId(Long commentId) {
		this.commentId = commentId;
	}

	/**
	 * Returns the news message to which the comment is written.
	 * 
	 * @return news message
	 */
	public News getNewsMessage() {
		return newsMessage;
	}

	/**
	 * Sets the news message to which the comment is written.
	 * 
	 * @param newsMessage
	 *            the news message
	 */
	public void setNewsMessage(News newsMessage) {
		this.newsMessage = newsMessage;
	}

	/**
	 * Returns the comment's text.
	 * 
	 * @return comment's text
	 */
	public String getCommentText() {
		return commentText;
	}

	/**
	 * Sets the the comment's text.
	 * 
	 * @param commentText
	 *            the comment's text
	 */
	public void setCommentText(String commentText) {
		this.commentText = commentText;
	}

	/**
	 * Returns the comment's creation date.
	 * 
	 * @return comment's creation date
	 */
	public Timestamp getCreationDate() {
		return creationDate;
	}

	/**
	 * Sets the the comment's creation date.
	 * 
	 * @param creationDate
	 *            the comment's creation date
	 */
	public void setCreationDate(Timestamp creationDate) {
		this.creationDate = creationDate;
	}

	@Override
	public boolean equals(Object object) {
		if (object == this) {
			return true;
		}
		if (object == null || object.getClass() != this.getClass()) {
			return false;
		}
		Comment otherComment = (Comment) object;
		if (commentId == null) {
			if (otherComment.getCommentId() != null) {
				return false;
			}
		} else if (commentId != otherComment.getCommentId()) {
			return false;
		}
		if (newsMessage == null) {
			if (otherComment.getNewsMessage() != null) {
				return false;
			}
		} else if (newsMessage != otherComment.getNewsMessage()) {
			return false;
		}
		if (commentText == null) {
			if (otherComment.getCommentText() != null) {
				return false;
			}
		} else if (!commentText.equals(otherComment.getCommentText())) {
			return false;
		}
		if (creationDate == null) {
			if (otherComment.getCreationDate() != null) {
				return false;
			}
		} else if (!creationDate.equals(otherComment.getCreationDate())) {
			return false;
		}
		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * ((commentId == null) ? 0 : commentId.intValue())
				+ ((commentText == null) ? 0 : commentText.hashCode())
				+ ((newsMessage == null) ? 0 : newsMessage.hashCode())
				+ ((creationDate == null) ? 0 : creationDate.hashCode());
		return result;
	}
}
