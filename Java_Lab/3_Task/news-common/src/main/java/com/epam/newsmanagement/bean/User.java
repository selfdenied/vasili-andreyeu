package com.epam.newsmanagement.bean;

import java.io.Serializable;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Class {@code User} is a Java Bean that stores the data of application users
 * (ID, login and password, name and their roles).
 * 
 * @author Vasili Andreev
 * @version 1.0
 */
@Component
@Scope("prototype")
public class User implements Serializable {
	private static final long serialVersionUID = 2191660602425609124L;
	private Long userId;
	private String userName;
	private String login;
	private String password;
	private String roleName;

	/**
	 * Returns the ID of the user.
	 * 
	 * @return user's ID
	 */
	public Long getUserId() {
		return userId;
	}

	/**
	 * Sets the ID of the user.
	 * 
	 * @param userId
	 *            the user's ID
	 */
	public void setUserId(Long userId) {
		this.userId = userId;
	}

	/**
	 * Returns the name of the user.
	 * 
	 * @return the user's name
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * Sets the name of the user.
	 * 
	 * @param userName
	 *            the user's name
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * Returns the login of the user.
	 * 
	 * @return Login
	 */
	public String getLogin() {
		return login;
	}

	/**
	 * Sets the login of the user.
	 * 
	 * @param login
	 *            the user's login
	 */
	public void setLogin(String login) {
		this.login = login;
	}

	/**
	 * Returns the password of the user.
	 * 
	 * @return Password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * Sets the password of the user.
	 * 
	 * @param password
	 *            the user's password
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * Returns the role of the user.
	 * 
	 * @return User's role
	 */
	public String getRoleName() {
		return roleName;
	}

	/**
	 * Sets the role of the user.
	 * 
	 * @param roleName
	 *            User's role
	 */
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	@Override
	public boolean equals(Object object) {
		if (object == this) {
			return true;
		}
		if (object == null || object.getClass() != this.getClass()) {
			return false;
		}
		User otherUser = (User) object;
		if (userId == null) {
			if (otherUser.getUserId() != null) {
				return false;
			}
		} else if (userId != otherUser.getUserId()) {
			return false;
		}
		if (userName == null) {
			if (otherUser.getUserName() != null) {
				return false;
			}
		} else if (!userName.equals(otherUser.getUserName())) {
			return false;
		}
		if (login == null) {
			if (otherUser.getLogin() != null) {
				return false;
			}
		} else if (!login.equals(otherUser.getLogin())) {
			return false;
		}
		if (password == null) {
			if (otherUser.getPassword() != null) {
				return false;
			}
		} else if (!password.equals(otherUser.getPassword())) {
			return false;
		}
		if (roleName == null) {
			if (otherUser.getRoleName() != null) {
				return false;
			}
		} else if (!roleName.equals(otherUser.getRoleName())) {
			return false;
		}
		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * ((userId == null) ? 0 : userId.intValue())
				+ ((userName == null) ? 0 : userName.hashCode())
				+ ((login == null) ? 0 : login.hashCode())
				+ ((password == null) ? 0 : password.hashCode())
				+ ((roleName == null) ? 0 : roleName.hashCode());
		return result;
	}
}
