package com.epam.newsmanagement.dao.oracledao;

import java.util.HashSet;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.epam.newsmanagement.bean.User;
import com.epam.newsmanagement.dao.AbstractDAO;
import com.epam.newsmanagement.exception.DAOException;

/**
 * Class {@code OracleUserDAO} contains methods allowing to extract information
 * about Users found in the application, add, update, and delete their data.
 * 
 * @author Vasili Andreev
 * @version 1.0
 * @see com.epam.newsmanagement.bean.User
 * @see com.epam.newsmanagement.dao.AbstractDAO
 */
@Component
@Scope("prototype")
public class OracleUserDAO extends AbstractDAO<User> {

	/* finds all Users in the application */
	@Override
	@SuppressWarnings("unchecked")
	public Set<User> findAll() throws DAOException {
		Set<User> usersList = new HashSet<User>();

		try {
			Session session = getSession();
			Criteria cr = session.createCriteria(User.class);
			usersList.addAll(cr.list());
		} catch (HibernateException ex) {
			throw new DAOException("Database error", ex);
		} finally {
			closeSession();
		}
		return usersList;
	}

	/* returns User with the given ID */
	@Override
	public User findEntityById(Long userId) throws DAOException {
		User user = null;
		
		try {
			Session session = getSession();
			user = session.get(User.class, userId);
		} catch (HibernateException ex) {
			throw new DAOException("Database error", ex);
		} finally {
			closeSession();
		}
		return user;
	}

	/* adds new User to the database */
	@Override
	public boolean addNewEntity(User user) throws DAOException {
		Transaction tx = null;
		boolean isAdded = false;

		try {
			Session session = getSession();
			tx = session.beginTransaction();
			session.save(user);
			tx.commit();
			isAdded = true;
		} catch (HibernateException ex) {
			if (tx != null) {
				tx.rollback();
			}
			throw new DAOException("Database error", ex);
		} finally {
			closeSession();
		}
		return isAdded;
	}

	/* updates User's data */
	@Override
	public boolean updateEntity(User user, Long userId) throws DAOException {
		Transaction tx = null;
		boolean isUpdated = false;

		try {
			Session session = getSession();
			tx = session.beginTransaction();
			User oldUser = session.get(User.class, userId);
			oldUser.setUserName(user.getUserName());
			oldUser.setLogin(user.getLogin());
			oldUser.setPassword(user.getPassword());
			oldUser.setRoleName(user.getRoleName());
			session.update(oldUser);
			tx.commit();
			isUpdated = true;
		} catch (HibernateException ex) {
			if (tx != null) {
				tx.rollback();
			}
			throw new DAOException("Database error", ex);
		} finally {
			closeSession();
		}
		return isUpdated;
	}

	/* there is no need in such method in the application */
	@Override
	public boolean deleteEntity(Long userId) {
		throw new UnsupportedOperationException(
				"Error. This operation is not supported!");
	}
}
