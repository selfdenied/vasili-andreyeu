package com.epam.newsmanagement.bean;

import java.io.Serializable;
import java.util.Set;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Class {@code Tag} is a Java Bean that stores the data of Tags (IDs and
 * names).
 * 
 * @author Vasili Andreev
 * @version 1.0
 */
@Component
@Scope("prototype")
public class Tag implements Serializable {
	private static final long serialVersionUID = 8909637379314360840L;
	private Long tagId;
	private String tagName;
	private Set<News> listOfNews;

	/**
	 * Returns the ID of the tag.
	 * 
	 * @return tag's ID
	 */
	public Long getTagId() {
		return tagId;
	}

	/**
	 * Sets the ID of the tag.
	 * 
	 * @param tagId
	 *            the tag's ID
	 */
	public void setTagId(Long tagId) {
		this.tagId = tagId;
	}

	/**
	 * Returns the name of the tag.
	 * 
	 * @return tag's name
	 */
	public String getTagName() {
		return tagName;
	}

	/**
	 * Sets the name of the tag.
	 * 
	 * @param tagName
	 *            the tag's name
	 */
	public void setTagName(String tagName) {
		this.tagName = tagName;
	}

	/**
	 * Returns the list of news messages containing this tag.
	 * 
	 * @return the list of news messages
	 */
	public Set<News> getListOfNews() {
		return listOfNews;
	}

	/**
	 * Sets the the list of news messages containing this tag.
	 * 
	 * @param listOfNews
	 *            the list of news messages
	 */
	public void setListOfNews(Set<News> listOfNews) {
		this.listOfNews = listOfNews;
	}

	@Override
	public boolean equals(Object object) {
		if (object == this) {
			return true;
		}
		if (object == null || object.getClass() != this.getClass()) {
			return false;
		}
		Tag otherTag = (Tag) object;
		if (tagId == null) {
			if (otherTag.getTagId() != null) {
				return false;
			}
		} else if (tagId != otherTag.getTagId()) {
			return false;
		}
		if (tagName == null) {
			if (otherTag.getTagName() != null) {
				return false;
			}
		} else if (!tagName.equals(otherTag.getTagName())) {
			return false;
		}
		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * ((tagId == null) ? 0 : tagId.intValue())
				+ ((tagName == null) ? 0 : tagName.hashCode());
		return result;
	}
}
