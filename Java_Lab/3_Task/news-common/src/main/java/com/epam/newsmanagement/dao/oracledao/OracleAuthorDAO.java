package com.epam.newsmanagement.dao.oracledao;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;
import java.util.TimeZone;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.epam.newsmanagement.bean.Author;
import com.epam.newsmanagement.bean.News;
import com.epam.newsmanagement.dao.AbstractDAO;
import com.epam.newsmanagement.exception.DAOException;

/**
 * Class {@code OracleAuthorDAO} contains methods allowing to extract
 * information about Authors found in the application, add, update, and delete
 * their data.
 * 
 * @author Vasili Andreev
 * @version 1.0
 * @see com.epam.newsmanagement.bean.Author
 * @see com.epam.newsmanagement.dao.AbstractDAO
 */
@Component
@Scope("prototype")
public class OracleAuthorDAO extends AbstractDAO<Author> {
	private static final String LIST_OF_NEWS = "listOfNews";
	private static final String NEWS_MESSAGE = "newsMessage";
	private static final String MESSAGE_ID = "newsMessage.newsId";

	/* finds all Authors in the application */
	@Override
	@SuppressWarnings("unchecked")
	public Set<Author> findAll() throws DAOException {
		Set<Author> authorsList = new HashSet<Author>();

		try {
			Session session = getSession();
			Criteria cr = session.createCriteria(Author.class);
			authorsList.addAll(cr.list());
		} catch (HibernateException ex) {
			throw new DAOException("Database error", ex);
		} finally {
			closeSession();
		}
		return authorsList;
	}

	/* returns Author with the given ID */
	@Override
	public Author findEntityById(Long authorId) throws DAOException {
		Author author = null;

		try {
			Session session = getSession();
			author = session.get(Author.class, authorId);
		} catch (HibernateException ex) {
			throw new DAOException("Database error", ex);
		} finally {
			closeSession();
		}
		return author;
	}

	/**
	 * Finds the Author associated with the given news message.
	 * 
	 * @param newsId
	 *            The ID of the news message
	 * @return The Author associated with the news message
	 * @throws DAOException
	 *             If a database access/handling error occurs.
	 */
	public Author findAuthorByNewsId(Long newsId) throws DAOException {
		Author author = null;

		try {
			Session session = getSession();
			Criteria cr = session.createCriteria(Author.class);
			cr.createAlias(LIST_OF_NEWS, NEWS_MESSAGE);
			cr.add(Restrictions.eq(MESSAGE_ID, newsId));
			if (!cr.list().isEmpty()) {
				author = (Author) cr.list().get(0);				
			}
		} catch (HibernateException ex) {
			throw new DAOException("Database error", ex);
		} finally {
			closeSession();
		}
		return author;
	}
	

	/* adds new Author to the database */
	@Override
	public boolean addNewEntity(Author author) throws DAOException {
		Transaction tx = null;
		boolean isAdded = false;

		try {
			Session session = getSession();
			tx = session.beginTransaction();
			session.save(author);
			tx.commit();
			isAdded = true;
		} catch (HibernateException ex) {
			if (tx != null) {
				tx.rollback();
			}
			throw new DAOException("Database error", ex);
		} finally {
			closeSession();
		}
		return isAdded;
	}

	/**
	 * Assigns an Author with the given ID to the news message with the given
	 * ID.
	 * 
	 * @param authorId
	 *            The ID of the Author
	 * @param newsId
	 *            The ID of the News message
	 * @return {@code true} if the Author has been successfully assigned and
	 *         {@code false} otherwise
	 * @throws DAOException
	 *             If a database access/handling error occurs.
	 */
	public boolean assignAuthorToNews(Long newsId, Long authorId)
			throws DAOException {
		Transaction tx = null;
		boolean isAssigned = false;

		try {
			Session session = getSession();
			tx = session.beginTransaction();
			Author author = session.get(Author.class, authorId);
			News news = session.get(News.class, newsId);
			author.getListOfNews().add(news);
			session.update(author);
			tx.commit();
			isAssigned = true;
		} catch (HibernateException ex) {
			if (tx != null) {
				tx.rollback();
			}
			throw new DAOException("Database error", ex);
		} finally {
			closeSession();
		}
		return isAssigned;
	}

	/**
	 * Deassigns an Author from the news message with the given ID.
	 * 
	 * @param newsId
	 *            The ID of the News message
	 * @return {@code true} if the Author has been successfully deassigned and
	 *         {@code false} otherwise
	 * @throws DAOException
	 *             If a database access/handling error occurs.
	 */
	public boolean deassignAuthorFromNews(Long newsId) throws DAOException {
		Transaction tx = null;
		boolean isDeAssigned = false;

		try {
			Session session = getSession();
			tx = session.beginTransaction();
			News news = session.get(News.class, newsId);
			Author author = news.getAuthor();
			author.getListOfNews().remove(news);
			session.update(author);
			tx.commit();
			isDeAssigned = true;
		} catch (HibernateException ex) {
			if (tx != null) {
				tx.rollback();
			}
			throw new DAOException("Database error", ex);
		} finally {
			closeSession();
		}
		return isDeAssigned;
	}

	/* updates Author's data */
	@Override
	public boolean updateEntity(Author author, Long authorId)
			throws DAOException {
		Transaction tx = null;
		boolean isUpdated = false;

		try {
			Session session = getSession();
			tx = session.beginTransaction();
			Author oldAuthor = session.get(Author.class, authorId);
			oldAuthor.setAuthorName(author.getAuthorName());
			oldAuthor.setExpired(author.getExpired());
			session.update(oldAuthor);
			tx.commit();
			isUpdated = true;
		} catch (HibernateException ex) {
			if (tx != null) {
				tx.rollback();
			}
			throw new DAOException("Database error", ex);
		} finally {
			closeSession();
		}
		return isUpdated;
	}

	/* Deletes Author's data */
	@Override
	public boolean deleteEntity(Long authorId) throws DAOException {
		Transaction tx = null;
		boolean isDeleted = false;

		try {
			Session session = getSession();
			tx = session.beginTransaction();
			Author author = session.get(Author.class, authorId);
			author.setExpired(obtainTimestamp());
			session.update(author);			
			tx.commit();
			isDeleted = true;
		} catch (HibernateException ex) {
			if (tx != null) {
				tx.rollback();
			}
			throw new DAOException("Database error", ex);
		} finally {
			closeSession();
		}
		return isDeleted;
	}

	/* Gets the current Timestamp */
	private Timestamp obtainTimestamp() {
		TimeZone zone = TimeZone.getTimeZone("GMT+3");
		Calendar calendar = Calendar.getInstance(zone, Locale.getDefault());
		Date date = calendar.getTime();
		return new Timestamp(date.getTime());
	}
}
