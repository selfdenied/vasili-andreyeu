package com.epam.newsmanagement.service;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.epam.newsmanagement.bean.User;
import com.epam.newsmanagement.dao.oracledao.OracleUserDAO;
import com.epam.newsmanagement.exception.DAOException;

import static com.epam.newsmanagement.util.HashPassword.validatePassword;

/**
 * Class {@code UserService} contains methods that use DAO layer to check the
 * correctness of User's login and password.
 * 
 * @author Vasili Andreev
 * @version 1.0
 * @see com.epam.newsmanagement.dao.oracledao.OracleUserDAO
 */
@Component
@Scope("prototype")
public class UserService {
	@Autowired
	private OracleUserDAO userDAO;

	/**
	 * Sets the UserDAO dependency.
	 * 
	 * @param userDAO
	 *            the instance of UserDAO
	 */
	public void setUserDAO(OracleUserDAO userDAO) {
		this.userDAO = userDAO;
	}

	/**
	 * Method checks User's data for correctness.
	 * 
	 * @param login
	 *            User's login
	 * @param password
	 *            User's password
	 * @return {@code true} when User's data is correct and {@code false}
	 *         otherwise
	 * @throws DAOException
	 *             If a DAO exception of some sort has occurred
	 * @throws InvalidKeySpecException
	 *             If key specification is invalid
	 * @throws NoSuchAlgorithmException
	 *             if there is no such cryptographic algorithm
	 */
	public boolean checkUserData(String login, String password)
			throws DAOException, NoSuchAlgorithmException,
			InvalidKeySpecException {
		User user = null;
		boolean isDataCorrect = false;

		Set<User> usersList = userDAO.findAll();
		for (User usr : usersList) {
			if (login.equals(usr.getLogin())) {
				user = usr;
			}
		}
		if (user != null && validatePassword(password, user.getPassword())) {
			isDataCorrect = true;
		}
		return isDataCorrect;
	}

	/**
	 * Method obtains User's role.
	 * 
	 * @param login
	 *            User's login
	 * @return User's role
	 * @throws DAOException
	 *             If a DAO exception of some sort has occurred
	 */
	public String obtainUserRole(String login) throws DAOException {
		String role = null;

		Set<User> usersList = userDAO.findAll();
		for (User user : usersList) {
			if (login.equals(user.getLogin())) {
				role = user.getRoleName();
			}
		}
		return role;
	}

	/**
	 * Method obtains a User by its login.
	 * 
	 * @param login
	 *            User's login
	 * @return The instance of User
	 * @throws DAOException
	 *             If a DAO exception of some sort has occurred
	 */
	public User obtainUserByLogin(String login) throws DAOException {
		User user = null;

		Set<User> usersList = userDAO.findAll();
		for (User usr : usersList) {
			if (login.equals(usr.getLogin())) {
				user = usr;
			}
		}
		return user;
	}
}
