package com.epam.newsmanagement.dao;

import org.apache.log4j.Logger;

import java.util.Set;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.epam.newsmanagement.exception.DAOException;

/**
 * Abstract class {@code AbstractDAO} is a generic class that contains methods
 * allowing to operate with a database or data source (extract information,
 * update, add or delete data). The specific implementation of these methods
 * depends on a type of database/data source used (e.g. Oracle/MySQL database,
 * XML, etc.).
 * 
 * @author Vasili Andreev
 * @version 1.0
 */
public abstract class AbstractDAO<T> {
	private static final Logger LOG = Logger.getLogger(AbstractDAO.class);
	@Autowired
	private SessionFactory sessionFactory;
	private volatile Session session;
	
	/**
	 * Returns a current Hibernate session.
	 * 
	 * @return org.hibernate.Session
	 */
	protected Session getSession() throws HibernateException {
		try {
			session = sessionFactory.openSession();
		} catch (HibernateException ex) {
			LOG.error("Error. Unable to initialize session!", ex);
		}
		return session;
	}
	
	/**
	 * Closes current Hibernate session.
	 */
	protected void closeSession() {
		if (session != null) {
			session.close();
		}
	}
	
	/**
	 * Returns the set of type T Entity objects available in the application.
	 * The information is extracted from a database/data source.
	 * 
	 * @return The set of type T Entity objects
	 * @throws DAOException
	 *             If a database access/handling error occurs.
	 */
	public abstract Set<T> findAll() throws DAOException;

	/**
	 * Returns an object of type T Entity with the given ID. The information is
	 * extracted from a database/data source.
	 * 
	 * @param entityId
	 *            The id of the Entity
	 * @return Type T Entity object
	 * @throws DAOException
	 *             If a database access/handling error occurs.
	 */
	public abstract T findEntityById(Long entityId) throws DAOException;

	/**
	 * Adds new object of type T Entity to the database/data source.
	 * 
	 * @param entity
	 *            new type T Entity object
	 * @return {@code true} if the object has been successfully added and
	 *         {@code false} otherwise
	 * @throws DAOException
	 *             If a database access/handling error occurs.
	 */
	public abstract boolean addNewEntity(T entity) throws DAOException;

	/**
	 * Updates the data of type T Entity object in the database/data source.
	 * 
	 * @param entity
	 *            new type T Entity object
	 * @param entityId
	 *            the ID of type T Entity object to be updated
	 * @return {@code true} if the object's data has been successfully updated
	 *         and {@code false} otherwise
	 * @throws DAOException
	 *             If a database access/handling error occurs.
	 */
	public abstract boolean updateEntity(T entity, Long entityId)
			throws DAOException;

	/**
	 * Deletes the type T Entity object from the database/data source.
	 * 
	 * @param entityId
	 *            the ID of type T Entity object to be deleted
	 * @return {@code true} if the object's data has been successfully deleted
	 *         and {@code false} otherwise
	 * @throws DAOException
	 *             If a database access/handling error occurs.
	 */
	public abstract boolean deleteEntity(Long entityId) throws DAOException;
}
