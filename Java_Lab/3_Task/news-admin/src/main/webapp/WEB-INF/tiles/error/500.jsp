<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

	<div class="error">
		<img src="resources/images/error500.jpg"
			alt="<spring:message code="internal_error"/>"
			height="319" width="320">
	</div>

	<div class="error3">
		<p>
			<b><spring:message code="request_uri"/></b>
			<c:choose>
				<c:when test="${not empty pageContext.exception}">
    				${pageContext.request.scheme}://${header.host}${pageContext.errorData.requestURI}
    			</c:when>
				<c:otherwise>
    				${base}
    			</c:otherwise>
			</c:choose>
		</p>
		<p>
			<b><spring:message code="request_status"/></b>
			<spring:message code="failed"/>
		</p>
		<p>
			<b><spring:message code="error_code"/></b>
    		<spring:message code="500"/>
		</p>
		<p>
			<b><spring:message code="exception"/></b>
			<c:choose>
				<c:when test="${not empty pageContext.exception}">
    				${pageContext.exception}
    			</c:when>
				<c:otherwise>
    				${requestScope.exception}
    			</c:otherwise>
			</c:choose>
		</p>
		<p>
			<b><spring:message code="exception_message"/></b>
			<c:choose>
				<c:when test="${not empty pageContext.exception}">
    				${pageContext.exception.message}
    			</c:when>
				<c:otherwise>
    				${requestScope.exception.message}
    			</c:otherwise>
			</c:choose>
		</p>
		<c:if test="${not empty requestScope.exception}">
			<p>
				<b><spring:message code="exception_cause"/></b>
				${requestScope.exception.cause}
			</p>
		</c:if>

		<button onclick="history.back()">
			<spring:message code="back_to_previous"/>
		</button>
	</div>

</body>
</html>