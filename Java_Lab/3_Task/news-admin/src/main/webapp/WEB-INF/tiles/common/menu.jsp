<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="menu">
	<br />
	<form action="viewNews" method="post">
		<input type="HIDDEN" name="${_csrf.parameterName}" value="${_csrf.token}"/>
		<input type="HIDDEN" name="authorId" value="${authorId}">
		<c:forEach var="tag" items="${tagNames}">
			<input type="HIDDEN" name="tagNames" value="${tag}">
		</c:forEach>
		<c:choose>
		<c:when test="${action != 'viewNews'}">
			<input type="submit" id="menu" value="<spring:message code="news_list"/>">
		</c:when>
		<c:otherwise>
			<input type="submit" id="menu" disabled="disabled" value="<spring:message code="news_list"/>">
		</c:otherwise>
		</c:choose>
	</form>
	<form action="addNews" method="post">
		<input type="HIDDEN" name="${_csrf.parameterName}" value="${_csrf.token}"/>
		<c:choose>
		<c:when test="${action != 'addNews'}">
			<input type="submit" id="menu" value="<spring:message code="add_news"/>">
		</c:when>
		<c:otherwise>
			<input type="submit" id="menu" disabled="disabled" value="<spring:message code="add_news"/>">
		</c:otherwise>
		</c:choose>
	</form>
	<form action="addAuthors" method="post">
		<input type="HIDDEN" name="${_csrf.parameterName}" value="${_csrf.token}"/>
		<c:choose>
		<c:when test="${action != 'addAuthors'}">
			<input type="submit" id="menu" value="<spring:message code="add_authors"/>">
		</c:when>
		<c:otherwise>
			<input type="submit" id="menu" disabled="disabled" value="<spring:message code="add_authors"/>">
		</c:otherwise>
		</c:choose>
	</form>
	<form action="addTags" method="post">
		<input type="HIDDEN" name="${_csrf.parameterName}" value="${_csrf.token}"/>
		<c:choose>
		<c:when test="${action != 'addTags'}">
			<input type="submit" id="menu" value="<spring:message code="add_tags"/>">
		</c:when>
		<c:otherwise>
			<input type="submit" id="menu" disabled="disabled" value="<spring:message code="add_tags"/>">
		</c:otherwise>
		</c:choose>
	</form>
	<br />
</div>