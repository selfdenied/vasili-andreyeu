<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="center">
	
	<form action="login" method="post">
		<input type="HIDDEN" name="${_csrf.parameterName}" value="${_csrf.token}"/>
		<b><spring:message code="login"/></b>
		<br />
		<input type="text" name="username" size="30" maxlength="30" required="required">
		<br />
		<b><spring:message code="password"/></b>
		<br />
		<input type="password" name="password" size="30" maxlength="30" 
		pattern="[A-Za-z0-9@\\._\\-]{5,20}" required="required">
		<br />
		<br /> 
		<input type="submit" value="<spring:message code="sign_in"/>">
	</form>
	
	<br />
	<br />
	
	<c:if test="${param.error != null}">
		<div class="redC" id="19px">
			<spring:message code="auth_incorrect_message"/>
		</div>
	</c:if>
	
	<c:if test="${param.loggedOut != null}">
		<div class="greenC" id="19px">
			<spring:message code="logout_performed"/>
		</div>
	</c:if>

</div>