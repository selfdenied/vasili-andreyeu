<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

	<div class="error">
		<br> <img src="resources/images/error404.jpg"
			alt="<spring:message code="page_not_found"/>" height="328" width="554">
	</div>

	<div class="error2">
		<p>
			<b><spring:message code="request_uri"/></b>
			${pageContext.request.scheme}://${header.host}${pageContext.errorData.requestURI}
		</p>
		<p>
			<b><spring:message code="request_status"/></b>
			<spring:message code="failed"/>
		</p>
		<p>
			<b><spring:message code="servlet_name"/></b>
			${pageContext.errorData.servletName}
		</p>
		<p>
			<b><spring:message code="error_code"/></b>
			${pageContext.errorData.statusCode}
		</p>

		<br>

		<button onclick="history.back()">
			<spring:message code="back_to_previous"/>
		</button>
	</div>

</body>
</html>