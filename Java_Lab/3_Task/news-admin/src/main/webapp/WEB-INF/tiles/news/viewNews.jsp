<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="my" uri="/WEB-INF/tld/custom.tld"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<div class="center">
	<form method="post" action="viewNews">
		<input type="HIDDEN" name="${_csrf.parameterName}" value="${_csrf.token}"/>
		<select	id="author" name="authorId">
			<option selected="selected" disabled="disabled">
				<spring:message code="select_author"/>
			</option>
			<c:forEach var="author" items="${listOfAuthors}">
				<c:choose>
				<c:when test="${author.authorId == authorId}">
					<option selected="selected" value="${author.authorId}">
						<c:out value="${author.authorName}" />
					</option>
				</c:when>
				<c:otherwise>
					<option value="${author.authorId}">
						<c:out value="${author.authorName}" />
					</option>
				</c:otherwise>
				</c:choose>
			</c:forEach>
		</select> 
		&nbsp;
		<select id="tags" name="tagNames" multiple="multiple">
			<option disabled="disabled">
				<spring:message code="select_tags"/>
			</option>
    		<c:forEach var="tag" items="${listOfTags}">
    			<c:choose>
    			<c:when test="${my:contains(tagNames, tag.tagName)}">
    				<option selected="selected" value="${tag.tagName}">
    					<c:out value="${tag.tagName}" />
    				</option>
    			</c:when>
    			<c:otherwise>
    			    <option value="${tag.tagName}">
    					<c:out value="${tag.tagName}" />
    				</option>
    			</c:otherwise>
    			</c:choose>
    		</c:forEach>
		</select>
		&nbsp; 
		<input type="submit" value="<spring:message code="filter"/>" />
		&nbsp;
		<button type="button" id="reset">
			<spring:message code="reset"/>
		</button> 
	</form>

	<c:choose>
		<c:when test="${not empty listOfNews}">
		<table>
			<c:forEach var="news" items="${listOfNews}">
				<tr>
					<td id="news">
						<div>
							<b><c:out value="${news.title}" /></b>
							&nbsp;&nbsp;
							<span class="red">
								(<c:out value="${news.author.authorName}" />)
							</span>
							<span id="underline">
								<fmt:formatDate value="${news.modDate}" />
							</span>
						</div>
					</td>
				</tr>
				<tr>
					<td id="shortText">
						<br />
						<c:out value="${news.shortText}" />.
						<form method="post" id="right" action="viewMessage">
							<input type="HIDDEN" name="${_csrf.parameterName}" value="${_csrf.token}"/>
							<input type="HIDDEN" name="newsId" value="${news.newsId}">
							<input type="HIDDEN" name="authorId" value="${authorId}">
							<c:forEach var="tag" items="${tagNames}">
								<input type="HIDDEN" name="tagNames" value="${tag}">
							</c:forEach>
							<input type="submit" id="view2" value="<spring:message code="view"/>">
						</form>
						<form method="post" id="right" action="editNews">
							<input type="HIDDEN" name="${_csrf.parameterName}" value="${_csrf.token}"/>
							<input type="HIDDEN" name="newsId" value="${news.newsId}">
							<input type="HIDDEN" name="authorId" value="${authorId}">
							<c:forEach var="tag" items="${tagNames}">
								<input type="HIDDEN" name="tagNames" value="${tag}">
							</c:forEach>
							<input type="submit" id="view2" value="<spring:message code="edit"/>">
						</form>
					</td>
				</tr>
				<tr>
					<td id="tags">
						<div class="right" id="gray">
							<c:forEach var="tag" items="${news.listOfTags}">
								<c:out value="${tag.tagName}" />,&nbsp;
							</c:forEach>
							<span class="red">
								<spring:message code="comments"/>
								(<c:out value="${fn:length(news.listOfComments)}" />)
							</span>
							&nbsp;
							&nbsp;
							<input type="checkbox" name="newsDeleteIds"
							value="${news.newsId}" form="delForm">
						</div>
					</td>
				</tr>
			</c:forEach>		
		</table>
		</c:when>
		<c:otherwise>
			<h1 class="red">
				<br />
				<spring:message code="news_list_empty"/>
			</h1>
		</c:otherwise>
	</c:choose>
	
	<br />
	<div class="right">
		<form action="deleteNews" method="post" id="delForm">
			<input type="HIDDEN" name="${_csrf.parameterName}" value="${_csrf.token}"/>
			<input type="HIDDEN" name="pageNumber" value="${pageNumber}">
			<input type="HIDDEN" name="authorId" value="${authorId}">
			<c:forEach var="tag" items="${tagNames}">
				<input type="HIDDEN" name="tagNames" value="${tag}">
			</c:forEach>
			<input type="submit" id="del" value="<spring:message code="delete"/>" 
			onclick="return confirm('<spring:message code="sure_news"/>')">
		</form>
	</div>
	
 	<ul class="pagination">
 		<c:forEach var="page" items="${listOfPages}">
 			<c:choose>
 			<c:when test="${page == pageNumber}">
 				<li>
 					<form action="viewNews" method="post" id="left">
 						<input type="HIDDEN" name="${_csrf.parameterName}" value="${_csrf.token}"/>
						<input type="HIDDEN" name="pageNumber" value="${page}"> 
						<input type="HIDDEN" name="authorId" value="${authorId}">
						<c:forEach var="tag" items="${tagNames}">
							<input type="HIDDEN" name="tagNames" value="${tag}">
						</c:forEach>
						<input type="submit" disabled="disabled" value="<c:out value='${page}'></c:out>">
					</form>
				</li>
  			</c:when>
  			<c:otherwise>
  				<li>
  					<form action="viewNews" method="post" id="left">
  						<input type="HIDDEN" name="${_csrf.parameterName}" value="${_csrf.token}"/>
						<input type="HIDDEN" name="pageNumber" value="${page}">
						<input type="HIDDEN" name="authorId" value="${authorId}">
						<c:forEach var="tag" items="${tagNames}">
							<input type="HIDDEN" name="tagNames" value="${tag}">
						</c:forEach>
						<input type="submit" value="<c:out value='${page}'></c:out>">
					</form>
  				</li>
  			</c:otherwise>
  			</c:choose>
  		</c:forEach>
  	</ul>
 </div>