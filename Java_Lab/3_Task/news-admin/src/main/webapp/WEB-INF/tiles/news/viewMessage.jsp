<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<table>
	<tr>
		<td id="news2">
			<div>
				<b><c:out value="${news.title}" /></b>
				&nbsp;&nbsp;
				<span class="red">
				(<c:out value="${news.author.authorName}" />)
				</span>
				<span id="underline">
					<fmt:formatDate value="${news.modDate}" />
				</span>
			</div>
		</td>
	</tr>
</table>

<table id="text">	
	<tr>
		<td id="text">
			<br />
			<c:out value="${news.fullText}" />
			<br />
		</td>
	</tr>
</table>

<br />
<br />

<table id="comment">
	<tr>
		<c:choose>
		<c:when test="${not empty listOfComments}">
			<td id="comment">
				<br />
				<c:forEach var="comment" items="${listOfComments}">
					<span id="underline">
						<fmt:formatDate value="${comment.creationDate}" />
					</span>
					<br />
					<div id="grayB">
						<c:out value="${comment.commentText}"></c:out>
						<form method="post" action="deleteComment" id="right">
							<input type="HIDDEN" name="${_csrf.parameterName}" value="${_csrf.token}"/>
							<input type="HIDDEN" name="commentId" value="${comment.commentId}">
							<input type="HIDDEN" name="newsId" value="${newsId}">
							<input type="HIDDEN" name="authorId" value="${authorId}">
							<c:forEach var="tag" items="${tagNames}">
								<input type="HIDDEN" name="tagNames" value="${tag}">
							</c:forEach>
							<input type="submit" id="delComment" value="X" 
							onclick="return confirm('<spring:message code="sure_comment"/>')">
						</form>
					</div>
					<br />
				</c:forEach>
			</td>
		</c:when>
		<c:otherwise>
			<td id="comment">
				<h2 class="blueC">
					<spring:message code="comment_list_empty"/>
				</h2>
			</td>
		</c:otherwise>
		</c:choose>
	</tr>
</table>

<br />

<div class="center">
	<form method="post" action="viewMessage">
		<input type="HIDDEN" name="${_csrf.parameterName}" value="${_csrf.token}"/>
		<input type="HIDDEN" name="newsId" value="${newsId}">
		<input type="HIDDEN" name="authorId" value="${authorId}">
		<c:forEach var="tag" items="${tagNames}">
			<input type="HIDDEN" name="tagNames" value="${tag}">
		</c:forEach>
		<textarea name="commentText" maxlength="100" required="required"></textarea>
		<br />
		<br />
		<input type="submit" value="<spring:message code="post_comment"/>">
	</form>
</div>

<br />
<br />

<c:if test="${not empty prevNewsId}">
	<form method="post" id="left" action="viewMessage">
		<input type="HIDDEN" name="${_csrf.parameterName}" value="${_csrf.token}"/>
		<input type="HIDDEN" name="newsId" value="${prevNewsId}">
		<input type="HIDDEN" name="authorId" value="${authorId}">
		<c:forEach var="tag" items="${tagNames}">
			<input type="HIDDEN" name="tagNames" value="${tag}">
		</c:forEach>
		<input type="submit" id="view" value="<spring:message code="previous"/>">
	</form>
</c:if>

<c:if test="${not empty nextNewsId}">
	<form method="post" id="right" action="viewMessage">
		<input type="HIDDEN" name="${_csrf.parameterName}" value="${_csrf.token}"/>
		<input type="HIDDEN" name="newsId" value="${nextNewsId}">
		<input type="HIDDEN" name="authorId" value="${authorId}">
		<c:forEach var="tag" items="${tagNames}">
			<input type="HIDDEN" name="tagNames" value="${tag}">
		</c:forEach>
		<input type="submit" id="view" value="<spring:message code="next"/>">
	</form>
</c:if>