<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

	<br />
	<br />
	
	<div class="error">
		<br> <img src="resources/images/error403.jpg"
			alt="<spring:message code="access_denied"/>" height="421" width="554">
	</div>

	<div class="error2">
		<br />
		
		<h3>
			<b><spring:message code="access_denied_message"/></b>
		</h3>

		<br />

		<button onclick="history.back()">
			<spring:message code="back_to_previous"/>
		</button>
	</div>

</body>
</html>