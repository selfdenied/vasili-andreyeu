<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<br />

<table class="center">
	<c:forEach var="tag" items="${listOfTags}">
		<tr>
			<td id="addLeft">
				<b><spring:message code="tag"/></b>
				&nbsp;&nbsp;&nbsp;
				<form action="editTag" method="post" class="inline" id="editTF${tag.tagId}">
					<input type="HIDDEN" name="tagId" value="${tag.tagId}">
					<input type="HIDDEN" name="${_csrf.parameterName}" value="${_csrf.token}"/>
					
					<input type="text" disabled="disabled" name="tagName" size="30" maxlength="30"
					id="text${tag.tagId}" value="${tag.tagName}">
				</form>
			</td>	
			<td id="addRight">
				<input type="submit" class="hideThis" id="update${tag.tagId}" 
				value="<spring:message code="update"/>" form="editTF${tag.tagId}">	
				<button type="button" id="edit${tag.tagId}" onclick="showUpdate(${tag.tagId})">
					<spring:message code="edit"/>
				</button>
				
				<form action="deleteTag" method="post" class="inline">
					<input type="HIDDEN" name="tagId" value="${tag.tagId}">
					<input type="HIDDEN" name="${_csrf.parameterName}" value="${_csrf.token}"/>
					
					<input type="submit" class="hideThis" id="expire${tag.tagId}" 
					value="<spring:message code="delete"/>">
				</form>
				&nbsp;
				<button type="button" onclick="cancelUpdate('${tag.tagId}', '${tag.tagName}')" 
				id="cancel${tag.tagId}" class="hideThis">
					<spring:message code="cancel"/>
				</button>
			</td>
		</tr>	
	</c:forEach>
</table>

<br />
<br />
<br />

<table class="center">
	<tr>
		<td id="add">
			<b><spring:message code="add_tag"/></b>
			&nbsp;&nbsp;&nbsp;
			<form action="addTags" method="post" class="inline">
				<input type="text" name="tagName" size="30" maxlength="30" required="required">
				<input type="HIDDEN" name="${_csrf.parameterName}" value="${_csrf.token}"/>
				&nbsp;&nbsp;&nbsp;
				<input type="submit" id="view" value="<spring:message code="save"/>">
			</form>
		</td>
	</tr>
</table>