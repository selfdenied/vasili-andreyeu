<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<br />

<table class="center">
	<c:forEach var="author" items="${listOfAuthors}">
		<tr>
			<td id="addLeft">
				<b><spring:message code="author"/></b>
				&nbsp;&nbsp;&nbsp;
				<form action="editAuthor" method="post" class="inline" id="editF${author.authorId}">
					<input type="HIDDEN" name="authorId" value="${author.authorId}">
					<input type="HIDDEN" name="${_csrf.parameterName}" value="${_csrf.token}"/>
					
					<input type="text" disabled="disabled" name="authorName" size="30" maxlength="30"
					id="text${author.authorId}" value="${author.authorName}">
				</form>
			</td>	
			<td id="addRight">
				<input type="submit" class="hideThis" id="update${author.authorId}" 
				value="<spring:message code="update"/>" form="editF${author.authorId}">	
				<button type="button" id="edit${author.authorId}" onclick="showUpdate(${author.authorId})">
					<spring:message code="edit"/>
				</button>
				
				<form action="expireAuthor" method="post" class="inline">
					<input type="HIDDEN" name="authorId" value="${author.authorId}">
					<input type="HIDDEN" name="${_csrf.parameterName}" value="${_csrf.token}"/>
					
					<input type="submit" class="hideThis" id="expire${author.authorId}" 
					value="<spring:message code="expire"/>">
				</form>
				&nbsp;
				<button type="button" onclick="cancelUpdate('${author.authorId}', '${author.authorName}')" 
				id="cancel${author.authorId}" class="hideThis">
					<spring:message code="cancel"/>
				</button>
			</td>
		</tr>	
	</c:forEach>
</table>

<br />
<br />
<br />

<table class="center">
	<tr>
		<td id="add">
			<b><spring:message code="add_author"/></b>
			&nbsp;&nbsp;&nbsp;
			<form action="addAuthors" method="post" class="inline">
				<input type="text" name="authorName" size="30" maxlength="30" required="required">
				<input type="HIDDEN" name="${_csrf.parameterName}" value="${_csrf.token}"/>
				&nbsp;&nbsp;&nbsp;
				<input type="submit" id="view" value="<spring:message code="save"/>">
			</form>
		</td>
	</tr>
</table>