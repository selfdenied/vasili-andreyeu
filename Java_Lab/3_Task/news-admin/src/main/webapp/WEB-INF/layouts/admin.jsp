<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<html>
  <head>
  	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  	<link rel='stylesheet' href='resources/css/bootstrap-3.3.2.min.css' type='text/css' />
  	<link rel='stylesheet' href='resources/css/bootstrap-multiselect.css' type='text/css' />
  	<link rel='stylesheet' href='resources/css/style.css' type='text/css' />
  	<script type="text/javascript" src="resources/js/jquery-2.1.3.min.js"></script>
  	<script type="text/javascript" src="resources/js/bootstrap-3.3.2.min.js"></script>
  	<script type="text/javascript" src="resources/js/bootstrap-multiselect.js"></script>
  	<script type="text/javascript" src="resources/js/dropdown.js"></script>
    <title><tiles:getAsString name="title"/></title>
  </head>
 	<body>
		<table>
  			<tr>
    			<td colspan="2">
     				<tiles:insertAttribute name="header" />
    			</td>
  			</tr>
  			<tr>
    			<td id="menu">
      				<tiles:insertAttribute name="menu" />
    			</td>
    			<td id="menu2">
      				<tiles:insertAttribute name="body" />
    			</td>
  			</tr>
  			<tr>
    			<td colspan="2">
      				<tiles:insertAttribute name="footer" />
   				 </td>
  			</tr>
		</table>
  </body>
</html>