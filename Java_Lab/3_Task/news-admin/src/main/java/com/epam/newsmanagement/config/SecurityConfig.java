package com.epam.newsmanagement.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;


/**
 * Class {@code SecurityConfig} is a configuration class that contains various
 * methods that enable proper functioning of Spring Security with custom login
 * page, extraction of users' data from the database, and passwords hashing.
 * 
 * @author Vasili Andreev
 * @version 1.0
 */
@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
	@Autowired
	@Qualifier("customUserDetailsService")
	private UserDetailsService userDetailsService;
	@Autowired
	@Qualifier("PBKDF2PasswordEncoder")
	private PasswordEncoder passwordEncoder;

	@Autowired
	public void configAuthentication(AuthenticationManagerBuilder auth)
			throws Exception {
		auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder);
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests()
				.antMatchers("/login").permitAll()
				.antMatchers("/error403", "/log_out").authenticated()
				.antMatchers("/viewNews", "/deleteNews", "/viewMessage",
						"/deleteNews", "/deleteComment", "/editNews",
						"/addNews", "/addTags", "/editTag", "/deleteTag",
						"/addAuthors", "/editAuthor", "/expireAuthor")
				.access("hasRole('ROLE_ADMIN')")
				.and().formLogin().loginPage("/login")
				.defaultSuccessUrl("/viewNews", true)
				.usernameParameter("username").passwordParameter("password")
				.and().csrf()
				.and().exceptionHandling().accessDeniedPage("/error403");
	}
}
