package com.epam.newsmanagement.controller.author;

import static com.epam.newsmanagement.util.RequestParameters.*;

import java.util.Locale;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.epam.newsmanagement.bean.Author;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.service.AuthorService;

/**
 * Class {@code AddEditAuthorsController} deals with adding and updating of the
 * existing authors.
 * 
 * @author Vasili Andreev
 * @version 1.0
 */
@Controller
public class AddEditAuthorsController {
	private static final String ADD_EDIT_AUTHORS = "author/addEditAuthors";
	@Autowired
	private AuthorService authorService;

	@RequestMapping(value = "/addAuthors", method = RequestMethod.POST)
	public String addAuthors(HttpServletRequest req, Model model, Locale locale,
			Author author) throws DAOException {
		String url = ADD_EDIT_AUTHORS;
		String authorName = author.getAuthorName();
		HttpSession session = req.getSession(false);
		
		if (authorName != null && !authorName.isEmpty()) {
			authorService.addNewAuthor(author);
		}
		setAttrsToModel(model);
		session.setAttribute(ACTION, "addAuthors");
		session.setAttribute(LOCALE, locale);
		return url;
	}
	
	@RequestMapping(value = "/editAuthor", method = RequestMethod.POST)
	public String editAuthor(HttpServletRequest req, Model model, Locale locale,
			Author author) throws DAOException {
		String url = ADD_EDIT_AUTHORS;
		String authorName = author.getAuthorName();
		HttpSession session = req.getSession(false);
		
		if (authorName != null && !authorName.isEmpty()) {
			authorService.updateAuthor(author, author.getAuthorId());
		}
		setAttrsToModel(model);
		session.setAttribute(LOCALE, locale);
		return url;
	}
	
	@RequestMapping(value = "/expireAuthor", method = RequestMethod.POST)
	public String expireAuthor(HttpServletRequest req, Model model, Locale locale,
			Author author) throws DAOException {
		String url = ADD_EDIT_AUTHORS;
		Long authorId = author.getAuthorId();
		HttpSession session = req.getSession(false);
		
		if (authorId != null) {
			authorService.deleteAuthor(authorId);
		}
		setAttrsToModel(model);
		session.setAttribute(LOCALE, locale);
		return url;
	}
	
	/* supplementary method that sets attributes to model */
	private void setAttrsToModel(Model model) throws DAOException {
		Set<Author> authorsList = authorService.findAllActiveAuthors();
		model.addAttribute(AUTHORS_LIST, authorsList);
	}
}
