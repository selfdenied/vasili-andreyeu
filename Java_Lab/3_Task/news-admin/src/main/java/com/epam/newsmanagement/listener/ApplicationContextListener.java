package com.epam.newsmanagement.listener;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import org.apache.log4j.Logger;

import oracle.ucp.UniversalConnectionPoolException;
import oracle.ucp.admin.UniversalConnectionPoolManager;
import oracle.ucp.admin.UniversalConnectionPoolManagerImpl;

/**
 * Class {@code ApplicationContextListener} is a Listener which closes DB
 * connection pool when the application is destroyed.
 * 
 * @author Vasili Andreev
 * @version 1.0
 * @see javax.servlet.ServletContextListener
 */
@WebListener
public class ApplicationContextListener implements ServletContextListener {

	/**
	 * Constructs ApplicationContextListener object
	 */
	public ApplicationContextListener() {
	}

	/**
	 * This method is invoked when the application context is initialized.
	 * 
	 * @param event
	 *            javax.servlet.ServletContextEvent
	 * @see javax.servlet.ServletContextListener
	 */
	public void contextInitialized(ServletContextEvent event) {
	}

	/**
	 * This method is invoked when the application context is destroyed. Here DB
	 * connection pool is closed.
	 * 
	 * @param event
	 *            javax.servlet.ServletContextEvent
	 * @see javax.servlet.ServletContextListener
	 */
	public void contextDestroyed(ServletContextEvent event) {
		Logger LOG = Logger.getLogger(ApplicationContextListener.class);
		
		try {
			UniversalConnectionPoolManager ucpManager = UniversalConnectionPoolManagerImpl
					.getUniversalConnectionPoolManager();
			if (ucpManager != null) {
				String[] poolNames = ucpManager.getConnectionPoolNames();
				if (poolNames != null) {
					for (String poolName : poolNames) {
						ucpManager.destroyConnectionPool(poolName);
					}
				}
			}
		} catch (UniversalConnectionPoolException ex) {
			LOG.warn("Unable to close UCP: ", ex);
		}
	}
}
