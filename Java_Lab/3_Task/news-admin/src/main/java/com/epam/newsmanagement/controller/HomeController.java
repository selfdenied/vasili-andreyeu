package com.epam.newsmanagement.controller;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import static com.epam.newsmanagement.util.RequestParameters.*;

/**
 * Class {@code HomeController} is invoked when the request to view the home
 * page is received.
 * 
 * @author Vasili Andreev
 * @version 1.0
 */
@Controller
public class HomeController {
	private static final String LOGIN = "common/login";
	private static final String LOGOUT = "redirect:/login?loggedOut";
	private static final String ERROR_403 = "error/error403";

	@RequestMapping(value = "/login")
	public String home(HttpServletRequest req, Locale locale) {
		HttpSession session = req.getSession();
		session.setAttribute(LOCALE, locale);
		session.setAttribute(ACTION, "login");
		return LOGIN;
	}

	@RequestMapping(value = "/log_out")
	public String logout(HttpServletRequest req, HttpServletResponse resp,
			Locale locale, Model model) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();

		if (auth != null) {
			new SecurityContextLogoutHandler().logout(req, resp, auth);
		}
		model.addAttribute(LOCALE, locale);
		return LOGOUT;
	}

	@RequestMapping(value = "/error403")
	public String accessDenied(HttpServletRequest req, Locale locale) {
		HttpSession session = req.getSession(false);
		session.setAttribute(LOCALE, locale);
		return ERROR_403;
	}
}
