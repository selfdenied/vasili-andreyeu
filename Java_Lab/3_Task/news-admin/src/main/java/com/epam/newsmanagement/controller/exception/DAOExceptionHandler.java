package com.epam.newsmanagement.controller.exception;

import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.epam.newsmanagement.exception.DAOException;

/**
 * Class {@code DAOExceptionHandler} handles internal DAO Exceptions that may be
 * thrown by DAO layer. It performs logging and redirects the request to the
 * proper error page.
 * 
 * @author Vasili Andreev
 * @version 1.0
 */
@ControllerAdvice
public class DAOExceptionHandler {
	private static final Logger LOG = Logger.getLogger(DAOExceptionHandler.class);
	private static final String ERROR = "error/error500";

	@ExceptionHandler(DAOException.class)
	@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
	public String handleDAOException(Exception ex) {
		LOG.error(ex);
		return ERROR;
	}
}
