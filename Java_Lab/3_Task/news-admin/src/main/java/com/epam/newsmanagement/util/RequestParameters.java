package com.epam.newsmanagement.util;

/**
 * Abstract class {@code RequestParameters} contains a number of constants used
 * throughout the application, i.e. request/session parameters.
 * 
 * @author Vasili Andreev
 * @version 1.0
 */
public abstract class RequestParameters {
	public static final String DEFAULT_PAGE = "1";
	public static final String LOCALE = "locale";
	public static final String ACTION = "action";
	public static final String USER_NAME = "userName";
	public static final String ADMIN = "ROLE_ADMIN";
	public static final String PAGE_NUMBER = "pageNumber";
	public static final String AUTHORS_LIST = "listOfAuthors";
	public static final String TAGS_LIST = "listOfTags";
	public static final String NEWS_LIST = "listOfNews";
	public static final String PAGES_LIST = "listOfPages";
	public static final String AUTHOR_ID = "authorId";
	public static final String AUTHOR = "author";
	public static final String TAG_NAMES = "tagNames";
	public static final String NEWS_TAG_NAMES = "newsTagNames";
	public static final int RESULTS_PER_PAGE = 4;
	public static final String NEXT_NEWS_ID = "nextNewsId";
	public static final String PREV_NEWS_ID = "prevNewsId";
	public static final String DEL_NEWS_IDS = "newsDeleteIds";
	public static final String NEWS_ID = "newsId";
	public static final String NEWS = "news";
	public static final String TITLE = "title";
	public static final String CR_DATE = "creationDate";
	public static final String ADD_TAGS_IDS = "addTagsIds";
	public static final String EDIT_TAGS_IDS = "editTagsIds";
	public static final String COMMENTS_LIST = "listOfComments";
	public static final String LOCKED = "locked";
}
