package com.epam.newsmanagement.util;

import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.epam.newsmanagement.bean.User;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.service.UserService;

/**
 * Class {@code CustomUserDetailsService} provides a method that allows to
 * authenticate users with the help of the data stored in the database.
 * 
 * @author Vasili Andreev
 * @version 1.0
 */
@Service("customUserDetailsService")
public class CustomUserDetailsService implements UserDetailsService {
	private static final Logger LOG = Logger.getLogger(CustomUserDetailsService.class);
	@Autowired
	private UserService userService;

	/* loads all necessary User data */
	@Override
	public UserDetails loadUserByUsername(String login)
			throws UsernameNotFoundException {
		User user = null;
		List<SimpleGrantedAuthority> authorities = null;

		try {
			user = userService.obtainUserByLogin(login);
			String role = userService.obtainUserRole(login);
			if (user == null) {
				LOG.error("Error. User is not found!");
				throw new UsernameNotFoundException("User not found!");
			}
			authorities = Arrays.asList(new SimpleGrantedAuthority(role));
		} catch (DAOException ex) {
			LOG.error(ex);
		}
		return new org.springframework.security.core.userdetails.User(
				user.getLogin(), user.getPassword(), authorities);
	}
}
