package com.epam.newsmanagement.controller.news;

import static com.epam.newsmanagement.util.RequestParameters.*;
import static com.epam.newsmanagement.search.news.NewsSearchCriteria.AuthorSearchType.BY_ID;

import java.security.Principal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.epam.newsmanagement.bean.Author;
import com.epam.newsmanagement.bean.Tag;
import com.epam.newsmanagement.bean.User;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.search.news.NewsSearchResults;
import com.epam.newsmanagement.service.*;

/**
 * Class {@code ViewNewsController} is invoked to view the list of News messages
 * available in the application. It also deals with pagination and Author/Tags
 * search functionality.
 * 
 * @author Vasili Andreev
 * @version 1.0
 */
@Controller
public class ViewNewsController {
	private static final String VIEW_NEWS = "news/viewNews";
	@Autowired
	private TagService tagService;
	@Autowired
	private UserService userService;
	@Autowired
	private AuthorService authorService;
	@Autowired
	private NewsService newsService;

	@RequestMapping(value = "/viewNews")
	public String viewNews(HttpServletRequest req, Model model, Locale locale,
			Principal principal) throws DAOException {
		String url = VIEW_NEWS;
		String pageNumber = req.getParameter(PAGE_NUMBER);
		HttpSession session = req.getSession(false);
		String userName = (String) session.getAttribute(USER_NAME);

		pageNumber = checkPageNumber(pageNumber);
		setAttrsToModel(model);
		setNewsResultsToModel(req, model, pageNumber);
		session.setAttribute(LOCALE, locale);
		session.setAttribute(ACTION, "viewNews");
		if (userName == null || userName.isEmpty()) {
			User user = userService.obtainUserByLogin(principal.getName());
			session.setAttribute(USER_NAME, user.getUserName());
		}
		return url;
	}

	@RequestMapping(value = "/deleteNews", method = RequestMethod.POST)
	public String deleteNews(HttpServletRequest req, Model model)
			throws DAOException {
		String url = VIEW_NEWS;
		String pageNumber = req.getParameter(PAGE_NUMBER);
		String[] delNewsIds = req.getParameterValues(DEL_NEWS_IDS);

		if (delNewsIds != null && delNewsIds.length != 0) {
			for (String newsId : delNewsIds) {
				Long id = Long.valueOf(newsId);
				newsService.deleteNewsMessage(id);
			}
		}
		setAttrsToModel(model);
		setNewsResultsToModel(req, model, pageNumber);
		return url;
	}

	/* supplementary method that sets attributes to model */
	private void setAttrsToModel(Model model) throws DAOException {
		/* find tags and authors and set them to model */
		Set<Author> authorsList = authorService.findAllAuthors();
		Set<Tag> tagsList = tagService.findAllTags();
		model.addAttribute(AUTHORS_LIST, authorsList);
		model.addAttribute(TAGS_LIST, tagsList);
	}

	/* supplementary method that searches the DB for news messages */
	private void setNewsResultsToModel(HttpServletRequest req, Model model,
			String pageNumber) throws DAOException {
		Author author = new Author();
		Set<String> namesOfTags = new HashSet<>();
		int page = Integer.parseInt(pageNumber);
		String authorId = req.getParameter(AUTHOR_ID);
		String[] tagNames = req.getParameterValues(TAG_NAMES);

		if (authorId != null && !authorId.isEmpty()) {
			author.setAuthorId(Long.valueOf(authorId));
		}
		if (tagNames != null && tagNames.length != 0) {
			for (String name : tagNames) {
				namesOfTags.add(name);
			}
		}
		NewsSearchResults newsResults = newsService.searchNews(author,
				namesOfTags, BY_ID, RESULTS_PER_PAGE);
		model.addAttribute(PAGE_NUMBER, pageNumber);
		model.addAttribute(NEWS_LIST, newsResults.getResults(page));
		model.addAttribute(PAGES_LIST, obtainPageNumbers(newsResults));
		model.addAttribute(AUTHOR_ID, authorId);
		model.addAttribute(TAG_NAMES, tagNames);
	}

	/* supplementary method that gets the list of page numbers */
	private List<String> obtainPageNumbers(NewsSearchResults newsResults) {
		List<String> pageNumbers = new ArrayList<>();

		for (int i = 1; i <= newsResults.getNumberOfPages(); i++) {
			pageNumbers.add(String.valueOf(i));
		}
		return pageNumbers;
	}

	/* initializes the default value of page number */
	private String checkPageNumber(String pageNumber) {
		if (pageNumber == null || pageNumber.isEmpty()) {
			pageNumber = DEFAULT_PAGE;
		}
		return pageNumber;
	}
}
