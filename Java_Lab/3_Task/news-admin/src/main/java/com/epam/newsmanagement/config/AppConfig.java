package com.epam.newsmanagement.config;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Locale;
import java.util.TimeZone;

import oracle.ucp.jdbc.PoolDataSource;
import oracle.ucp.jdbc.PoolDataSourceFactory;

import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Scope;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.core.io.ClassPathResource;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

/**
 * Class {@code AppConfig} is a configuration class that contains various
 * methods to create Spring beans and register Spring handlers/interceptors
 * that are used by Spring MVC application.
 * 
 * @author Vasili Andreev
 * @version 1.0
 */
@Configuration
@ComponentScan(basePackages = "com.epam.newsmanagement")
@EnableWebMvc
@Import({ SecurityConfig.class })
public class AppConfig extends WebMvcConfigurerAdapter {
	private static final Logger LOG = Logger.getLogger(AppConfig.class);

	/**
	 * Method initializes InternalResourceViewResolver and sets the proper
	 * parameters.
	 * 
	 * @return the instance of InternalResourceViewResolver
	 */
	@Bean
	public ViewResolver getViewResolver() {
		InternalResourceViewResolver resolver = new InternalResourceViewResolver();
		resolver.setPrefix("/WEB-INF/views/");
		resolver.setSuffix(".jsp");
		return resolver;
	}

	/**
	 * Method initializes ReloadableResourceBundleMessageSource and sets the
	 * proper parameters.
	 * 
	 * @return the instance of ReloadableResourceBundleMessageSource
	 */
	@Bean
	public MessageSource messageSource() {
		ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
		messageSource.setBasename("classpath:messages");
		messageSource.setDefaultEncoding("UTF-8");
		return messageSource;
	}

	/**
	 * Method initializes SessionLocaleResolver and sets the proper parameters.
	 * 
	 * @return the instance of SessionLocaleResolver
	 */
	@Bean
	public LocaleResolver localeResolver() {
		SessionLocaleResolver resolver = new SessionLocaleResolver();
		resolver.setDefaultLocale(Locale.getDefault());
		resolver.setDefaultTimeZone(TimeZone.getTimeZone("GMT+3"));
		return resolver;
	}

	/**
	 * Method initializes PoolDataSource and sets the proper parameters.
	 * 
	 * @return the instance of PoolDataSource
	 */
	@Bean
	@Scope("singleton")
	public PoolDataSource poolDataSource() {
		PoolDataSource poolDataSource = PoolDataSourceFactory.getPoolDataSource();

		try {
			poolDataSource.setURL("jdbc:oracle:thin:@localhost:1521:NEWSMANAGEM");
			poolDataSource.setUser("System");
			poolDataSource.setPassword("Bnfkbz1011");
			poolDataSource.setConnectionFactoryClassName("oracle.jdbc.pool.OracleDataSource");
			poolDataSource.setConnectionPoolName("JDBC_UCP");
			poolDataSource.setMinPoolSize(5);
			poolDataSource.setMaxPoolSize(20);
			poolDataSource.setInitialPoolSize(5);
			poolDataSource.setInactiveConnectionTimeout(60);
			poolDataSource.setConnectionWaitTimeout(10);
			poolDataSource.setValidateConnectionOnBorrow(true);
			poolDataSource.setMaxStatements(20);
		} catch (SQLException ex) {
			LOG.error("Error. Unable to initialize connection pool");
		}
		return poolDataSource;
	}
	
	/**
	 * Method initializes Hibernate SesionFactory and sets the proper parameters.
	 * 
	 * @return the instance of Hibernate SesionFactory
	 */
	@Bean
	@Scope("singleton")
	public SessionFactory sessionFactory() {
		LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();

		try {
			sessionFactory.setDataSource(poolDataSource());
			sessionFactory.setConfigLocation(new ClassPathResource("hibernate.xml"));
			sessionFactory.setMappingResources("entity.hbm.xml");
			sessionFactory.afterPropertiesSet();
		} catch (IOException ex) {
			LOG.error("Error. Unable to initialize Hibernate Session Factory");
		}
		return sessionFactory.getObject();
	}
	
	/**
	 * Method initializes HibernateTransactionManager and sets the proper parameters.
	 * 
	 * @return the instance of HibernateTransactionManager
	 */
	@Bean 
	public HibernateTransactionManager transactionManager() {
		HibernateTransactionManager transactionManager = new HibernateTransactionManager();
		transactionManager.setSessionFactory(sessionFactory());
		return transactionManager;
	}

	/* ResourceHandler is registered here */
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/resources/**").addResourceLocations(
				"/resources/");
	}

	/* Interceptors are registered here */
	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		LocaleChangeInterceptor localeInterceptor = new LocaleChangeInterceptor();
		localeInterceptor.setParamName("locale");
		registry.addInterceptor(localeInterceptor).addPathPatterns("/**");
	}
}