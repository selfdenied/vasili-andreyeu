$(function() {

	$('#tags').multiselect({

		includeSelectAllOption : true
	});

	$("#reset").on("click", function () {
	    $('#author').prop('selectedIndex', 0);
	    $('#tags').multiselect('clearSelection');
	    $('#tags').multiselect('refresh');
	});
});