<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="my" uri="/WEB-INF/tld/custom.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<fmt:setLocale value="${locale}" scope="request" />
<fmt:setBundle basename="text" var="rb" />

<div class="center">
	<form method="post">
		<input type="HIDDEN" name="action" value="searchNews">
		<input type="HIDDEN" name="locale" value="${locale}">
		<select	id="author" name="authorId">
			<option selected="selected" disabled="disabled">
				<fmt:message key="select_author" bundle="${rb}" />
			</option>
			<c:forEach var="author" items="${listOfAuthors}">
				<c:choose>
				<c:when test="${author.authorId == authorId}">
					<option selected="selected" value="${author.authorId}">
						<c:out value="${author.authorName}" />
					</option>
				</c:when>
				<c:otherwise>
					<option value="${author.authorId}">
						<c:out value="${author.authorName}" />
					</option>
				</c:otherwise>
				</c:choose>
			</c:forEach>
		</select> 
		&nbsp;
		<select id="tags" name="tagNames" multiple="multiple">
			<option disabled="disabled">
				<fmt:message key="select_tags" bundle="${rb}" />
			</option>
    		<c:forEach var="tag" items="${listOfTags}">
    			<c:choose>
    			<c:when test="${my:contains(tagNames, tag.tagName)}">
    				<option selected="selected" value="${tag.tagName}">
    					<c:out value="${tag.tagName}" />
    				</option>
    			</c:when>
    			<c:otherwise>
    			    <option value="${tag.tagName}">
    					<c:out value="${tag.tagName}" />
    				</option>
    			</c:otherwise>
    			</c:choose>
    		</c:forEach>
		</select>
		&nbsp; 
		<input type="submit" value="<fmt:message key='filter' bundle='${rb}' />" />
		&nbsp;
		<button type="button" id="reset">
			<fmt:message key="reset" bundle="${rb}" />
		</button> 
	</form>

	<c:choose>
		<c:when test="${not empty listOfNews}">
		<table>
			<c:forEach var="news" items="${listOfNews}">
				<tr>
					<td id="news">
						<div>
							<b><c:out value="${news.title}" /></b>
							&nbsp;&nbsp;
							<span class="red">
								(<c:out value="${news.author.authorName}" />)
							</span>
							<span id="underline">
								<fmt:formatDate value="${news.modDate}" />
							</span>
						</div>
					</td>
				</tr>
				<tr>
					<td id="shortText">
						<br />
						<c:out value="${news.shortText}" />.
						<form method="post" id="right">
							<input type="HIDDEN" name="action" value="viewNews">
							<input type="HIDDEN" name="locale" value="${locale}"> 
							<input type="HIDDEN" name="newsId" value="${news.newsId}">
							<input type="HIDDEN" name="authorId" value="${authorId}">
							<input type="HIDDEN" name="pageNumber" value="${pageNumber}">
							<c:forEach var="tag" items="${tagNames}">
								<input type="HIDDEN" name="tagNames" value="${tag}">
							</c:forEach>
							<input type="submit" id="view" value="<fmt:message key='view' bundle='${rb}' />">
						</form>
					</td>
				</tr>
				<tr>
					<td id="tags">
						<div class="right" id="gray">
							<c:forEach var="tag" items="${news.listOfTags}">
								<c:out value="${tag.tagName}" />,&nbsp;
							</c:forEach>
							<span class="red">
								<fmt:message key="comments" bundle="${rb}" />
								(<c:out value="${fn:length(news.listOfComments)}" />)
							</span>
						</div>
					</td>
				</tr>
			</c:forEach>		
		</table>
		</c:when>
		<c:otherwise>
			<h1 class="red">
				<br />
				<fmt:message key="news_list_empty" bundle="${rb}" />
			</h1>
		</c:otherwise>
	</c:choose>
	
	<div class="container">
  		<ul class="pagination">
  			<c:forEach var="page" items="${listOfPages}">
  				<c:choose>
  				<c:when test="${page == pageNumber}">
  					<li>
  						<form action="${base}" method="post" id="left">
							<input type="HIDDEN" name="locale" value="${locale}">
							<c:if test="${not empty action}">
								<input type="HIDDEN" name="action" value="${action}">
							</c:if>
							<input type="HIDDEN" name="pageNumber" value="${page}"> 
							<input type="HIDDEN" name="authorId" value="${authorId}">
							<c:forEach var="tag" items="${tagNames}">
								<input type="HIDDEN" name="tagNames" value="${tag}">
							</c:forEach>
							<input type="submit" disabled="disabled" value="<c:out value='${page}'></c:out>">
						</form>
  					</li>
  				</c:when>
  				<c:otherwise>
  					<li>
  						<form action="${base}" method="post" id="left">
							<input type="HIDDEN" name="locale" value="${locale}"> 
							<c:if test="${not empty action}">
								<input type="HIDDEN" name="action" value="${action}">
							</c:if>
							<input type="HIDDEN" name="pageNumber" value="${page}">
							<input type="HIDDEN" name="authorId" value="${authorId}">
							<c:forEach var="tag" items="${tagNames}">
								<input type="HIDDEN" name="tagNames" value="${tag}">
							</c:forEach>
							<input type="submit" value="<c:out value='${page}'></c:out>">
						</form>
  					</li>
  				</c:otherwise>
  				</c:choose>
  			</c:forEach>
  		</ul>
	</div>
</div>