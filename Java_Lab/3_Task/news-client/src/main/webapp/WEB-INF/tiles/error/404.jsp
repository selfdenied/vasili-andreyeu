<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<fmt:setLocale value="${locale}" scope="request" />
<fmt:setBundle basename="text" var="rb" />

	<div class="error">

		<br> <img src="images/error404.jpg"
			alt="<fmt:message key='page_not_found' bundle='${rb}' />"
			
			height="328" width="554">
	</div>

	<div class="error2">
		<p>
			<b><fmt:message key="request_uri" bundle="${rb}" /></b>
			${pageContext.request.scheme}://${header.host}${pageContext.errorData.requestURI}
		</p>
		<p>
			<b><fmt:message key="request_status" bundle="${rb}" /></b>
			<fmt:message key="failed" bundle="${rb}" />
		</p>
		<p>
			<b><fmt:message key="servlet_name" bundle="${rb}" /></b>
			${pageContext.errorData.servletName}
		</p>
		<p>
			<b><fmt:message key="error_code" bundle="${rb}" /></b>
			${pageContext.errorData.statusCode}
		</p>

		<br>

		<button onclick="history.back()">
			<fmt:message key="back_to_previous" bundle="${rb}" />
		</button>
	</div>

</body>
</html>