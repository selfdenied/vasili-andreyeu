package com.epam.newsmanagement.command;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.epam.newsmanagement.bean.Author;
import com.epam.newsmanagement.bean.News;
import com.epam.newsmanagement.bean.Tag;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.search.news.NewsSearchResults;
import com.epam.newsmanagement.service.AuthorService;
import com.epam.newsmanagement.service.NewsService;
import com.epam.newsmanagement.service.TagService;

import static com.epam.newsmanagement.util.RequestParameters.*;

/**
 * Class {@code HomeCommandImpl} is invoked when no 'action' parameter is
 * specified in the request object. It redirects the request to the default
 * page.
 * 
 * @author Vasili Andreev
 * @version 1.0
 * @see com.epam.newsmanagement.command.ICommand
 */
public class HomeCommandImpl implements ICommand {
	private static final Logger LOG = Logger.getLogger(HomeCommandImpl.class);
	private static final String ERROR = "/jsp/error/error500.jsp";
	private static final String HOME = "/jsp/common/home.jsp";
	@Autowired
	private TagService tagService;
	@Autowired
	private AuthorService authorService;
	@Autowired
	private NewsService newsService;

	@Override
	public String execute(HttpServletRequest request) {
		String url = HOME;
		String locale = request.getParameter(LOCALE);
		String pageNumber = request.getParameter(PAGE_NUMBER);
		/* setting default locale and page number values */
		if (locale == null || locale.isEmpty()) {
			locale = DEFAULT_LOCALE;
		}
		if (pageNumber == null || pageNumber.isEmpty()) {
			pageNumber = DEFAULT_PAGE;
		}
		/* setting necessary attributes to request */
		try {
			setAttrsToRequest(request, pageNumber);
		} catch (DAOException ex) {
			LOG.error(ex);
			request.setAttribute("exception", ex);
			url = ERROR;
		}
		request.setAttribute(LOCALE, locale);
		request.setAttribute(PAGE_NUMBER, pageNumber);
		return url;
	}

	/* supplementary method that sets attributes to request */
	private void setAttrsToRequest(HttpServletRequest request, String pageNumber)
			throws DAOException {
		int page = Integer.parseInt(pageNumber);
		List<News> results = newsService.obtainAllNewsSorted();
		NewsSearchResults newsResults = new NewsSearchResults();
		newsResults.setResultsPerPage(RESULTS_PER_PAGE);
		newsResults.setResults(results);
		
		/* find tags and authors and set them to request */
		Set<Author> authorsList = authorService.findAllAuthors();
		Set<Tag> tagsList = tagService.findAllTags();

		request.setAttribute(AUTHORS_LIST, authorsList);
		request.setAttribute(TAGS_LIST, tagsList);
		request.setAttribute(NEWS_LIST, newsResults.getResults(page));
		request.setAttribute(PAGES_LIST, obtainPageNumbers(newsResults));
	}

	/* supplementary method that gets the list of page numbers */
	private List<String> obtainPageNumbers(NewsSearchResults newsResults) {
		List<String> pageNumbers = new ArrayList<>();

		for (int i = 1; i <= newsResults.getNumberOfPages(); i++) {
			pageNumbers.add(String.valueOf(i));
		}
		return pageNumbers;
	}
}
