package com.epam.newsmanagement.command;

import static com.epam.newsmanagement.util.RequestParameters.*;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.epam.newsmanagement.bean.Author;
import com.epam.newsmanagement.bean.Comment;
import com.epam.newsmanagement.bean.News;
import com.epam.newsmanagement.exception.DAOException;

import static com.epam.newsmanagement.search.news.NewsSearchCriteria.AuthorSearchType.BY_ID;

import com.epam.newsmanagement.service.CommentService;
import com.epam.newsmanagement.service.NewsService;

/**
 * Class {@code ViewNewsCommandImpl} allows to view the selected news message.
 * 
 * @author Vasili Andreev
 * @version 1.0
 * @see com.epam.newsmanagement.command.ICommand
 */
public class ViewNewsCommandImpl implements ICommand {
	private static final Logger LOG = Logger.getLogger(ViewNewsCommandImpl.class);
	private static final String ERROR = "/jsp/error/error500.jsp";
	private static final String VIEW_NEWS = "/jsp/news/viewNews.jsp";
	@Autowired
	private NewsService newsService;
	@Autowired
	private CommentService commentService;

	@Override
	public String execute(HttpServletRequest request) {
		String url = VIEW_NEWS;
		String commentText = request.getParameter(COMMENT_TEXT);
		Long newsId = Long.valueOf(request.getParameter(NEWS_ID));

		try {
			if (commentText != null && !commentText.isEmpty()) {
				Comment comment = new Comment();
				comment.setCommentText(commentText);
				News message = newsService.obtainNewsMessage(newsId);
				comment.setNewsMessage(message);
				commentService.addNewComment(comment);
			}
			setAttrsToRequest(request);
		} catch (DAOException ex) {
			LOG.error(ex);
			request.setAttribute("exception", ex);
			url = ERROR;
		}
		return url;
	}

	/* supplementary method that sets various attributes to request */
	private void setAttrsToRequest(HttpServletRequest request)
			throws DAOException {
		News news = null;
		String locale = request.getParameter(LOCALE);
		String newsId = request.getParameter(NEWS_ID);
		String authorId = request.getParameter(AUTHOR_ID);
		String[] tagNames = request.getParameterValues(TAG_NAMES);
		String pageNumber = request.getParameter(PAGE_NUMBER);
		Long id = Long.valueOf(newsId);
		news = newsService.obtainNewsMessage(id);
		List<Comment> listOfComments = newsService.obtainNewsCommentsSorted(id);

		setNewsIds(request, id, authorId, tagNames);
		request.setAttribute(LOCALE, locale);
		request.setAttribute(NEWS_ID, newsId);
		request.setAttribute(NEWS, news);
		request.setAttribute(ACTION, "viewNews");
		request.setAttribute(PAGE_NUMBER, pageNumber);
		request.setAttribute(AUTHOR_ID, authorId);
		request.setAttribute(TAG_NAMES, tagNames);
		request.setAttribute(COMMENTS_LIST, listOfComments);
	}

	/* sets the IDs of the next and previous news messages to request */
	private void setNewsIds(HttpServletRequest request, Long newsId,
			String authorId, String[] tagNames) throws DAOException {
		List<News> newsList = allFoundNews(authorId, tagNames);
		int index = findNewsIndex(newsList, newsId);

		if (index > 0) {
			News message = newsList.get(index - 1);
			request.setAttribute(PREV_NEWS_ID, message.getNewsId());
		}
		if (index >= 0 && index < newsList.size() - 1) {
			News message = newsList.get(index + 1);
			request.setAttribute(NEXT_NEWS_ID, message.getNewsId());
		}
	}

	/* finds the index of news in the list */
	private int findNewsIndex(List<News> newsList, Long newsId) {
		int index = -1;
		for (int i = 0; i < newsList.size(); i++) {
			News message = newsList.get(i);
			if (message.getNewsId() == newsId) {
				index = i;
			}
		}
		return index;
	}

	/* finds all news according to previous search criteria */
	private List<News> allFoundNews(String authorId, String[] tagNames)
			throws DAOException {
		Author author = new Author();
		Set<String> tagsNames = new HashSet<>();

		if (authorId != null && !authorId.isEmpty()) {
			author.setAuthorId(Long.valueOf(authorId));
		}
		if (tagNames != null && tagNames.length != 0) {
			for (String name : tagNames) {
				tagsNames.add(name);
			}
		}
		return newsService.searchNews(author, tagsNames, BY_ID,
				RESULTS_PER_PAGE).getResults();
	}
}
